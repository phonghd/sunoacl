﻿/*
 * Licensed under the Apache License, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * See https://github.com/aspnet-contrib/AspNet.Security.OAuth.Providers
 * for more information concerning the license and the contributors participating to this project.
 */

using System;
using System.Globalization;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Http;

namespace AspNet.Security.OAuth.Zalo
{
    /// <summary>
    /// Defines a set of options used by <see cref="ZaloAuthenticationHandler"/>.
    /// </summary>
    public class ZaloAuthenticationOptions : OAuthOptions
    {
        public ZaloAuthenticationOptions()
        {
            ClaimsIssuer = ZaloAuthenticationDefaults.Issuer;

            CallbackPath = new PathString(ZaloAuthenticationDefaults.CallbackPath);

            AuthorizationEndpoint = ZaloAuthenticationDefaults.AuthorizationEndpoint;
            TokenEndpoint = ZaloAuthenticationDefaults.TokenEndpoint;
            UserInformationEndpoint = ZaloAuthenticationDefaults.UserInformationEndpoint;

            ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "id");
            ClaimActions.MapJsonKey(ClaimTypes.Name, "name");
            ClaimActions.MapJsonKey("gender", "gender");
            ClaimActions.MapJsonKey("picture", "picture:data:url");
            ClaimActions.MapJsonKey("birthdate", "birthday");

            //            ClaimActions.MapJsonKey("urn:github:name", "name");
            //            ClaimActions.MapJsonKey("urn:github:url", "url");
        }

        /// <summary>
        /// Check that the options are valid.  Should throw an exception if things are not ok.
        /// </summary>
        public override void Validate()
        {
            if (string.IsNullOrEmpty(this.AppId))
                throw new ArgumentException();
            if (string.IsNullOrEmpty(this.AppSecret))
                throw new ArgumentException();
            base.Validate();
        }

        /// <summary>Gets or sets the Facebook-assigned appId.</summary>
        public string AppId
        {
            get
            {
                return ClientId;
            }
            set
            {
                ClientId = value;
            }
        }

        /// <summary>Gets or sets the Facebook-assigned app secret.</summary>
        public string AppSecret
        {
            get
            {
                return this.ClientSecret;
            }
            set
            {
                this.ClientSecret = value;
            }
        }

        /// <summary>
        /// Gets or sets the address of the endpoint exposing
        /// the email addresses associated with the logged in user.
        /// </summary>
        public string UserEmailsEndpoint { get; set; } = ZaloAuthenticationDefaults.UserEmailsEndpoint;
    }
}
