FROM microsoft/dotnet:2.1-sdk
WORKDIR /app

ENV TZ=Asia/Bangkok
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN ls

COPY publish .
ENTRYPOINT [ "dotnet" , "Suno.Acl.dll" ]