using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class Signup
    {
        public long Id { get; set; } //(bigint, not null)
        public string EmailOrPhone { get; set; } //(nvarchar(511), not null)
        public DateTime ExpirationDate { get; set; } //(datetime, not null)
        public string SignupToken { get; set; } //(nvarchar(511), not null)
        public DateTime? SignupDate { get; set; } //(datetime, null)
        public string SignupSource { get; set; } //(nvarchar(1024), null)
    }
}