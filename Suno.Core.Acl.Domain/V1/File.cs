using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class File
    {
        public long Id { get; set; } //(bigint, not null)
        public string Path { get; set; } //(nvarchar(1023), not null)
        public string OrignalName { get; set; } //(nvarchar(511), not null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public int? CompanyId { get; set; } //(int, null)
        public int ResourceType { get; set; } //(int, not null)
        public Guid FileKey { get; set; } //(uniqueidentifier, not null)
        public string ThumbnailInfo { get; set; } //(nvarchar(4000), null)
    }
}