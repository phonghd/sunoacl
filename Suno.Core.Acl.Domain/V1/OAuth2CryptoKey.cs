using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class OAuth2CryptoKey
    {
        public int Id { get; set; } //(int, not null)
        public string Handle { get; set; } //(nvarchar(max), null)
        public string Bucket { get; set; } //(nvarchar(max), null)
        public byte[] Secret { get; set; } //(varbinary(max)2147483647)
        public DateTime ExpiresUtc { get; set; } //(datetime, not null)
    }
}