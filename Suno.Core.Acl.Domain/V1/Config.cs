namespace Suno.Core.Acl.Domain.V1
{
    public class Config
    {
        public long Id { get; set; } //(bigint, not null)
        public string Key { get; set; } //(nvarchar(511), not null)
        public string Value { get; set; } //(nvarchar(max), not null)
    }
}