using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class CompanyFeatureLimit
    {
        public int CompanyId { get; set; } //(int, not null)
        public int? MaxEmailPerMonth { get; set; } //(int, null)
        public int? MaxUser { get; set; } //(int, null)
        public int? MaxStore { get; set; } //(int, null)
        public int? MaxSaleOrderPerDay { get; set; } //(int, null)
        public int? MaxProduct { get; set; } //(int, null)
        public int? MaxProductItem { get; set; } //(int, null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int? CreatedUser { get; set; } //(int, null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
    }
}