namespace Suno.Core.Acl.Domain.V1
{
    public class BillingPaymentProvider
    {
        public int id { get; set; } //(int, not null)
        public string ProviderCode { get; set; } //(nvarchar(10), not null)
        public string ProviderName { get; set; } //(nvarchar(50), not null)
        public string MerchantId { get; set; } //(nvarchar(50), not null)
        public string MerchantPassword { get; set; } //(nvarchar(50), not null)
        public string MerchantEmail { get; set; } //(nvarchar(50), not null)
        public string ReturnUrl { get; set; } //(nvarchar(50), not null)
        public string CancelUrl { get; set; } //(nvarchar(50), not null)
    }
}