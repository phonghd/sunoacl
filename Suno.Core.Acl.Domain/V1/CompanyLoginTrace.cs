using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class CompanyLoginTrace
    {
        public int CompanyId { get; set; } //(int, not null)
        public DateTime? LastLogin { get; set; } //(datetime, null)
    }
}