using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class Feedback
    {
        public long Id { get; set; } //(bigint, not null)
        public string FeedbackContent { get; set; } //(nvarchar(max), not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CompanyId { get; set; } //(int, not null)
        public int UserId { get; set; } //(int, not null)
    }
}