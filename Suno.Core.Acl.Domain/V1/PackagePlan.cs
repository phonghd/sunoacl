using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class PackagePlan
    {
        public int Id { get; set; } //(int, not null)
        public int CompanyId { get; set; } //(int, not null)
        public int PackageId { get; set; } //(int, not null)
        public DateTime SelectedDate { get; set; } //(datetime, not null)
        public int Status { get; set; } //(int, not null)
        public DateTime StatusDate { get; set; } //(datetime, not null)
    }
}