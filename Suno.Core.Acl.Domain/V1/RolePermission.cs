namespace Suno.Core.Acl.Domain.V1
{
    public class RolePermission
    {
        public int Id { get; set; } //(int, not null)
        public int RoleId { get; set; } //(int, not null)
        public int PermissionId { get; set; } //(int, not null)

        public Role Role { get; set; }
        public Permission Permission { get; set; }
    }
}