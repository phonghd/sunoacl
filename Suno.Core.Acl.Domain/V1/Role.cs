using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V1
{
    public class Role
    {
        public int RoleId { get; set; } //(int, not null)
        public string Name { get; set; } //(nvarchar(125), not null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public long CreatedUser { get; set; } //(bigint, not null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public bool IsGlobal { get; set; } //(bit, not null)
        public int? CompanyId { get; set; } //(int, null)


        public List<RolePermission> RolePermissions { get; set; }
        public List<UserRole> UserRoles { get; set; }
    }
}