namespace Suno.Core.Acl.Domain.V1
{
    public class MailTemplate
    {
        public long Id { get; set; } //(bigint, not null)
        public string Title { get; set; } //(nvarchar(max), not null)
        public string Content { get; set; } //(nvarchar(max), not null)
        public string MailTitle { get; set; } //(nvarchar(max), null)
    }
}