using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class Recommend
    {
        public int Id { get; set; } //(int, not null)
        public string Fullname { get; set; } //(nchar(100), not null)
        public string Phone { get; set; } //(nchar(20), not null)
        public string Email { get; set; } //(nchar(100), null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CompanyId { get; set; } //(int, not null)
        public int UserId { get; set; } //(int, not null)
    }
}