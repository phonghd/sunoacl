using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V1
{
    public partial class Package
    {
        public int PackageId { get; set; } //(int, not null)
        public string Name { get; set; } //(nvarchar(125), not null)
        public bool IsTrial { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public int CreatedUser { get; set; } //(int, not null)
        public bool IsFree { get; set; } //(bit, not null)
        public string Key { get; set; } //(nvarchar(50), not null)

        public List<PackageFeature> PackageFeatures { get; set; }
    }
}