using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class Membership
    {
        public int UserId { get; set; } //(int, not null)
        public string UserName { get; set; } //(nvarchar(511), not null)
        public string PasswordSalt { get; set; } //(nvarchar(1023), not null)
        public string Password { get; set; } //(nvarchar(1023), not null)
        public bool IsConfirmed { get; set; } //(bit, not null)
        public Guid? ResetPasswordToken { get; set; } //(uniqueidentifier, null)
        public int? CompanyId { get; set; } //(int, null)
        public Guid ConfirmationToken { get; set; } //(uniqueidentifier, not null)
        public DateTime? PasswordVerificationTokenExpirationDate { get; set; } //(datetime, null)
    }
}