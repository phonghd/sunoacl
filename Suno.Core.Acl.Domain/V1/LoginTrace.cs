using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class LoginTrace
    {
        public long Id { get; set; } //(bigint, not null)
        public int UserId { get; set; } //(int, not null)
        public DateTime LoginDate { get; set; } //(datetime, not null)
        public string IpAddress { get; set; } //(nvarchar(50), not null)
        public int? CompanyId { get; set; } //(int, null)
        public string UserAgent { get; set; } //(nvarchar(125), not null)
        public int ClientType { get; set; } //(int, not null)
    }
}