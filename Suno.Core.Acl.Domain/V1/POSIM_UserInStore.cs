using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class POSIM_UserInStore
    {
        public int Id { get; set; } //(int, not null)
        public int UserId { get; set; } //(int, not null)
        public int StoreId { get; set; } //(int, not null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public int? DeletedBy { get; set; } //(int, null)
    }
}