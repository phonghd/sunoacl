using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class Invitation
    {
        public long InvitationId { get; set; } //(bigint, not null)
        public int Sender { get; set; } //(int, not null)
        public string Email { get; set; } //(nvarchar(255), not null)
        public DateTime? ExpiredDate { get; set; } //(datetime, null)
        public Guid InviteToken { get; set; } //(uniqueidentifier, not null)
        public DateTime InvitedDate { get; set; } //(datetime, not null)
        public DateTime? AcceptedDate { get; set; } //(datetime, null)
        public int? RoleId { get; set; } //(int, null)

        public UserProfile UserSender { get; set; }
    }
}