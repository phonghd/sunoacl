namespace Suno.Core.Acl.Domain.V1
{
    public class PackageFeature
    {
        public int Id { get; set; } //(int, not null)
        public int PackageId { get; set; } //(int, not null)
        public int FeatureId { get; set; } //(int, not null)

        public Package Package { get; set; }
        public Feature Feature { get; set; }
    }
}