using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class BillingOrderDetail
    {
        public int Id { get; set; } //(int, not null)
        public int OrderId { get; set; } //(int, not null)
        public int PackageId { get; set; } //(int, not null)
        public byte Type { get; set; } //(tinyint, not null)
        public byte Quantity { get; set; } //(tinyint, not null)
        public decimal? Price { get; set; } //(money, null)
        public decimal? Discount { get; set; } //(money, null)
        public decimal? SellPrice { get; set; } //(money, null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CreatedUser { get; set; } //(int, not null)
        public int CompanyId { get; set; } //(int, not null)
    }
}