using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class CompanyContract
    {
        public int Id { get; set; } //(int, not null)
        public int CompanyId { get; set; } //(int, not null)
        public string ContractCode { get; set; } //(nvarchar(255), not null)
        public DateTime SignedDate { get; set; } //(datetime, not null)
        public int SignedUser { get; set; } //(int, not null)
        public DateTime? ActivedDate { get; set; } //(datetime, null)
        public DateTime? ExpiryDate { get; set; } //(date, null)
        public decimal Amount { get; set; } //(money, not null)
        public string RefCode { get; set; } //(nvarchar(255), null)
        public int Type { get; set; } //(int, not null)
        public string Description { get; set; } //(nvarchar(max), null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CreatedUser { get; set; } //(int, not null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
    }
}