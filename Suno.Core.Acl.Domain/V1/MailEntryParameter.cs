namespace Suno.Core.Acl.Domain.V1
{
    public class MailEntryParameter
    {
        public long Id { get; set; } //(bigint, not null)
        public string Key { get; set; } //(nvarchar(max), not null)
        public string Value { get; set; } //(nvarchar(max), not null)
        public long EntryId { get; set; } //(bigint, not null)
    }
}