namespace Suno.Core.Acl.Domain.V1
{
    public class OAuthMembership
    {
        public int Id { get; set; } //(int, not null)
        public string Provider { get; set; } //(nvarchar(125), not null)
        public string ProviderUserId { get; set; } //(nvarchar(255), not null)
        public int UserId { get; set; } //(int, not null)
        public int? CompanyId { get; set; } //(int, null)

        public UserProfile User { get; set; }
    }
}