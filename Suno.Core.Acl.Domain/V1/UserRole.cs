namespace Suno.Core.Acl.Domain.V1
{
    public class UserRole
    {
        public int Id { get; set; } //(int, not null)
        public int UserId { get; set; } //(int, not null)
        public int RoleId { get; set; } //(int, not null)

        public UserProfile UserProfile { get; set; }
        public Role Role { get; set; }
    }
}