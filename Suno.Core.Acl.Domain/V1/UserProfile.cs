using System;
using System.Collections.Generic;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Core.Acl.Domain.V1
{
    public class UserProfile
    {
        public int UserId { get; set; } //(int, not null)
        public string Email { get; set; } //(nvarchar(511), not null)
        public string PhoneNumber { get; set; } //(nvarchar(125), not null)
        public bool IsAdmin { get; set; } //(bit, not null)
        public long? Avatar { get; set; } //(bigint, null)
        public string DisplayName { get; set; } //(nvarchar(511), null)
        public string FirstName { get; set; } //(nvarchar(511), null)
        public string LastName { get; set; } //(nvarchar(511), null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public bool IsActived { get; set; } //(bit, not null)
        public int? CompanyId { get; set; } //(int, null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CreatedUser { get; set; } //(int, not null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public string DeletedNote { get; set; } //(nvarchar(1023), null)
        public string CultureInfo { get; set; } //(nvarchar(8), null)

        public List<Invitation> Invitations { get; set; }
        public List<OAuthMembership> OAuthMemberships { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public User User { get; set; }
    }
}