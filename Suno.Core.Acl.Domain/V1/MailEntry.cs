using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class MailEntry
    {
        public long Id { get; set; } //(bigint, not null)
        public string Subject { get; set; } //(nvarchar(max), not null)
        public string Body { get; set; } //(nvarchar(max), not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public long TenantId { get; set; } //(bigint, not null)
        public long? TemplateId { get; set; } //(bigint, null)
        public string To { get; set; } //(nvarchar(max), null)
        public string Cc { get; set; } //(nvarchar(max), null)
        public string Bcc { get; set; } //(nvarchar(max), null)

        
    }
}