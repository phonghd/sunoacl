namespace Suno.Core.Acl.Domain.V1
{
    public class CompanyExtension
    {
        public int CompanyId { get; set; } //(int, not null)
        public int? ProvinceId { get; set; } //(int, null)
        public int? StatusId { get; set; } //(int, null)
        public string Industry { get; set; } //(nvarchar(255), null)
        public string Packages { get; set; } //(nvarchar(255), null)
        public string Services { get; set; } //(nvarchar(255), null)
        public string Hardware { get; set; } //(nvarchar(255), null)
        public decimal? TotalAmount { get; set; } //(money, null)
        public string PartnerCode { get; set; } //(nvarchar(255), null)
    }
}