using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V1
{
    public class Company
    {
        public int CompanyId { get; set; } //(int, not null)
        public string CompanyName { get; set; } //(nvarchar(511), not null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CreatedUser { get; set; } //(int, not null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public string Address { get; set; } //(nvarchar(1023), null)
        public string PhoneNumber { get; set; } //(nvarchar(127), null)
        public string Website { get; set; } //(nvarchar(127), null)
        public string Email { get; set; } //(nvarchar(127), null)
        public int Status { get; set; } //(int, not null)
        public string LegalRepresentative { get; set; } //(nvarchar(127), not null)
        public string CompanyCode { get; set; } //(nvarchar(127), not null)
        public long? Logo { get; set; } //(bigint, null)
        public string TaxCode { get; set; } //(nvarchar(127), null)
        public int? Industry { get; set; } //(int, null)
        public Industry IndustryObj { get; set; }
        public string Comments { get; set; } //(ntext, null)

        public List<CompanyPackage> CompanyPackages { get; set; }
    }
}