using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    public class PermissionV2
    {
        public int PermissionId { get; set; }

        public string PermissionCode { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? DeletedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }

        public int? FeatureId { get; set;}

        public string Scope { get; set; }

        //public FeatureV2 Feature { get; set; }
        public List<RolePermissionV2> RolePermissions { get; set; }
    }


}
