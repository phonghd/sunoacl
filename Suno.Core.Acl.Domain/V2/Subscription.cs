﻿using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    


    public class App
    {
        public int AppId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }

        public List<User> Users { get; set; }

        public List<V1.Package> Packages { get; set; }
        public List<V1.Feature> Features { get; set; }
    }

    //public class PackageV2
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public int AppId { get; set; }
    //    public App App { get; set; }
    //    public List<FeatureV2> Features { get; set; }
    //}

    //public class FeatureV2
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public int PackageId { get; set; }
    //    public PackageV2 Package { get; set; }
    //}
}