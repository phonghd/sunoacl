﻿using System;

namespace Suno.Core.Acl.Domain.V2
{
    public class PosInvoiceDetail  : InvoiceDetail
    {
        public int CompanyId { get; set; }
        public int PackageId { get; set; }
        public DateTime ActivedDate { get; set; }
        public DateTime ExpiredDate { get; set; }

        public int? MaxEmailPerMonth { get; set; } //(int, null)
        public int? MaxUser { get; set; } //(int, null)
        public int? MaxStore { get; set; } //(int, null)
        public int? MaxSaleOrderPerDay { get; set; } //(int, null)
        public int? MaxProduct { get; set; } //(int, null)
        public int? MaxProductItem { get; set; } //(int, null)
        public int? MaxCustomer { get; set; }
    }

    public class OmnichannelInvoiceDetail : InvoiceDetail
    {
        public int CompanyId { get; set; }
        public int PackageId { get; set; }
        public DateTime ActivedDate { get; set; }
        public DateTime ExpiredDate { get; set; }
    }


    public abstract class Subscription
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Note { get; set; }
        public Guid TenantId { get; set; }
        public int PackageId { get; set; }

        public DateTime ActivedDate { get; set; }
        public DateTime ExpiredDate { get; set; }
    }

    public class PosSubscription : Subscription
    {
        public int? MaxEmailPerMonth { get; set; } //(int, null)
        public int? MaxUser { get; set; } //(int, null)
        public int? MaxStore { get; set; } //(int, null)
        public int? MaxSaleOrderPerDay { get; set; } //(int, null)
        public int? MaxProduct { get; set; } //(int, null)
        public int? MaxProductItem { get; set; } //(int, null)
        public int? MaxCustomer { get; set; }
    }

    public class OmniSubscription : Subscription
    {

    }

    public class PackagePermission
    {
        public int PackageId { get; set; }
        public int PermissionId { get; set; }
    }
}