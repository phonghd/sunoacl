namespace Suno.Core.Acl.Domain.V2
{
    public  class ClientGrantType 
    {
        public int Id { get; set;}
        public int ClientId { get; set;}
        public string GrantType { get; set;}

        public Client Client { get; set; }
    }
}
