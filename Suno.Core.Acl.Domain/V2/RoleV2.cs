using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    public class RoleV2
    {

        public int RoleId { get; set; }

        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedUser { get; set; }
        public int? DeletedUser { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool IsGlobal { get; set; }
        public Guid? TenantId { get; set; }

        public List<RolePermissionV2> RolePermissions { get; set; }
        public List<UserTenantRoleV2> UserTenantRoles { get; set; }
    }

    public class UserTenantRoleV2
    {
        public Guid UserSubjectId { get; set; }
        public Guid TenantId { get; set; }
        public int RoleId { get; set; }

        public RoleV2 Role { get; set; }
        public User User { get; set; }
        public Tenant Tenant { get; set; }
    }
}
