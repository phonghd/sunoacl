﻿using System;

namespace Suno.Core.Acl.Domain.V2
{
    public class UserSetting
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }

        
    }
}