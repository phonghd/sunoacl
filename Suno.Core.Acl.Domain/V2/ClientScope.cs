namespace Suno.Core.Acl.Domain.V2
{
    public partial class ClientScope 
    {
        public int Id { get; set;}
        public int ClientId { get; set;}
        public string Scope { get; set;}

        public Client Client { get; set; }
    }
}
