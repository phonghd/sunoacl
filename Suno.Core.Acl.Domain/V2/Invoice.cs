﻿using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    public class Invoice
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal InvoiceAmount { get; set; }
        public int CreatedBy { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime? DueDate { get; set; }
        public bool IsPaid { get; set; }

        public List<InvoiceDetail> InvoiceDetails { get; set; }
        public List<InvoicePayment> InvoicePayments { get; set; }
    }
}