﻿using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    public class Client
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string ClientName { get; set; }
        public int AccessTokenLifetime { get; set; }
        public int AccessTokenType { get; set; }
        public bool AllowAccessTokensViaBrowser { get; set; }
        public bool AllowOfflineAccess { get; set; }
        public string ClientUri { get; set; }
        public string Description { get; set; }
        public int IdentityTokenLifetime { get; set; }
        public bool RequireConsent { get; set; }
        public bool Enabled { get; set; }
        public string RedirectUris { get; set; }
        public string PostLogoutRedirectUris { get; set; }

        public List<ClientGrantType> ClientGrantTypes { get; set; }
        public List<ClientScope> ClientScopes { get; set; }
    }
}
