﻿using System;
using System.Collections.Generic;
using Suno.Core.Acl.Domain.V1;

namespace Suno.Core.Acl.Domain.V2
{
    public class User
    {
        public Guid SubjectId { get; set; }
        public int UserId { get; set; }
        public int? AppId { get; set; }
        public App App { get; set; }

        public List<UserClaim> UserClaims { get; set; }
        public List<UserTenantStore> UserTenantStores { get; set; }
        public List<UserTenant> UserTenants { get; set; }
        public UserProfile UserProfile { get; set; }
        public List<UserTenantRoleV2> UserTenantRoles { get; set; }
    }


    public class ClientInfo
    {
        public int Id { get; set; }
        public int RegisterCompanyId { get; set; }
        public string Action { get; set; }
        public string Info { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public enum eServiceRequest
    {
        ExtendationRequest
    }

    public class ServiceRequest
    {
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public int RequestedBy { get; set; }
        public DateTime RequestedDate { get; set; }
        public eServiceRequest Type { get; set; }

    }
}