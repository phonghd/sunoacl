﻿using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    public class Tenant
    {
        public Guid TenantId { get; set; }
        public int CompanyId { get; set; }

        public string Logo { get; set; }

        public int? AppId { get; set; }
        public App App { get; set; }


        public List<UserTenant> UserTenants { get; set; }
        public List<UserTenantRoleV2> UserTenantRoles { get; set; }
        public List<UserTenantStore> UserTenantStores { get; set; }
    }

    
}