namespace Suno.Core.Acl.Domain.V2
{
    public  class RolePermissionV2 
    {
        public int Id { get; set;}
        public int RoleId { get; set;}
        public int PermissionId { get; set;}

        public PermissionV2 Permission { get; set; }
        public RoleV2 Role { get; set; }
    }
}
