﻿using System;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Core.Acl.Domain
{
    public static class StaticData
    {
        public static App PosV2 = new App
        {
            Code = "SunoPosV2",
            Name = "Suno Pos",
            Version = (new Version(2, 0, 0, 0)).ToString()
        };

        public static App Omni = new App
        {
            Code = "Omni",
            Name = "Bán hàng đa kênh",
            Version = (new Version(1, 0, 0, 0)).ToString()
        };
    }
}
