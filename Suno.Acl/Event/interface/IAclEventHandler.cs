﻿namespace Suno.Acl.Event
{
    public interface IAclEventHandler
    {
        void AddHandler(ICompanyEventSender @event);
    }
}