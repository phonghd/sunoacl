﻿using System;
using Suno.Acl.Event.EventArgs;
namespace Suno.Acl.Event
{
    public interface ICompanyEventSender
    {
        event EventHandler<CompanyEventArgs> CompanyCreated;
        void RaiseEvent(CompanyEventArgs data);
    }
}