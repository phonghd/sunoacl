﻿using ServiceStack.Redis;
using Suno.Acl.Event.EventArgs;

namespace Suno.Acl.Event
{
    public class AclEventHandler : IAclEventHandler
    {
        private readonly IRedisClientsManager _manager;
        public AclEventHandler( IRedisClientsManager manager)
        {
            _manager = manager;
        }

        public void AddHandler(ICompanyEventSender @event)
        {
            @event.CompanyCreated += CompanyCreatedHandler;
        }

        private async void CompanyCreatedHandler(object sender, CompanyEventArgs data)
        {
            //Console.W
            //var result = await _data.GetCompanyInfo(data.TenantId);
            
            //using (var client = _manager.GetClient())
            //{
            //    client.Set($"Company_Created:{result.TenantId.ToString()}", result);
            //    client.PublishMessage("CompanyCreated", $"Company_Created:{result.TenantId.ToString()}");
            //}           
        }        
    }
}