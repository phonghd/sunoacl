﻿
using System;
using System.IO;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using Serilog;
using FluentEmail;
using FluentEmail.Core;
using FluentEmail.Core.Interfaces;
using FluentEmail.Razor;
using FluentEmail.Smtp;
using ServiceStack.Messaging;
using Suno.Acl.Models.Account;
using System.Linq;

namespace Suno.Acl.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly string _host;
        private readonly int _port;
        private readonly MailAddress _sender;
        private readonly string _senderStr;
        private readonly NetworkCredential _credential;
        public EmailSender(MailConfigOptions config)
        {
            _host = config.Host;
            _port = config.Port;

            _sender = new MailAddress(config.Sender ?? "");
            _senderStr = config.Sender;
            _credential = new NetworkCredential(config.User, config.Password);
        }


        public Task SendEmailAsync(string email, string subject, string message)
        {
            var smtp = new SmtpClient(_host, _port);
            smtp.EnableSsl = true;
            smtp.Credentials = _credential;

            var mailMessage = new MailMessage();
            mailMessage.From = _sender;

            if (!email.Contains(';'))
                mailMessage.To.Add(email);
            else
                email.Split(';').ToList().ForEach(e => mailMessage.To.Add(e));

            mailMessage.Body = message;
            mailMessage.Subject = subject;


            try
            {
                return smtp.SendMailAsync(mailMessage);
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }
            return Task.FromResult(0);
        }

        public Task SendResetPasswordEmail(string mailto, string subject, string message)
        {
            var smtp = new SmtpClient(_host, _port);
            smtp.EnableSsl = true;
            smtp.Credentials = _credential;
            var sender = new SmtpSender(smtp);
            var directory = Directory.GetCurrentDirectory();
            Email.DefaultRenderer = new RazorRenderer();
            var email = Email
                .From(_senderStr)
                .To(mailto)
                .Subject(subject)
                .UsingTemplateFromFile($"{directory}/EmailTemplates/ResetPassword.cshtml", new { Email = mailto, ResetLink = message });
            ;
            email.Sender = sender;

            try
            {
                return email.SendAsync();
            }
            catch (Exception e)
            {
                Log.Error(e.ToString());
            }

            return Task.FromResult(0);
        }
    }
}