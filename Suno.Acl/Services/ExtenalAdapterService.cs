﻿using System;
using System.Net.Http;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Suno.Acl.Models.Account;

namespace Suno.Acl.Services
{
    public class ExtenalAdapterService : IExternalAdapterService
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _env;

        public ExtenalAdapterService(IConfiguration configuration, IHostingEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }
        
        public bool CreateTenantToDomain(string tenantId, string companyCode, SignupInputModel companyInfo)
        {
            var enablePublishToPos =  _configuration.GetValue<bool>("PublishTenantToPOS");
            if (!enablePublishToPos)
                return true;

            var client = new HttpClient();
            string createTenantUrl = _configuration["CreateTenantDomainUrl"];
            var jsonObject = new
            {
                name = companyInfo.CompanyName,
                code = companyCode
            };

            var jsonContent = JsonConvert.SerializeObject(jsonObject);

            client.DefaultRequestHeaders.Add("TenantId", tenantId);
            var content = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            //if (_env.EnvironmentName == "Staging" || _env.EnvironmentName == "Production")
            {
                try
                {
                    var response = new HttpResponseMessage();
                    response = client.PostAsync(createTenantUrl, content).Result;
                    return response.IsSuccessStatusCode;
                }
                catch (Exception e)
                {                    
                    throw e;
                }
                
            }

            return true;
        }        
    }
}