﻿using System;
using System.Linq;
using ServiceStack;
using Suno.Acl.Data;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Acl.Services
{
    public class SettingService
    {
        private readonly AuthDbContext _authDbContext;
        public SettingService(AuthDbContext authDbContext)
        {
            _authDbContext = authDbContext;
        }

        public T GetValue<T>(Guid userId, Guid tenantId, string key, T defaultValue)
        {
            var setting = _authDbContext.Settings.Where(p => p.Key == key && p.UserId == userId && p.TenantId == tenantId)
                .Select(p => new { p.Id, p.Value }).FirstOrDefault();

            if (setting != null)
                return (T)setting.Value.ChangeTo(typeof(T));

            return defaultValue;
        }

        public void PutValue<T>(Guid userId, Guid tenantId, string key, T value)
        {
            var setting = _authDbContext.Settings.FirstOrDefault(p => p.Key == key && p.UserId == userId && p.TenantId == tenantId);
            if (setting == null)
            {
                setting = new UserSetting { TenantId = tenantId, UserId = userId, Key = key, Value = value.ToString() };
                _authDbContext.Settings.Add(setting);
            }
            else
                setting.Value = value.ToString();

            _authDbContext.SaveChanges();
        }
    }

    public class SettingKeys
    {
        public const string ViewedWelcome = "ViewedWelcome";
        public const string ViewedSalesTour = "ViewedSalesTour";
        public const string ViewedOverviewTour = "ViewedOverviewTour";

        public const string SurveyNewCustomer = "SurveyNewCustomer";
        public const string SurveyWantToViewTour = "SurveyWantToViewTour";
        public const string CollectedInfo = "CollectedInfo";

    }
}
;