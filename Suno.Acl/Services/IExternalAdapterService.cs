﻿using Suno.Acl.Models.Account;

namespace Suno.Acl.Services
{
    public interface IExternalAdapterService
    {
        bool CreateTenantToDomain(string tenantId, string companyCode, SignupInputModel companyInfo);
    }
}