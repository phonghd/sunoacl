﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.EntityFrameworkCore;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Core.Acl.Domain;

namespace Suno.Acl.Services
{
    public class ProfileService : IProfileService
    {
        private readonly AuthDbContext _authDbContext;
        private readonly AuthService _authService;

        public ProfileService(AuthDbContext authDbContext, AuthService authService)
        {
            _authDbContext = authDbContext;
            _authService = authService;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var subjectId = Guid.Parse(context.Subject.GetSubjectId());
            var user = _authService.Users().FirstOrDefault(p => p.User.SubjectId == subjectId);

            var scopeList = (from item in context.RequestedResources.ApiResources select item.Name).ToList();
            var userClaims = _authDbContext.UserClaims.AsNoTracking().Where(p => p.UserSubjectId == subjectId).ToList();

            var claims = new List<Claim>
            {
                new Claim("Username", user.Membership.UserName),
                new Claim("DisplayName", user.UserProfile.DisplayName ?? user.Membership.UserId.ToString()),
                new Claim("membershipId", user.Membership.UserId.ToString())
            };


            userClaims.ForEach(uc =>
            {
                claims.Add(new Claim(uc.ClaimType, uc.ClaimValue));
            });

            var userTenants = _authDbContext.UserTenants.Where(p => p.UserSubjectId == subjectId).Include(p => p.Tenant).ToList();
            var defaultTenant = userTenants.OrderByDescending(p => p.IsDefault).Select(p => p.Tenant).FirstOrDefault();

            if (defaultTenant != null)
            {
                claims.Add(new Claim("TenantId", defaultTenant.TenantId.ToString()));
                claims.Add(new Claim("TenantIds", string.Join(',', userTenants.Select(p => p.TenantId))));
                claims.Add(new Claim("companyId", defaultTenant.CompanyId.ToString()));

                claims.AddRange(GetUserPermission(subjectId, defaultTenant.TenantId, scopeList));
            }

            context.IssuedClaims = claims;
            return Task.FromResult(0);
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            var validGuidSubject = Guid.TryParse(context.Subject.GetSubjectId(), out Guid subjectId);
            if (validGuidSubject)
            {
                context.IsActive = _authService.Users().Where(p => p.User.SubjectId == subjectId).Select(p => p.UserProfile.IsActived && !p.UserProfile.IsDeleted).FirstOrDefault();
            }
            else
            {
                var externalClaims = context.Subject;
                var claims = externalClaims.Claims;
                context.IsActive = false;
            }

            return Task.FromResult(0);
        }

        private List<Claim> GetUserPermission(Guid userId, Guid tenantId, List<string> scopeList)
        {
            var query = _authDbContext.UserTenantRoles.Where(p => p.UserSubjectId == userId);
            query = query.Where(p => p.TenantId == tenantId);

            var roleIds = query.Select(p => p.RoleId).ToList();

            // check current package
            var companyId = _authDbContext.Tenant.Where(p => p.TenantId == tenantId).Select(p => p.CompanyId).FirstOrDefault();

            var dateTimeNow = DateTimeHelper.Now;         

            var currentPackageIds = _authDbContext.Subscriptions.Where(p => p.TenantId == tenantId)
                .Where(p => p.ActivedDate <= dateTimeNow && p.ExpiredDate > dateTimeNow).Select(p => p.PackageId)
                .ToList();


            var posApp = _authDbContext.Apps.Where(p => p.Code == StaticData.PosV2.Code).First();
            var posPackages = _authDbContext.Packages.Where(p => p.AppId == posApp.AppId).Select(p => p.PackageId).ToList();



            if (!currentPackageIds.Any(p => posPackages.Contains(p)))
            {
                // get previous packages - ONLY POS APP  

                var previousPackageId = _authDbContext.PosSubscriptions.Where(p => p.TenantId == tenantId && posPackages.Contains(p.PackageId) && p.ExpiredDate <= dateTimeNow)
                    .OrderByDescending(p => p.ExpiredDate).Select(p => p.PackageId).FirstOrDefault();

                if(previousPackageId > 0)
                    currentPackageIds.Add(previousPackageId);
            }

            var permissionPackages = (from pack in _authDbContext.PackagePermissions.Where(p => currentPackageIds.Contains(p.PackageId))
                                  join per in _authDbContext.PermissionsV2 on pack.PermissionId equals per.PermissionId
                                  select per.PermissionCode).Distinct().ToList();

            var permissionCodeQuery = _authDbContext.RolePermissionsV2.Where(p => roleIds.Contains(p.RoleId))
                .Include(p => p.Permission).Select(p => new { p.Permission.PermissionCode, p.Permission.Scope, p.PermissionId });


            permissionCodeQuery = permissionCodeQuery.Where(p => scopeList.Contains(p.Scope)); // dont check package permissions


            var permissionCodes = (from per1 in permissionCodeQuery.Select(p => p.PermissionCode).Distinct().ToList()
                                   join per2 in permissionPackages on per1 equals per2
                                   select per2).Distinct().ToList();

            var claims = permissionCodes.Select(p => new Claim("Permission", p)).ToList();
            return claims;

        }

    }


}
