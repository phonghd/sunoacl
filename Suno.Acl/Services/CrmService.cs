﻿using Newtonsoft.Json;
using Suno.Acl.Infrastructure;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace Suno.Acl.Services
{
    public class CrmService
    {
        private readonly CrmSettings _crmSettings;

        public CrmService(CrmSettings crmSettings)
        {
            _crmSettings = crmSettings;
        }

        public string SugarCRMLogin(WebClient client)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(new
            {
                user_auth = new { user_name = _crmSettings.UserName, password = CalculateMD5Hash(_crmSettings.Password) },
                application_name = "RestClient"
            });

            var values = new NameValueCollection();
            values["method"] = "login";
            values["input_type"] = "json";
            values["response_type"] = "json";
            values["rest_data"] = json;

            var response = client.UploadValues(_crmSettings.Host, values);

            var responseString = Encoding.Default.GetString(response);

            var loginResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(responseString);

            if (null == loginResponse || loginResponse.Count == 0)
                return string.Empty;

            string sessionId = loginResponse["id"];
            string userId = loginResponse["name_value_list"]["user_id"]["value"];

            return sessionId;
        }

        public string SugarCRMAddLead(WebClient client, string sessionId, string sunoId, string accountName, string firstName, string lastName, string phone, string email, string address, string province, string signupSource, string description, string bid, string sid)
        {
            string json = JsonConvert.SerializeObject(new
            {
                session = sessionId,
                module = "Leads",
                name_value_list = new
                {
                    sunoid_c = sunoId,
                    account_name = accountName,
                    first_name = firstName,
                    last_name = lastName,
                    phone_mobile = phone,
                    email1 = email,
                    jjwg_maps_address_c = address,
                    primary_address_state = province,
                    status = "",
                    tracking_url_c = signupSource,
                    description = description,
                    sid_c = sid,
                    created_by = bid
                }
            });

            var values = new NameValueCollection();
            values["method"] = "set_entry";
            values["input_type"] = "json";
            values["response_type"] = "json";
            values["rest_data"] = json;

            var response = client.UploadValues(_crmSettings.Host, values);

            var responseString = Encoding.Default.GetString(response);

            var loginResponse = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(responseString);

            if (null == loginResponse || loginResponse.Count == 0) return string.Empty;

            string leadId = loginResponse["id"];

            return leadId;
        }

        public void SugarCRMLogout(WebClient client, string sessionId)
        {
            var json = JsonConvert.SerializeObject(new
            {
                session = sessionId
            });

            var values = new NameValueCollection();
            values["method"] = "logout";
            values["input_type"] = "json";
            values["response_type"] = "json";
            values["rest_data"] = json;

            var response = client.UploadValues(_crmSettings.Host, values);

            var responseString = Encoding.Default.GetString(response);
        }

        private string CalculateMD5Hash(string input)
        {

            var md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }
}
