﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Linq;

namespace Suno.Acl.Services
{
    public class CollectingInfoService
    {
        public List<ClientInfo> CollectInfo(HttpContext context)
        {
            var info = new List<ClientInfo>();
            try
            {
                if (context.Request.QueryString.HasValue)
                {
                    info.AddRange(context.Request.Query.ToList().Select(p => new ClientInfo
                    {
                        Key = p.Key,
                        Value = p.Value
                    }).ToList());
                }

                if (context.Request.Headers.ContainsKey("User-Agent"))
                    info.Add(new ClientInfo { Key = "User-Agent", Value = context.Request.Headers["User-Agent"] });

                try
                {
                    info.Add(new ClientInfo { Key = "RemoteIp", Value = context.Connection.RemoteIpAddress.ToString() });
                }
                catch
                {

                }
            }
            catch
            {

            }
            return info;

        }
    }


    public class ClientInfo
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

}
