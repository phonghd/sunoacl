﻿using Suno.Core.Acl.Domain.V1;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Acl.Services.Models
{
    public class UserModel
    {
        public UserProfile UserProfile { get; set; }
        public Membership Membership { get; set; }
        public User User { get; set; }
    }
}
