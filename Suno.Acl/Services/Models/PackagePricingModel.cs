﻿using System.Collections.Generic;

namespace Suno.Acl.Services.Models
{
    public class PackagePricingModel
    {
        public int PackageId { get; set; }
        public List<string> Features { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
        public decimal Price { get; set; }
        public string PriceText { get; set; }
        public bool IsRecommended { get; set; }

    }
}
