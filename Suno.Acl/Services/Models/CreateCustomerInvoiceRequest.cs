﻿using System;

namespace Suno.Acl.Services.Models
{
    public class CreatePosSubscriptionRequest
    {
        public int PackageId { get; set; }
        public int Month { get; set; }
        public DateTime? ActivedDate { get; set; }
        public int MaxStore { get; set; }
    }
}

