﻿namespace Suno.Acl.Services.Models
{
    public class ActivityModel
    {
        public string Action { get; set; }
        public string Info { get; set; }
    }
}
