﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using ServiceStack;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Core.Acl.Domain;
using Suno.Core.Acl.Domain.V2;
using Feature = Suno.Core.Acl.Domain.V1.Feature;
using Package = Suno.Core.Acl.Domain.V1.Package;
using PackageFeatureLimit = Suno.Core.Acl.Domain.V1.PackageFeatureLimit;

namespace Suno.Acl.Services
{
    public class BasicDataService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public BasicDataService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public List<RoleV2> Roles = new List<RoleV2>
        {
            new RoleV2{ RoleId = 1, Name = "Chủ cửa hàng"},
            new RoleV2{ RoleId = 2, Name = "Quản lý"},
            new RoleV2{ RoleId = 3, Name = "Nhân viên kho"},
            new RoleV2{ RoleId = 4, Name = "Nhân viên thu ngân"},
            new RoleV2{ RoleId = 5, Name = "Nhân viên bán hàng"}
        };

        public List<PermissionV2> Permissions = new List<PermissionV2>
        {
            new PermissionV2 {PermissionCode = "Product_Create", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Product_Read", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 {PermissionCode = "Product_Update", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Product_Delete", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Product_Search", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Buy_Price_Update", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
            }},
            new PermissionV2 { PermissionCode = "Buy_Price_Read", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
            }},

            new PermissionV2 { PermissionCode = "Template_Setting", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
            }},

            new PermissionV2 { PermissionCode = "Stock_Transfer_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 {PermissionCode = "Stock_Transfer_Create", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Transfer_Update", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Transfer_Delete", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Inventory_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Card_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},


            new PermissionV2 { PermissionCode = "Pos_Order_Create", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 {PermissionCode = "Pos_Order_Read", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 {PermissionCode = "Pos_Order_Update", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Pos_Order_Delete", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Placed_Order_Create", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 {PermissionCode = "Placed_Order_Read", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Placed_Order_Update", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Placed_Order_Delete", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Sale_Return_Create", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Sale_Return_Read", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Sale_Return_Update", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Sale_Return_Delete", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},

            new PermissionV2 { PermissionCode = "Purchase_Order_Create", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Purchase_Order_Read", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Purchase_Order_Update", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Purchase_Order_Delete", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Purchase_Return_Create", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Purchase_Return_Read", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Purchase_Return_Update", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Purchse_Return_Delete", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},


            new PermissionV2 { PermissionCode = "Receipt_Voucher_Create", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}

            }},
            new PermissionV2 { PermissionCode = "Receipt_Voucher_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Receipt_Voucher_Update", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Receipt_Voucher_Delete", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Payment_Voucher_Create", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Payment_Voucher_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 {PermissionCode = "Payment_Voucher_Update", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Payment_Voucher_Delete", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Cashflow_Summary_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Cashflow_Ledger_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},



            new PermissionV2 { PermissionCode = "Customer_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Customer_Search", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Customer_Owner_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 {PermissionCode = "Customer_Owner_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Customer_Owner_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Customer_Owner_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Customer_Campaign_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Customer_Campaign_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Campaign_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Customer_Campaign_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Loyalty_Setting", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Campaign_Setting", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Search", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 {PermissionCode = "Supplier_Owner_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Supplier_All_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Ower_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 {PermissionCode = "Supplier_All_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Owner_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Supplier_All_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 {PermissionCode = "Supplier_Owner_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 {PermissionCode = "Supplier_All_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},


            new PermissionV2 {PermissionCode = "Promotion_Create", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Promotion_Search", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},



            new PermissionV2 { PermissionCode = "Promotion_Read", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Promotion_Update", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Promotion_Delete", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},


            new PermissionV2 { PermissionCode = "Employee_Create", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Employee_Read", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Employee_Update", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Employee_Delete", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Store_Setting", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Audit_Trail", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Pricing_Plan", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},


            new PermissionV2 { PermissionCode = "Daily_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Revenue_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Inventory_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3}
            }},
            new PermissionV2 {PermissionCode = "Employee_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Profit_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 {PermissionCode = "Profit_Loss_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},

            new PermissionV2 { PermissionCode = "Manage_Access", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},

            new PermissionV2 { PermissionCode = "Inventory_Count_Create", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Inventory_Count_Delete", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Inventory_Count_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Inventory_Count_Update", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},

            new PermissionV2 { PermissionCode = "Sale_Setting", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
            }},

            new PermissionV2 { PermissionCode = "Sale_Order_Search", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }}

        };

        public App SunoPosV2App = new App
        {
            Code = StaticData.PosV2.Code,
            Name = StaticData.PosV2.Name,
            Version = StaticData.PosV2.Version,

            Packages = new List<Package>
            {
                new Package
                {
                    CreatedDate = DateTimeHelper.Now,
                    CreatedUser = 0,
                    IsDefault = false,
                    IsFree = false,
                    IsTrial = false,
                    Key = "PosV2Basic",
                    Name = "Pos Basic",
                    PackageId = 10
                },
                new Package
                {
                    CreatedDate = DateTimeHelper.Now,
                    CreatedUser = 0,
                    IsDefault = true,
                    IsFree = false,
                    IsTrial = false,
                    Key = "PosV2Pro",
                    Name = "Pos Pro",
                    PackageId = 11
                }
            }
        };

        public App OmniApp = new App
        {
            Code = StaticData.Omni.Code,
            Name = StaticData.Omni.Name,
            Version = StaticData.Omni.Version,

            Packages = new List<Package>
            {
                new Package
                {
                    CreatedDate = DateTimeHelper.Now,
                    CreatedUser = 0,
                    IsDefault = true,
                    IsFree = true,
                    IsTrial = false,
                    Key = "OmniFree",
                    Name = "Omni Free",
                    PackageId = 12
                }
            }
        };

        public void SeedData(AuthDbContext authDbContext)
        {
            if (!authDbContext.Clients.Any())
            {
                authDbContext.Clients.Add(new Client
                {
                    ClientId = "suno_web_pos_local",
                    ClientName = "Suno Pos - Local",
                    AccessTokenLifetime = 86400,
                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true,
                    ClientUri = "http://localhost:4200,http://localhost:4200/silent-renew.html,http://localhost:4200/auth-callback",
                    IdentityTokenLifetime = 90000,
                    RequireConsent = false,
                    Enabled = true,
                    RedirectUris = "http://localhost:4200,http://localhost:4200/silent-renew.html,http://localhost:4200/auth-callback",
                    PostLogoutRedirectUris = "http://localhost:4200",
                    ClientScopes = (new List<string> { "openid", "offline_access", "productCatalog", "MasterData", "inventory", "pricing", "cashflow", "sales", "purchase", "crm", "Report" })
                    .Select(p => new ClientScope { Scope = p }).ToList(),
                    ClientGrantTypes = new List<ClientGrantType> { new ClientGrantType { GrantType = "implicit" } }
                });

                authDbContext.Clients.Add(new Client
                {
                    ClientId = "suno_web_pos_production",
                    ClientName = "Suno Pos - Production",
                    AccessTokenLifetime = 86400,
                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true,
                    ClientUri = "https://app.suno.vn,https://app.suno.vn/silent-renew.html,https://app.suno.vn/auth-callback",
                    IdentityTokenLifetime = 90000,
                    RequireConsent = false,
                    Enabled = true,
                    RedirectUris = "https://app.suno.vn,https://app.suno.vn/silent-renew.html,https://app.suno.vn/auth-callback",
                    PostLogoutRedirectUris = "https://app.suno.vn",
                    ClientScopes = (new List<string> { "openid", "offline_access", "productCatalog", "MasterData", "inventory", "pricing", "cashflow", "sales", "purchase", "crm", "Report" })
                    .Select(p => new ClientScope { Scope = p }).ToList(),
                    ClientGrantTypes = new List<ClientGrantType> { new ClientGrantType { GrantType = "implicit" } }
                });

                authDbContext.Clients.Add(new Client
                {
                    ClientId = "suno_web_admin_production",
                    ClientName = "Suno Admin",
                    AccessTokenLifetime = 86400,
                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true,
                    ClientUri = "https://admin2.suno.vn/app,https://admin2.suno.vn/app/silent-renew.html,https://admin2.suno.vn/app/auth-callback",
                    IdentityTokenLifetime = 90000,
                    RequireConsent = false,
                    Enabled = true,
                    RedirectUris = "https://admin2.suno.vn/app,https://admin2.suno.vn/app/silent-renew.html,https://admin2.suno.vn/app/auth-callback",
                    PostLogoutRedirectUris = "https://admin2.suno.vn/app",
                    ClientScopes = (new List<string> { "openid", "offline_access", "admin" })
                    .Select(p => new ClientScope { Scope = p }).ToList(),
                    ClientGrantTypes = new List<ClientGrantType> { new ClientGrantType { GrantType = "implicit" } }
                });

                authDbContext.SaveChanges();
            }

            var sunoPosV2App = authDbContext.Apps.Where(p => p.Code == SunoPosV2App.Code).Select(p => p.AppId).FirstOrDefault();
            if (sunoPosV2App == 0)
            {
                var app = SunoPosV2App.CreateCopy();
                authDbContext.Apps.Add(app);
                authDbContext.SaveChanges();
                sunoPosV2App = app.AppId;
            }

            var sunoOmniApp = authDbContext.Apps.Where(p => p.Code == OmniApp.Code).Select(p => p.AppId).FirstOrDefault();
            if (sunoOmniApp == 0)
            {
                var app = OmniApp.CreateCopy();
                authDbContext.Apps.Add(app);
                authDbContext.SaveChanges();
                sunoOmniApp = app.AppId;

                authDbContext.SaveChanges();
            }

            authDbContext.SaveChanges();

            Roles.ForEach(role =>
            {
                if (!authDbContext.RolesV2.Where(p => p.RoleId == role.RoleId).Select(p => true).FirstOrDefault())
                {
                    var sql = $"SET IDENTITY_INSERT [{AuthDbContext.schemaV2}].[Roles] ON; insert  [{AuthDbContext.schemaV2}].[Roles] ( RoleId, Name, CreatedDate, IsGlobal, CreatedUser, IsDeleted) VALUES( {role.RoleId} , N'{role.Name}', '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', 1, 0, 0); SET IDENTITY_INSERT [{AuthDbContext.schemaV2}].[Roles] OFF";
                    authDbContext.Database.ExecuteSqlCommand(sql);
                }
            });

            Permissions.ForEach(permission =>
            {
                if (!authDbContext.PermissionsV2.Where(p => p.PermissionCode == permission.PermissionCode).Select(p => true).FirstOrDefault())
                {
                    permission.CreatedDate = DateTimeHelper.Now;
                    authDbContext.PermissionsV2.Add(permission);
                }
            });

            authDbContext.SaveChanges();

            if (!authDbContext.PackagePermissions.Any())
            {
                Dictionary<int, List<string>> packages = new Dictionary<int, List<string>>();
                packages.Add(10, new List<string>
                {
                    "Manage_Access", "Product_Create", "Product_Read", "Product_Update", "Product_Delete", "Product_Search",
                    "Buy_Price_Read", "Buy_Price_Update", "Pos_Order_Create", "Pos_Order_Read", "Pos_Order_Update", "Pos_Order_Delete",
                    "Sale_Return_Create", "Sale_Return_Read", "Sale_Return_Update", "Sale_Return_Delete",
                    "Inventory_Count_Read", "Inventory_Count_Create", "Inventory_Count_Update", "Inventory_Count_Delete",
                    "Stock_Transfer_Read", "Stock_Transfer_Create", "Stock_Transfer_Update", "Stock_Transfer_Delete", "Inventory_Read",
                    "Stock_Card_Read", "Customer_Create", "Customer_Search", "Customer_Owner_Read", "Customer_All_Read", "Customer_Owner_Update",
                    "Customer_All_Update", "Customer_Owner_Delete", "Customer_All_Delete", "Customer_Owner_Export", "Customer_All_Export",
                    "Customer_Group_Create", "Customer_Group_Read", "Customer_Group_Update", "Customer_Group_Delete", "Customer_Campaign_Create",
                    "Customer_Campaign_Read", "Customer_Campaign_Update", "Customer_Campaign_Delete", "Customer_Loyalty_Setting", "Customer_Campaign_Setting",
                    "Supplier_Search", "Supplier_Create", "Supplier_Owner_Read", "Supplier_All_Read", "Supplier_Ower_Update", "Supplier_All_Update",
                    "Supplier_Owner_Delete", "Supplier_All_Delete", "Supplier_Owner_Export", "Supplier_All_Export", "Daily_Report", "Revenue_Report",
                    "Inventory_Report", "Employee_Create", "Employee_Read", "Employee_Update", "Employee_Delete", "Store_Setting", "Sale_Setting", "Template_Setting", "Audit_Trail", "Pricing_Plan",
                    "Sale_Order_Search", "Promotion_Search"


                });
                packages.Add(11, new List<string>
                {
                    "Manage_Access", "Product_Create", "Product_Read", "Product_Update", "Product_Delete", "Product_Search",
                    "Buy_Price_Read", "Buy_Price_Update", "Inventory_Count_Read", "Inventory_Count_Create", "Inventory_Count_Update",
                    "Inventory_Count_Delete", "Stock_Transfer_Read", "Stock_Transfer_Create", "Stock_Transfer_Update",
                    "Stock_Transfer_Delete", "Inventory_Read", "Stock_Card_Read", "Pos_Order_Create", "Pos_Order_Read",
                    "Pos_Order_Update", "Pos_Order_Delete", "Placed_Order_Create", "Placed_Order_Read", "Placed_Order_Update",
                    "Placed_Order_Delete", "Sale_Return_Create", "Sale_Return_Read", "Sale_Return_Update", "Sale_Return_Delete",
                    "Purchase_Order_Create", "Purchase_Order_Read", "Purchase_Order_Update", "Purchase_Order_Delete",
                    "Purchase_Return_Create", "Purchase_Return_Read", "Purchase_Return_Update", "Purchse_Return_Delete",
                    "Receipt_Voucher_Create", "Receipt_Voucher_Read", "Receipt_Voucher_Update", "Receipt_Voucher_Delete",
                    "Payment_Voucher_Create", "Payment_Voucher_Read", "Payment_Voucher_Update", "Payment_Voucher_Delete",
                    "Cashflow_Summary_Read", "Cashflow_Ledger_Read", "Customer_Create", "Customer_Search", "Customer_Owner_Read",
                    "Customer_All_Read", "Customer_Owner_Update", "Customer_All_Update", "Customer_Owner_Delete", "Customer_All_Delete",
                    "Customer_Owner_Export", "Customer_All_Export", "Customer_Group_Create", "Customer_Group_Read", "Customer_Group_Update",
                    "Customer_Group_Delete", "Customer_Campaign_Create", "Customer_Campaign_Read", "Customer_Campaign_Update",
                    "Customer_Campaign_Delete", "Customer_Loyalty_Setting", "Customer_Campaign_Setting", "Supplier_Search",
                    "Supplier_Create", "Supplier_Owner_Read", "Supplier_All_Read", "Supplier_Ower_Update", "Supplier_All_Update",
                    "Supplier_Owner_Delete", "Supplier_All_Delete", "Supplier_Owner_Export", "Supplier_All_Export",
                    "Promotion_Create", "Promotion_Read", "Promotion_Update", "Promotion_Delete", "Employee_Create",
                    "Employee_Read", "Employee_Update", "Employee_Delete", "Store_Setting", "Sale_Setting", "Template_Setting",
                    "Audit_Trail", "Pricing_Plan", "Daily_Report", "Revenue_Report", "Inventory_Report", "Employee_Report",
                    "Profit_Report", "Profit_Loss_Report",
                    "Promotion_Search", "Sale_Order_Search"
                });


                var allPermissions = authDbContext.PermissionsV2.Select(p => new { p.PermissionId, p.PermissionCode }).ToList();

                packages.Keys.ToList().ForEach(packageId =>
                {
                    packages[packageId].ForEach(permissionCode =>
                    {
                        var permission = allPermissions.Where(p => p.PermissionCode == permissionCode).FirstOrDefault();
                        if (permission != null)
                            authDbContext.PackagePermissions.Add(new PackagePermission { PackageId = packageId, PermissionId = permission.PermissionId });
                    });
                });

                authDbContext.SaveChanges();

            }

        }
    }
}
