﻿using System;
using System.Collections.Generic;
namespace Suno.Acl.Data
{
    public class Paging<TEntity>
    {
        IEnumerable<TEntity> _items;
        int _totalCount;

        public Paging(IEnumerable<TEntity> items, int totalCount)
        {
            _items = items;
            _totalCount = totalCount;
        }

        public IEnumerable<TEntity> Items { get { return _items; } }
        public int TotalCount { get { return _totalCount; } }
    }
}


