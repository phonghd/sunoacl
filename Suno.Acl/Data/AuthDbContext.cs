﻿using Microsoft.EntityFrameworkCore;
using Suno.Core.Acl.Domain.V1;
using Suno.Core.Acl.Domain.V2;


namespace Suno.Acl.Data
{


    public class AuthDbContext : DbContext
    {
        public static string schemaV2 = "jup";
        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options)
        {
            //this.Database.EnsureCreated();
        }

        #region "V1"

        public DbSet<Membership> Memberships { get; set; }
        public DbSet<BillingOrderDetail> BillingOrderDetails { get; set; }
        public DbSet<BillingOrderTransaction> BillingOrderTransactions { get; set; }
        public DbSet<BillingPackage> BillingPackages { get; set; }
        public DbSet<BillingPaymentProvider> BillingPaymentProviders { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyContract> CompanyContracts { get; set; }
        public DbSet<CompanyExtension> CompanyExtensions { get; set; }
        public DbSet<CompanyFeatureLimit> CompanyFeatureLimits { get; set; }
        public DbSet<CompanyLoginTrace> CompanyLoginTraces { get; set; }
        public DbSet<CompanyPackage> CompanyPackages { get; set; }
        public DbSet<Core.Acl.Domain.V1.Config> Configs { get; set; }
        public DbSet<Feature> Features { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<FileInfo> FileInfos { get; set; }
        public DbSet<Industry> Industries { get; set; }
        public DbSet<Invitation> Invitations { get; set; }
        public DbSet<LoginTrace> LoginTrace { get; set; }


        public DbSet<OAuth2CryptoKey> OAuth2CryptoKeys { get; set; }
        public DbSet<OAuthMembership> OAuthMemberships { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<PackageFeature> PackageFeatures { get; set; }
        public DbSet<PackageFeatureLimit> PackageFeatureLimits { get; set; }
        public DbSet<PackagePlan> PackagePlans { get; set; }
        public DbSet<Permission> Permissions { get; set; }

        public DbSet<POSIM_UserInStore> POSIM_UserInStores { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Recommend> Recommend { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<Signup> Signups { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }

        public DbSet<MailEntry> MailEntry { get; set; }
        public DbSet<MailEntryParameter> MailEntryParameter { get; set; }
        public DbSet<MailQueue> MailQueue { get; set; }
        public DbSet<MailQueueHistory> MailQueueHistory { get; set; }
        public DbSet<MailQueueHistoryErrorLog> MailQueueHistoryErrorLog { get; set; }
        public DbSet<MailTemplate> MailTemplate { get; set; }
        public DbSet<MailTenant> MailTenant { get; set; }
        #endregion

        #region "V2"
        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientScope> ClientScopes { get; set; }
        public DbSet<ClientGrantType> ClientGrantTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Tenant> Tenant { get; set; }
        public DbSet<UserClaim> UserClaims { get; set; }
        public DbSet<Store> Stores { get; set; }
        public DbSet<UserTenantStore> UserTenantStores { get; set; }
        public DbSet<UserTenant> UserTenants { get; set; }
        //public DbSet<Subscription> Subscriptions { get; set; }
        //public DbSet<SubscriptionLog> SubscriptionLogs { get; set; }

        public DbSet<RoleV2> RolesV2 { get; set; }
        public DbSet<PermissionV2> PermissionsV2 { get; set; }
        public DbSet<RolePermissionV2> RolePermissionsV2 { get; set; }
        public DbSet<UserTenantRoleV2> UserTenantRoles { get; set; }

        public DbSet<App> Apps { get; set; }
        public DbSet<UserSetting> Settings { get; set; }

        public DbSet<PosInvoiceDetail> SubscriptionInvoiceDetails { get; set; }
        public DbSet<OmnichannelInvoiceDetail> OmnichannelInvoiceDetails { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<InvoiceDetail> InvoiceDetails { get; set; }
        public DbSet<InvoicePayment> InvoicePayments { get; set; }

        public DbSet<PosSubscription> PosSubscriptions { get; set; }
        public DbSet<OmniSubscription> OmniSubscriptions { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }

        public DbSet<PackagePermission> PackagePermissions { get; set; }

        public DbSet<ClientInfo> ClientInfos { get; set; }
        public DbSet<ServiceRequest> ServiceRequests { get; set; }



        #endregion

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region "V1"
            modelBuilder.Entity<Membership>().ToTable("Membership").HasKey(p => p.UserId);

            modelBuilder.Entity<BillingOrderDetail>().ToTable("BillingOrderDetail").HasKey(p => p.Id);
            modelBuilder.Entity<BillingOrderTransaction>().ToTable("BillingOrderTransaction").HasKey(p => p.Id);
            modelBuilder.Entity<BillingPackage>().ToTable("BillingPackage").HasKey(p => p.Id);
            modelBuilder.Entity<BillingPaymentProvider>().ToTable("BillingPaymentProvider").HasKey(p => p.id);

            modelBuilder.Entity<Company>().ToTable("Company").HasKey(p => p.CompanyId);
            modelBuilder.Entity<Company>().HasOne(p => p.IndustryObj).WithMany(p => p.Companies).HasForeignKey(p => p.Industry);

            modelBuilder.Entity<CompanyContract>().ToTable("CompanyContract").HasKey(p => p.Id);
            modelBuilder.Entity<CompanyExtension>().ToTable("CompanyExtension").HasKey(p => p.CompanyId);
            modelBuilder.Entity<CompanyFeatureLimit>().ToTable("CompanyFeatureLimit").HasKey(p => p.CompanyId);
            modelBuilder.Entity<CompanyLoginTrace>().ToTable("CompanyLoginTrace").HasKey(p => p.CompanyId);
            modelBuilder.Entity<CompanyPackage>().ToTable("CompanyPackage").HasKey(p => p.Id);
            modelBuilder.Entity<Core.Acl.Domain.V1.Config>().ToTable("Config").HasKey(p => p.Id);
            modelBuilder.Entity<Feature>().ToTable("Feature").HasKey(p => p.FeatureId);
            modelBuilder.Entity<Feedback>().ToTable("Feedback").HasKey(p => p.Id);
            modelBuilder.Entity<Feedback>().Property(p => p.FeedbackContent).HasColumnName("Feedback");

            modelBuilder.Entity<File>().ToTable("File").HasKey(p => p.Id);
            modelBuilder.Entity<FileInfo>().ToTable("FileInfo").HasKey(p => p.Id);
            modelBuilder.Entity<Industry>().ToTable("Industry").HasKey(p => p.Id);

            modelBuilder.Entity<Invitation>().ToTable("Invitation").HasKey(p => p.InvitationId);
            modelBuilder.Entity<Invitation>().HasOne(p => p.UserSender).WithMany(p => p.Invitations).HasForeignKey(p => p.Sender);


            modelBuilder.Entity<LoginTrace>().ToTable("LoginTrace").HasKey(p => p.Id);

            modelBuilder.Entity<OAuth2CryptoKey>().ToTable("OAuth2CryptoKey").HasKey(p => p.Id);

            modelBuilder.Entity<OAuthMembership>().ToTable("OAuthMembership").HasKey(p => p.Id);

            modelBuilder.Entity<Package>().ToTable("Package").HasKey(p => p.PackageId);

            modelBuilder.Entity<PackageFeature>().ToTable("PackageFeature").HasKey(p => p.Id);
            modelBuilder.Entity<PackageFeature>().HasOne(p => p.Package).WithMany(p => p.PackageFeatures).HasForeignKey(p => p.PackageId);
            modelBuilder.Entity<PackageFeature>().HasOne(p => p.Feature).WithMany(p => p.PackageFeatures).HasForeignKey(p => p.FeatureId);

            modelBuilder.Entity<PackageFeatureLimit>().ToTable("PackageFeatureLimit").HasKey(p => p.PackageId);
            modelBuilder.Entity<PackagePlan>().ToTable("PackagePlan").HasKey(p => p.Id);
            modelBuilder.Entity<Permission>().ToTable("Permission").HasKey(p => p.PermissionID);

            modelBuilder.Entity<POSIM_UserInStore>().ToTable("POSIM_UserInStore").HasKey(p => p.Id);
            modelBuilder.Entity<Province>().ToTable("Province").HasKey(p => p.ProvinceId);
            modelBuilder.Entity<Rating>().ToTable("Rating").HasKey(p => p.Id);
            modelBuilder.Entity<Recommend>().ToTable("Recommend").HasKey(p => p.Id);
            modelBuilder.Entity<Role>().ToTable("Role").HasKey(p => p.RoleId);
            modelBuilder.Entity<RolePermission>().ToTable("RolePermission").HasKey(p => p.Id);
            modelBuilder.Entity<Signup>().ToTable("Signup").HasKey(p => p.Id);

            modelBuilder.Entity<UserProfile>().ToTable("UserProfile").HasKey(p => p.UserId);


            modelBuilder.Entity<UserRole>().ToTable("UserRole").HasKey(p => p.Id);
            modelBuilder.Entity<UserRole>().HasOne(p => p.UserProfile).WithMany(p => p.UserRoles).HasForeignKey(p => p.UserId);


            modelBuilder.Entity<MailEntry>().ToTable("MailEntry").HasKey(p => p.Id);
            modelBuilder.Entity<MailEntryParameter>().ToTable("MailEntryParameter").HasKey(p => p.Id);
            modelBuilder.Entity<MailQueue>().ToTable("MailQueue").HasKey(p => p.Id);

            modelBuilder.Entity<MailQueueHistory>().ToTable("MailQueueHistory").HasKey(p => p.Id);

            modelBuilder.Entity<MailQueueHistoryErrorLog>().ToTable("MailQueueHistoryErrorLog").HasKey(p => p.Id);

            modelBuilder.Entity<MailTemplate>().ToTable("MailTemplate").HasKey(p => p.Id);
            modelBuilder.Entity<MailTenant>().ToTable("MailTenant").HasKey(p => p.Id);



            #endregion

            #region "Update V1"
            modelBuilder.Entity<UserProfile>().HasOne(p => p.User).WithOne(p => p.UserProfile).HasForeignKey<User>(p => p.UserId);
            #endregion

            #region "V2"

            modelBuilder.Entity<Client>().ToTable("Clients", schema: schemaV2).HasKey(p => p.Id);
            modelBuilder.Entity<ClientScope>().ToTable("ClientScopes", schema: schemaV2).HasKey(p => p.Id);
            modelBuilder.Entity<ClientGrantType>().ToTable("ClientGrantTypes", schema: schemaV2).HasKey(p => p.Id);

            modelBuilder.Entity<User>().ToTable("Users", schema: schemaV2).HasKey(p => p.SubjectId);
            modelBuilder.Entity<Tenant>().ToTable("Tenants", schema: schemaV2).HasKey(p => p.TenantId);

            modelBuilder.Entity<UserClaim>().ToTable("UserClaims", schema: schemaV2).HasKey(p => p.Id);
            modelBuilder.Entity<UserClaim>().HasOne(p => p.User).WithMany(p => p.UserClaims).HasForeignKey(p => p.UserSubjectId);

            modelBuilder.Entity<Store>().ToTable("Stores", schema: schemaV2).HasKey(p => p.StoreId);
            modelBuilder.Entity<UserTenantStore>().ToTable("UserTenantStores", schema: schemaV2).HasKey(p => new { p.UserSubjectId, p.StoreId, p.TenantId });
            modelBuilder.Entity<UserTenantStore>().HasOne(p => p.User).WithMany(p => p.UserTenantStores).HasForeignKey(p => p.UserSubjectId);
            modelBuilder.Entity<UserTenantStore>().HasOne(p => p.Store).WithMany(p => p.UserTenantStores).HasForeignKey(p => p.StoreId);
            modelBuilder.Entity<UserTenantStore>().HasOne(p => p.Tenant).WithMany(p => p.UserTenantStores).HasForeignKey(p => p.TenantId);



            modelBuilder.Entity<UserTenant>().ToTable("UserTenants", schemaV2).HasKey(p => new { p.TenantId, p.UserSubjectId });
            modelBuilder.Entity<UserTenant>().HasOne(p => p.User).WithMany(p => p.UserTenants).HasForeignKey(p => p.UserSubjectId);
            modelBuilder.Entity<UserTenant>().HasOne(p => p.Tenant).WithMany(p => p.UserTenants).HasForeignKey(p => p.TenantId);

            //modelBuilder.Entity<Tenant>().ToTable("Tenants", schemaV2).HasOne(h => h.Subscription).WithOne(p => p.Tenant).HasForeignKey<Subscription>(p => p.TenantId).OnDelete(DeleteBehavior.Cascade);

            //modelBuilder.Entity<Subscription>().ToTable("Subscriptions", schema: schemaV2).HasKey(p => p.Id);
            //modelBuilder.Entity<SubscriptionLog>().ToTable("SubscriptionLogs", schema: schemaV2).HasKey(p => p.Id);


            modelBuilder.Entity<RoleV2>().ToTable("Roles", schema: schemaV2).HasKey(p => p.RoleId);
            modelBuilder.Entity<PermissionV2>().ToTable("Permissions", schema: schemaV2).HasKey(p => p.PermissionId);
            modelBuilder.Entity<RolePermissionV2>().ToTable("RolePermissions", schema: schemaV2).HasKey(p => p.Id);
            modelBuilder.Entity<RolePermissionV2>().HasOne(p => p.Role).WithMany(p => p.RolePermissions).HasForeignKey(p => p.RoleId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<RolePermissionV2>().HasOne(p => p.Permission).WithMany(p => p.RolePermissions).HasForeignKey(p => p.PermissionId).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<UserTenantRoleV2>().ToTable("UserTenantRoles", schemaV2).HasKey(p => new { p.TenantId, p.UserSubjectId, p.RoleId });
            modelBuilder.Entity<UserTenantRoleV2>().HasOne(p => p.User).WithMany(p => p.UserTenantRoles).HasForeignKey(p => p.UserSubjectId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<UserTenantRoleV2>().HasOne(p => p.Role).WithMany(p => p.UserTenantRoles).HasForeignKey(p => p.RoleId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<UserTenantRoleV2>().HasOne(p => p.Tenant).WithMany(p => p.UserTenantRoles).HasForeignKey(p => p.TenantId).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<App>().ToTable("App", schemaV2).HasKey(p => p.AppId);
            modelBuilder.Entity<App>().Property(p => p.Code).HasMaxLength(16);

            modelBuilder.Entity<Package>().HasOne(p => p.App).WithMany(p => p.Packages).HasForeignKey(p => p.AppId);
            modelBuilder.Entity<UserSetting>().ToTable("UserSettings", schemaV2).HasKey(p => p.Id);





            modelBuilder.Entity<PosInvoiceDetail>().ToTable("PosInvoiceDetails", schemaV2);
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.ActivedDate).HasColumnName("Pos_ActivedDate");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.CompanyId).HasColumnName("Pos_CompanyId");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.PackageId).HasColumnName("Pos_PackageId");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.ExpiredDate).HasColumnName("Pos_ExpiredDate");

            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.MaxEmailPerMonth).HasColumnName("Pos_MaxEmailPerMonth");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.MaxUser).HasColumnName("Pos_MaxUser");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.MaxStore).HasColumnName("Pos_MaxStore");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.MaxSaleOrderPerDay).HasColumnName("Pos_MaxSaleOrderPerDay");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.MaxProduct).HasColumnName("Pos_MaxProduct");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.MaxProductItem).HasColumnName("Pos_MaxProductItem");
            modelBuilder.Entity<PosInvoiceDetail>().Property(p => p.MaxCustomer).HasColumnName("Pos_MaxCustomer");


            modelBuilder.Entity<OmnichannelInvoiceDetail>().ToTable("OmnichannelInvoiceDetails", schemaV2);
            modelBuilder.Entity<OmnichannelInvoiceDetail>().Property(p => p.ActivedDate).HasColumnName("Omni_ActivedDate");
            modelBuilder.Entity<OmnichannelInvoiceDetail>().Property(p => p.CompanyId).HasColumnName("Omni_CompanyId");
            modelBuilder.Entity<OmnichannelInvoiceDetail>().Property(p => p.PackageId).HasColumnName("Omni_PackageId");
            modelBuilder.Entity<OmnichannelInvoiceDetail>().Property(p => p.ExpiredDate).HasColumnName("Omni_ExpiredDate");

            modelBuilder.Entity<InvoiceDetail>().ToTable("InvoiceDetails", schemaV2).HasKey(p => p.Id);


            //public DbSet<PosSubscription> PosSubscriptions { get; set; }
            //public DbSet<OmniSubscription> OmniSubscriptions { get; set; }
            //public DbSet<Subscription> Subscriptions { get; set; }




            modelBuilder.Entity<PosSubscription>().ToTable("PosSubscriptions", schemaV2);
            modelBuilder.Entity<PosSubscription>().Property(p => p.MaxEmailPerMonth).HasColumnName("Pos_MaxEmailPerMonth");
            modelBuilder.Entity<PosSubscription>().Property(p => p.MaxUser).HasColumnName("Pos_MaxUser");
            modelBuilder.Entity<PosSubscription>().Property(p => p.MaxStore).HasColumnName("Pos_MaxStore");
            modelBuilder.Entity<PosSubscription>().Property(p => p.MaxSaleOrderPerDay).HasColumnName("Pos_MaxSaleOrderPerDay");
            modelBuilder.Entity<PosSubscription>().Property(p => p.MaxProduct).HasColumnName("Pos_MaxProduct");
            modelBuilder.Entity<PosSubscription>().Property(p => p.MaxProductItem).HasColumnName("Pos_MaxProductItem");
            modelBuilder.Entity<PosSubscription>().Property(p => p.MaxCustomer).HasColumnName("Pos_MaxCustomer");

            modelBuilder.Entity<OmniSubscription>().ToTable("OmniSubscriptions", schemaV2);

            modelBuilder.Entity<Subscription>().ToTable("Subscriptions", schemaV2).HasKey(p => p.Id);


            modelBuilder.Entity<Invoice>().ToTable("Invoices", schemaV2).HasKey(p => p.Id);
            modelBuilder.Entity<InvoicePayment>().ToTable("InvoicePayments", schemaV2).HasKey(p => p.Id);

            modelBuilder.Entity<PackagePermission>().ToTable("PackagePermissions", schemaV2).HasKey(p => new { p.PackageId, p.PermissionId });

            modelBuilder.Entity<ClientInfo>().ToTable("ClientInfo", schemaV2).HasKey(p => p.Id);

            modelBuilder.Entity<ServiceRequest>().ToTable("ServiceRequest", schemaV2).HasKey(p => p.Id);
            #endregion
        }
    }
}
