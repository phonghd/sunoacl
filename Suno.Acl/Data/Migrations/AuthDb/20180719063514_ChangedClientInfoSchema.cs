﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class ChangedClientInfoSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "ClientInfo",
                newName: "ClientInfo",
                newSchema: "jup");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "ClientInfo",
                schema: "jup",
                newName: "ClientInfo");
        }
    }
}
