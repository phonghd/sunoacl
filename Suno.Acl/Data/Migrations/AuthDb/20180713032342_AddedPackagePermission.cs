﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class AddedPackagePermission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PackagePermissions",
                schema: "jup",
                columns: table => new
                {
                    PackageId = table.Column<int>(nullable: false),
                    PermissionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PackagePermissions", x => new { x.PackageId, x.PermissionId });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PackagePermissions",
                schema: "jup");
        }
    }
}
