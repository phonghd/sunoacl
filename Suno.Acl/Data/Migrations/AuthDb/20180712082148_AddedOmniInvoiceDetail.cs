﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class AddedOmniInvoiceDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Omni_ActivedDate",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Omni_CompanyId",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Omni_ExpiredDate",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Omni_PackageId",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Omni_ActivedDate",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Omni_CompanyId",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Omni_ExpiredDate",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Omni_PackageId",
                schema: "jup",
                table: "InvoiceDetails");
        }
    }
}
