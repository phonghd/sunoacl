﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class AddedInvoiceDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_SubscriptionInvoiceDetails",
                schema: "jup",
                table: "SubscriptionInvoiceDetails");

            migrationBuilder.RenameTable(
                name: "SubscriptionInvoiceDetails",
                schema: "jup",
                newName: "InvoiceDetails");

            migrationBuilder.AlterColumn<int>(
                name: "PackageId",
                schema: "jup",
                table: "InvoiceDetails",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpiredDate",
                schema: "jup",
                table: "InvoiceDetails",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                schema: "jup",
                table: "InvoiceDetails",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<DateTime>(
                name: "ActivedDate",
                schema: "jup",
                table: "InvoiceDetails",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                schema: "jup",
                table: "InvoiceDetails",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InvoiceDetails",
                schema: "jup",
                table: "InvoiceDetails",
                column: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_InvoiceDetails",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.RenameTable(
                name: "InvoiceDetails",
                schema: "jup",
                newName: "SubscriptionInvoiceDetails");

            migrationBuilder.AlterColumn<int>(
                name: "PackageId",
                schema: "jup",
                table: "SubscriptionInvoiceDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ExpiredDate",
                schema: "jup",
                table: "SubscriptionInvoiceDetails",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CompanyId",
                schema: "jup",
                table: "SubscriptionInvoiceDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "ActivedDate",
                schema: "jup",
                table: "SubscriptionInvoiceDetails",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubscriptionInvoiceDetails",
                schema: "jup",
                table: "SubscriptionInvoiceDetails",
                column: "Id");
        }
    }
}
