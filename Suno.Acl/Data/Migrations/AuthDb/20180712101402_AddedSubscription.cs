﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class AddedSubscription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Subscriptions",
                schema: "jup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    TenantId = table.Column<Guid>(nullable: false),
                    PackageId = table.Column<int>(nullable: false),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    Pos_MaxEmailPerMonth = table.Column<int>(nullable: true),
                    Pos_MaxUser = table.Column<int>(nullable: true),
                    Pos_MaxStore = table.Column<int>(nullable: true),
                    Pos_MaxSaleOrderPerDay = table.Column<int>(nullable: true),
                    Pos_MaxProduct = table.Column<int>(nullable: true),
                    Pos_MaxProductItem = table.Column<int>(nullable: true),
                    Pos_MaxCustomer = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscriptions", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Subscriptions",
                schema: "jup");
        }
    }
}
