﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class AddedSubscriptionLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SubscriptionLogs",
                schema: "v2",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExpiredDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    MaxEmailPerMonth = table.Column<int>(type: "int", nullable: true),
                    MaxProduct = table.Column<int>(type: "int", nullable: true),
                    MaxProductItem = table.Column<int>(type: "int", nullable: true),
                    MaxSaleOrderPerDay = table.Column<int>(type: "int", nullable: true),
                    MaxStore = table.Column<int>(type: "int", nullable: true),
                    MaxUser = table.Column<int>(type: "int", nullable: true),
                    Note = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OwnerId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    SubscriptionId = table.Column<int>(type: "int", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubscriptionLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubscriptionLogs_Users_OwnerId",
                        column: x => x.OwnerId,
                        principalSchema: "v2",
                        principalTable: "Users",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubscriptionLogs_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "v2",
                        principalTable: "Tenants",
                        principalColumn: "TenantId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionLogs_OwnerId",
                schema: "v2",
                table: "SubscriptionLogs",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionLogs_TenantId",
                schema: "v2",
                table: "SubscriptionLogs",
                column: "TenantId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubscriptionLogs",
                schema: "v2");
        }
    }
}
