﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class DropSubscriptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SubscriptionLogs",
                schema: "jup");

            migrationBuilder.DropTable(
                name: "Subscriptions",
                schema: "jup");

            migrationBuilder.DropColumn(
                name: "Version",
                schema: "jup",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Version",
                schema: "jup",
                table: "Tenants");

            migrationBuilder.AddColumn<int>(
                name: "AppId",
                schema: "jup",
                table: "Users",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AppId",
                schema: "jup",
                table: "Tenants",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxCustomer",
                table: "PackageFeatureLimit",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "AppId",
                table: "Package",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "Package",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "AppId",
                table: "Feature",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "App",
                schema: "jup",
                columns: table => new
                {
                    AppId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Code = table.Column<string>(type: "nvarchar(16)", maxLength: 16, nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Version = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_App", x => x.AppId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_AppId",
                schema: "jup",
                table: "Users",
                column: "AppId");

            migrationBuilder.CreateIndex(
                name: "IX_Tenants_AppId",
                schema: "jup",
                table: "Tenants",
                column: "AppId");

            migrationBuilder.CreateIndex(
                name: "IX_Package_AppId",
                table: "Package",
                column: "AppId");

            migrationBuilder.CreateIndex(
                name: "IX_Feature_AppId",
                table: "Feature",
                column: "AppId");

            migrationBuilder.AddForeignKey(
                name: "FK_Feature_App_AppId",
                table: "Feature",
                column: "AppId",
                principalSchema: "jup",
                principalTable: "App",
                principalColumn: "AppId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Package_App_AppId",
                table: "Package",
                column: "AppId",
                principalSchema: "jup",
                principalTable: "App",
                principalColumn: "AppId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Tenants_App_AppId",
                schema: "jup",
                table: "Tenants",
                column: "AppId",
                principalSchema: "jup",
                principalTable: "App",
                principalColumn: "AppId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_App_AppId",
                schema: "jup",
                table: "Users",
                column: "AppId",
                principalSchema: "jup",
                principalTable: "App",
                principalColumn: "AppId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Feature_App_AppId",
                table: "Feature");

            migrationBuilder.DropForeignKey(
                name: "FK_Package_App_AppId",
                table: "Package");

            migrationBuilder.DropForeignKey(
                name: "FK_Tenants_App_AppId",
                schema: "jup",
                table: "Tenants");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_App_AppId",
                schema: "jup",
                table: "Users");

            migrationBuilder.DropTable(
                name: "App",
                schema: "jup");

            migrationBuilder.DropIndex(
                name: "IX_Users_AppId",
                schema: "jup",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Tenants_AppId",
                schema: "jup",
                table: "Tenants");

            migrationBuilder.DropIndex(
                name: "IX_Package_AppId",
                table: "Package");

            migrationBuilder.DropIndex(
                name: "IX_Feature_AppId",
                table: "Feature");

            migrationBuilder.DropColumn(
                name: "AppId",
                schema: "jup",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "AppId",
                schema: "jup",
                table: "Tenants");

            migrationBuilder.DropColumn(
                name: "MaxCustomer",
                table: "PackageFeatureLimit");

            migrationBuilder.DropColumn(
                name: "AppId",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "Package");

            migrationBuilder.DropColumn(
                name: "AppId",
                table: "Feature");

            migrationBuilder.AddColumn<string>(
                name: "Version",
                schema: "jup",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Version",
                schema: "jup",
                table: "Tenants",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "SubscriptionLogs",
                schema: "jup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    MaxEmailPerMonth = table.Column<int>(nullable: true),
                    MaxProduct = table.Column<int>(nullable: true),
                    MaxProductItem = table.Column<int>(nullable: true),
                    MaxSaleOrderPerDay = table.Column<int>(nullable: true),
                    MaxStore = table.Column<int>(nullable: true),
                    MaxUser = table.Column<int>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    OwnerId = table.Column<Guid>(nullable: false),
                    SubscriptionId = table.Column<int>(nullable: false),
                    TenantId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubscriptionLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubscriptionLogs_Users_OwnerId",
                        column: x => x.OwnerId,
                        principalSchema: "jup",
                        principalTable: "Users",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SubscriptionLogs_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "jup",
                        principalTable: "Tenants",
                        principalColumn: "TenantId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Subscriptions",
                schema: "jup",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ExpiredDate = table.Column<DateTime>(nullable: false),
                    MaxEmailPerMonth = table.Column<int>(nullable: true),
                    MaxProduct = table.Column<int>(nullable: true),
                    MaxProductItem = table.Column<int>(nullable: true),
                    MaxSaleOrderPerDay = table.Column<int>(nullable: true),
                    MaxStore = table.Column<int>(nullable: true),
                    MaxUser = table.Column<int>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    OwnerId = table.Column<Guid>(nullable: false),
                    TenantId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscriptions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscriptions_Users_OwnerId",
                        column: x => x.OwnerId,
                        principalSchema: "jup",
                        principalTable: "Users",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Subscriptions_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "jup",
                        principalTable: "Tenants",
                        principalColumn: "TenantId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionLogs_OwnerId",
                schema: "jup",
                table: "SubscriptionLogs",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_SubscriptionLogs_TenantId",
                schema: "jup",
                table: "SubscriptionLogs",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_OwnerId",
                schema: "jup",
                table: "Subscriptions",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscriptions_TenantId",
                schema: "jup",
                table: "Subscriptions",
                column: "TenantId",
                unique: true);
        }
    }
}
