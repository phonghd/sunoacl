﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Suno.Acl.Data.Migrations.AuthDB
{
    public partial class AddedSettingsUserId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ObjId",
                schema: "jup",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "ObjType",
                schema: "jup",
                table: "Settings");

            migrationBuilder.AddColumn<Guid>(
                name: "TenantId",
                schema: "jup",
                table: "Settings",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "UserId",
                schema: "jup",
                table: "Settings",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantId",
                schema: "jup",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "UserId",
                schema: "jup",
                table: "Settings");

            migrationBuilder.AddColumn<string>(
                name: "ObjId",
                schema: "jup",
                table: "Settings",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ObjType",
                schema: "jup",
                table: "Settings",
                nullable: false,
                defaultValue: 0);
        }
    }
}
