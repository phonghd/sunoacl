﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class UpdateSubscriptionActiveDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ActiveDate",
                schema: "jup",
                table: "Subscriptions",
                newName: "ActivedDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ActivedDate",
                schema: "jup",
                table: "Subscriptions",
                newName: "ActiveDate");
        }
    }
}
