﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class ChangedSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "jup");

            migrationBuilder.RenameTable(
                name: "UserTenantStores",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "UserTenants",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "UserTenantRoles",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "Users",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "UserClaims",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "Tenants",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "Subscriptions",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "SubscriptionLogs",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "Stores",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "Roles",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "RolePermissions",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "Permissions",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "ClientScopes",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "Clients",
                schema: "v2",
                newSchema: "jup");

            migrationBuilder.RenameTable(
                name: "ClientGrantTypes",
                schema: "v2",
                newSchema: "jup");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "v2");

            migrationBuilder.RenameTable(
                name: "UserTenantStores",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "UserTenants",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "UserTenantRoles",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "Users",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "UserClaims",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "Tenants",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "Subscriptions",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "SubscriptionLogs",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "Stores",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "Roles",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "RolePermissions",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "Permissions",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "ClientScopes",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "Clients",
                schema: "jup",
                newSchema: "v2");

            migrationBuilder.RenameTable(
                name: "ClientGrantTypes",
                schema: "jup",
                newSchema: "v2");
        }
    }
}
