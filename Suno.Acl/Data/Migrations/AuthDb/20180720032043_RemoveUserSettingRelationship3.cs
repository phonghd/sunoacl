﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class RemoveUserSettingRelationship3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSettings_Tenants_TenantId",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettings_Users_UserId",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.DropIndex(
                name: "IX_UserSettings_TenantId",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.DropIndex(
                name: "IX_UserSettings_UserId",
                schema: "jup",
                table: "UserSettings");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_TenantId",
                schema: "jup",
                table: "UserSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_UserId",
                schema: "jup",
                table: "UserSettings",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettings_Tenants_TenantId",
                schema: "jup",
                table: "UserSettings",
                column: "TenantId",
                principalSchema: "jup",
                principalTable: "Tenants",
                principalColumn: "TenantId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettings_Users_UserId",
                schema: "jup",
                table: "UserSettings",
                column: "UserId",
                principalSchema: "jup",
                principalTable: "Users",
                principalColumn: "SubjectId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
