﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class UpdateSubscriptionInvoiceDetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "PackageId",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "Pos_PackageId");

            migrationBuilder.RenameColumn(
                name: "ExpiredDate",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "Pos_ExpiredDate");

            migrationBuilder.RenameColumn(
                name: "CompanyId",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "Pos_CompanyId");

            migrationBuilder.RenameColumn(
                name: "ActivedDate",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "Pos_ActivedDate");

            migrationBuilder.AddColumn<int>(
                name: "Pos_MaxEmailPerMonth",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Pos_MaxProduct",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Pos_MaxProductItem",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Pos_MaxSaleOrderPerDay",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Pos_MaxStore",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Pos_MaxUser",
                schema: "jup",
                table: "InvoiceDetails",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "CreatedUser",
                table: "CompanyPackage",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Pos_MaxEmailPerMonth",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Pos_MaxProduct",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Pos_MaxProductItem",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Pos_MaxSaleOrderPerDay",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Pos_MaxStore",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.DropColumn(
                name: "Pos_MaxUser",
                schema: "jup",
                table: "InvoiceDetails");

            migrationBuilder.RenameColumn(
                name: "Pos_PackageId",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "PackageId");

            migrationBuilder.RenameColumn(
                name: "Pos_ExpiredDate",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "ExpiredDate");

            migrationBuilder.RenameColumn(
                name: "Pos_CompanyId",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "CompanyId");

            migrationBuilder.RenameColumn(
                name: "Pos_ActivedDate",
                schema: "jup",
                table: "InvoiceDetails",
                newName: "ActivedDate");

            migrationBuilder.AlterColumn<int>(
                name: "CreatedUser",
                table: "CompanyPackage",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);
        }
    }
}
