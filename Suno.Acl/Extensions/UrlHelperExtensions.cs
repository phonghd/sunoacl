﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Suno.Acl.Extensions
{
    public static class UrlHelperExtensions
    {
        public static string ResetPasswordLink(this IUrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                action: nameof(AccountController.ResetPassword),
                controller: "Account",
                values: new {userId, code},
                protocol: scheme);
        }
    }
}