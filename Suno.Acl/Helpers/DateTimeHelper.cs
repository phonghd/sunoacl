﻿using System;

namespace Suno.Acl.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime Now => DateTime.Now;
    }
}
