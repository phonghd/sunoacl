﻿using System;
using System.ComponentModel.DataAnnotations;
using Suno.Acl.Models.Common;


namespace Suno.Acl.Models.Company
{
    public class CompanyInputModel : OperationData
    {
        [Required]        
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string LegalRepresentative { get; set; }
        public string PhoneNumber { get; set; }
        public string TaxCode { get; set; }
        public string Website { get; set; }
        public int? Industry { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Logo { get; set; }
    }
}
