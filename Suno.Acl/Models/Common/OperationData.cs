﻿using System;
using System.Runtime.Serialization;

namespace Suno.Acl.Models.Common
{
    
    public abstract class OperationData	
    {
        [IgnoreDataMember]
        public Guid TenantId { get; set; }
        [IgnoreDataMember]
        public Guid ByUserId { get; set; }
    }
}
