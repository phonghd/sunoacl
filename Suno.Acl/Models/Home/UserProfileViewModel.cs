﻿using System.ComponentModel.DataAnnotations;

namespace Suno.Acl.Models.Home
{
    public class UserProfileViewModel
    {
        [Required(ErrorMessage = "Vui lòng nhập tên hiển thị")]
        public string DisplayName { get; set; }

        [Required(ErrorMessage = "Số điện thoại là bắt buộc")]
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }
    }
}
