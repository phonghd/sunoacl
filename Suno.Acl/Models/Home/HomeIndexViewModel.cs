﻿using Suno.Acl.Infrastructure;

namespace Suno.Acl.Models.Home
{
    public class HomeIndexViewModel
    {
        public string DisplayName { get; set; }
        public AppSettings AppSettings { get; set; }
    }
}
