﻿using System.ComponentModel.DataAnnotations;

namespace Suno.Acl.Models.Account
{
    public class ChangePasswordModelView
    {
        [Required(ErrorMessage = "Mật khẩu cũ là bắt buộc")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "Mật khẩu mới là bắt buộc")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Nhắc lại mật khẩu là bắt buộc")]
        public string RepeatPassword { get; set; }
    }
}
