﻿
using System;

namespace Suno.Acl.Models.Account
{ 
    public class SignupViewModel: SignupInputModel
    {
        public string Token { get; set; }
    }
}
