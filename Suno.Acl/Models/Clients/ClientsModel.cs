﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Suno.Acl.Models
{
    public class ClientsModel
    {
        public int Id { get; set; }
        public int AccessTokenLifetime { get; set; }
        public int AccessTokenType { get; set; }
        public bool AllowAccessTokensViaBrowser { get; set; }
        public bool AllowOfflineAccess { get; set; }
        public string ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientUri { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public int IdentityTokenLifetime { get; set; }
        public bool RequireConsent { get; set; }
    }
}
