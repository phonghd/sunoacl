﻿namespace Suno.Acl.Models.Users
{
    public class SaveUserProfileRequest
    {
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
        public string PhoneNumber { get; set; }
    }
}

