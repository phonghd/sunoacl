﻿namespace Suno.Acl.Models.Users
{
    public class ChangePasswordRequest
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }

    }

    public class UpdateInfoRequest
    {
        public int ProvinceId { get; set; }
        public int IndustryId { get; set; }
    }
}