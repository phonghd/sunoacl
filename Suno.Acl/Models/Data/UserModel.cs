﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;

namespace Suno.Acl.Models.Data
{
    public class UserModel 
    {        
        public bool IsActive { get; set; } = true;
        public ICollection<Claim> Claims { get; set; } = new HashSet<Claim>(new ClaimComparer());
    }
}
