﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Suno.Acl.Models.Custom
{
    public class OperationResult
    {
        public  Guid UserId { get; set; }
        public  Guid TenantId { get; set; }
        public bool Succeeded { get; set; }
        
        //TODO: Add Errors list
        public IList<string> Errors { get; set; }

        public OperationResult()
        {
            Errors = new List<string>();
        }
    }
}
