﻿using System;
using System.Collections.Generic;

namespace Suno.Acl.Models.UserInfo
{
    public class UserRolesModel
    {
        public Guid UserId { get; set; }
        public Guid TenantId { get; set; }
        public List<int> RoleIds { get; set; }

        public UserRolesModel()
        {
            RoleIds = new List<int>();
        }
    }
}