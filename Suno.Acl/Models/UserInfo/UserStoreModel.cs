﻿using System;
using System.Collections.Generic;

namespace Suno.Acl.Models.UserInfo
{
    public class UserStoreModel
    {
        public Guid UserId { get; set; }
        public List<Guid> StoreIds { get; set; }
        public Guid TenantId { get; set; }

        public UserStoreModel()
        {
            StoreIds = new List<Guid>();
        }
    }
}