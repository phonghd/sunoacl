﻿using System;
using System.Collections.Generic;

namespace Suno.Acl.Models.UserInfo
{
    public class UserProfileModel
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool Enabled { get; set; }
        public string Avatar { get; set; }
        public bool IsAdmin { get; set; }
        public string LanguageCode { get; set; }
        public List<Guid> StoreAccess { get; set; }
        public List<int> Roles { get; set; }

        public UserProfileModel()
        {
            StoreAccess = new List<Guid>();
            Roles = new List<int>();
        }
    }
}
