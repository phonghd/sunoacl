﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Suno.Acl.Data;

namespace Suno.Acl.Controllers
{

    

    [Route("api/[controller]")]
    [ApiController]
    public class MasterDataController : BaseApiController
    {
        private readonly AuthDbContext _authDbContext;
        public MasterDataController(AuthDbContext authDbContext)
        {
            _authDbContext = authDbContext;
        }

        [HttpGet("industries")]
        public IActionResult GetIndustries()
        {
            return Ok(_authDbContext.Industries.ToList());
        }

        [HttpGet("provinces")]
        public IActionResult GetProvinces()
        {
            return Ok(_authDbContext.Provinces.ToList());
        }
    }
}