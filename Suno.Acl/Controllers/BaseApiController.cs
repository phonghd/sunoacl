﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Suno.Acl.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class BaseApiController : ControllerBase
    {
        public ErrorMessage ErrorMessage { get; set; }

        public BaseApiController()
        {
            ErrorMessage = new ErrorMessage();
        }
    }


    public class ErrorMessage
    {
        public List<string> Errors { get; set; }

        public ErrorMessage()
        {
            Errors = new List<string>();
        }
    }
}