﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Rest;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Acl.Infrastructure;
using Suno.Acl.Models.Company;
using Suno.Acl.Models.UserInfo;
using Suno.Acl.Services;
using Suno.Core.Acl.Domain.V1;
using Suno.Core.Acl.Domain.V2;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Suno.Acl.Controllers
{
    [Route("api/[controller]")]
    public class AccountApiController : BaseApiController
    {
        private readonly AuthDbContext _authDbContext;
        private readonly AuthService _authService;
        private readonly IWorkContext _workContext;

        public AccountApiController(AuthDbContext authDbContext, AuthService authService, IWorkContext workContext)
        {
            //_account = account;
            _authDbContext = authDbContext;
            this._authService = authService;
            _workContext = workContext;
        }

        [HttpGet]
        public IActionResult Get()
        {
            var claims = User.Claims.Select(c => new { c.Type, c.Value }).ToArray();
            return Ok(new { message = "Hello API", claims });
        }

        [HttpPost]
        [Route("CreateUser")]
        public async Task<IActionResult> Post([FromBody]UserInputModel user)
        {
            user.ByUserId = _workContext.UserId;
            user.TenantId = _workContext.TenantId;
            try
            {
                var result = _authService.CreateCompanyUser(user);
                return Ok(result);

            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message);
            }
        }

        [HttpPut]
        [Route("UpdateUser")]
        public async Task Put([FromBody]UserUpdateModel user)
        {
            if (user.UserId == Guid.Empty)
                throw new HttpRequestException("UserId is Missing");

            user.ByUserId = _workContext.UserId;
            user.TenantId = _workContext.TenantId;

            try
            {
                _authService.UpdateUserInfo(user);
            }
            catch (Exception e)
            {
                throw new HttpOperationException("Update userinfo error", e.InnerException);
            }
        }

        [HttpPut]
        [Route("UpdateUserRole")]
        public async Task<IActionResult> Put([FromBody] UserRolesModel userRole)
        {
            if (!_workContext.TenantIds.Contains(userRole.TenantId))
                return Forbid();

            using (var dbTransaction = _authDbContext.Database.BeginTransaction())
            {
                try
                {
                    #region "Update Roles"
                    var userTenantRoles = _authDbContext.UserTenantRoles.Where(p => p.UserSubjectId == userRole.UserId && p.TenantId == userRole.TenantId).ToList();
                    if (!userRole.RoleIds.Any())
                        _authDbContext.UserTenantRoles.RemoveRange(userTenantRoles);
                    else
                    {
                        //user.Roles
                        var needToDelete = userTenantRoles.Where(p => !userRole.RoleIds.Contains(p.RoleId)).ToList();
                        _authDbContext.UserTenantRoles.RemoveRange(needToDelete);

                        userRole.RoleIds.ForEach(roleId =>
                        {
                            if (!userTenantRoles.Where(p => p.RoleId == roleId).Select(p => true).FirstOrDefault())
                            {
                                _authDbContext.UserTenantRoles.Add(new UserTenantRoleV2
                                {
                                    RoleId = roleId,
                                    TenantId = userRole.TenantId,
                                    UserSubjectId = userRole.UserId,

                                });
                            }
                        });
                    }
                    #endregion

                    _authDbContext.SaveChanges();
                    dbTransaction.Commit();

                    return Ok();
                }
                catch (Exception e)
                {
                    dbTransaction.Rollback();
                    Console.WriteLine(e);
                    throw new HttpOperationException("Update roles error", e.InnerException);
                }
            }

        }

        [HttpPut]
        [Route("UpdateUserStore")]
        public async Task<IActionResult> Put([FromBody] UserStoreModel userStore)
        {
            if (!_workContext.TenantIds.Contains(userStore.TenantId))
                return Forbid();

            using (var dbTransaction = _authDbContext.Database.BeginTransaction())
            {
                try
                {
                    #region "Update Store"
                    var userTenantStores = _authDbContext.UserTenantStores.Where(p => p.UserSubjectId == userStore.UserId && p.TenantId == userStore.TenantId).ToList();
                    if (!userStore.StoreIds.Any())
                        _authDbContext.UserTenantStores.RemoveRange(userTenantStores);
                    else
                    {
                        //user.Roles
                        var needToDelete = userTenantStores.Where(p => !userStore.StoreIds.Contains(p.StoreId)).ToList();
                        _authDbContext.UserTenantStores.RemoveRange(needToDelete);

                        userStore.StoreIds.ForEach(storeId =>
                        {
                            if (!userTenantStores.Where(p => p.StoreId == storeId).Select(p => true).FirstOrDefault())
                            {
                                _authDbContext.UserTenantStores.Add(new UserTenantStore
                                {
                                    StoreId = storeId,
                                    TenantId = userStore.TenantId,
                                    UserSubjectId = userStore.UserId
                                });
                            }
                        });
                    }

                    #endregion

                    _authDbContext.SaveChanges();
                    dbTransaction.Commit();

                    return Ok();
                }
                catch (Exception e)
                {
                    dbTransaction.Rollback();
                    Console.WriteLine(e);
                    throw;
                }
            }
        }

        [HttpGet]
        [Route("GetCompanyInfo")]
        public async Task<object> GetCompanyInfo()
        {
            var tenantId = _workContext.TenantId;
            var stores = await _authDbContext.Stores.Where(p => p.TenantId == tenantId).Select(p => new
            {
                p.TenantId,
                p.StoreAddress,
                p.StoreCode,
                p.StoreName,
                p.StorePhone,
                p.CreatedDate,
                p.StoreId,
                p.TaxCode,
                p.IsDeleted
            }).ToListAsync();

            var allUser = _authService.GetListUser(tenantId);
            var allStore = new
            {
                Items = stores,
                Total = stores.Count
            };
            var allRole = _authService.GetTenantRoles(tenantId);
            var tenant = await _authDbContext.Tenant.Where(p => p.TenantId == tenantId).Select(p => new { p.CompanyId, p.Logo }).FirstOrDefaultAsync();

            var posSubscriptions = _authDbContext.PosSubscriptions.Where(p => p.TenantId == tenantId && p.ActivedDate <= DateTimeHelper.Now && p.ExpiredDate > DateTimeHelper.Now).ToList();
            var omniChannelSubscriptions = _authDbContext.OmniSubscriptions.Where(p => p.TenantId == tenantId && p.ActivedDate <= DateTimeHelper.Now && p.ExpiredDate > DateTimeHelper.Now).ToList();

            var posRemainDays = 0;
            DateTime expiredDate = DateTimeHelper.Now;

            PosSubscription previousSubscription = null;
            if (posSubscriptions.Any())
            {
                expiredDate = posSubscriptions.Select(p => p.ExpiredDate).Max();
                posRemainDays = (int)posSubscriptions.Select(p => p.ExpiredDate).Max().Subtract(DateTime.Now).TotalDays;
            }
            else
            {
                // var expiredSubscription = 
                var dateTimeNow = DateTimeHelper.Now;
                previousSubscription = _authDbContext.PosSubscriptions.Where(p => p.TenantId == tenantId && p.ExpiredDate <= dateTimeNow)
                    .OrderByDescending(p => p.ExpiredDate).FirstOrDefault();
            }



            var companyInfo = await _authDbContext.Companies.Where(p => p.CompanyId == tenant.CompanyId).Select(p => new
            {
                TenantId = tenantId,
                Address = p.Address,
                Comments = p.Comments,
                CompanyName = p.CompanyName,
                CompanyCode = p.CompanyCode,
                Email = p.Email,
                PhoneNumber = p.PhoneNumber,
                Status = p.Status,
                LegalRepresentative = p.LegalRepresentative,
                TaxCode = p.TaxCode,
                Industry = p.Industry,
                LanguageId = "vi-VN",
                Logo = tenant.Logo,
                Website = p.Website,
                AllStore = allStore,
                AllUser = allUser,
                AllRole = allRole,
                PosSubscriptions = posSubscriptions,
                OmniSubscriptions = omniChannelSubscriptions,
                PosRemainDays = posRemainDays,
                ExpiredDate = expiredDate,
                PreviousPosSubscription = previousSubscription
            }).FirstOrDefaultAsync();

            return companyInfo;
        }

        [HttpPost]
        [Route("UpdateCompanyInfo")]
        public async Task<IActionResult> Post([FromBody]CompanyInputModel model)
        {
            model.TenantId = _workContext.TenantId;
            model.ByUserId = _workContext.UserId;


            var company = new Company { CompanyId = _workContext.CompanyId };
            _authDbContext.Companies.Attach(company);

            company.Address = model.Address;
            company.TaxCode = model.TaxCode;
            company.LegalRepresentative = model.LegalRepresentative;
            company.Industry = model.Industry;
            company.PhoneNumber = model.PhoneNumber;
            company.Email = model.Email ?? string.Empty;
            company.Website = model.Website ?? string.Empty;
            company.CompanyName = model.CompanyName;

            company.UpdatedDate = DateTime.Now;

            if (!string.IsNullOrEmpty(model.Logo))
            {
                var tenant = new Tenant { TenantId = _workContext.TenantId };
                _authDbContext.Tenant.Attach(tenant);
                tenant.Logo = model.Logo;
            }

            await _authDbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPost]
        [Route("CreateStore")]
        public async Task<Guid> Post([FromBody]StoreInputModel store)
        {
            store.TenantId = _workContext.TenantId;
            store.ByUserId = _workContext.UserId;

            var storeId = Guid.NewGuid();
            _authDbContext.Stores.Add(new Store
            {
                CreatedDate = DateTime.Now,
                CreatedUser = store.ByUserId,
                IsDeleted = false,
                StoreCode = store.StoreCode,
                StoreName = store.StoreName,
                StorePhone = store.StorePhone,
                StoreAddress = store.StoreAddress,
                StoreId = storeId,
                TaxCode = store.TaxCode,

                TenantId = store.TenantId
            });

            _authDbContext.SaveChanges();
            return storeId;
        }

        [HttpPut]
        [Route("UpdateStoreInfo")]
        public async Task<IActionResult> Put([FromBody]StoreInputModel model)
        {
            model.TenantId = _workContext.TenantId;

            //var store = new Store { StoreId = model.StoreId };
            //_authDbContext.Stores.Attach(store);
            //store.StoreName = model.StoreName;
            //store.StoreAddress = model.StoreAddress;
            //store.StoreCode = store.StoreCode;
            //store.StorePhone = store.StorePhone;

            var store = _authDbContext.Stores.Where(p => p.StoreId == model.StoreId).FirstOrDefault();
            if (store != null)
            {
                store.StoreName = model.StoreName;
                store.StoreAddress = model.StoreAddress;
                //store.StoreCode = model.StoreCode;
                store.StorePhone = model.StorePhone;
            }

            await _authDbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPost]
        [Route("ChangeUserPassword")]
        public async Task Post([FromBody]ChangePasswordModel user)
        {
            //await _account.ChangeUserPassword(user);
            _authService.ChangeUserPassword(user);
        }

        [HttpGet]
        [Route("GetUserInfo")]
        public UserProfileModel GetUserInfo([FromQuery] Guid userId)
        {
            if (userId == null)
                throw new ArgumentException("cannot find userId", nameof(userId));

            var tenantId = _workContext.TenantId;

            var userProfile = _authService.Users().Where(p => p.User.SubjectId == userId).Select(p => new UserProfileModel
            {
                UserId = userId,
                DisplayName = p.UserProfile.DisplayName,
                Email = p.UserProfile.Email,
                Enabled = p.UserProfile.IsActived,
                IsAdmin = p.UserProfile.IsAdmin,
                Username = p.Membership.UserName,
                LanguageCode = p.UserProfile.CultureInfo,
                PhoneNumber = p.UserProfile.PhoneNumber
            }).FirstOrDefault();

            if (userProfile != null)
            {
                userProfile.StoreAccess = _authDbContext.UserTenantStores.Where(p => p.TenantId == tenantId && p.UserSubjectId == userId).Select(p => p.StoreId).ToList();
                userProfile.Roles = _authDbContext.UserTenantRoles.Where(p => p.TenantId == tenantId && p.UserSubjectId == userId).Select(p => p.RoleId).ToList();
                userProfile.Avatar = _authDbContext.UserClaims.Where(p => p.UserSubjectId == userId && p.ClaimType == JwtClaimTypes.Picture).Select(p => p.ClaimValue).FirstOrDefault();
            }
            return userProfile;
        }
    }
}
