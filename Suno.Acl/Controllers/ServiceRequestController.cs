﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Acl.Infrastructure;
using Suno.Acl.Models.ServiceRequest;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Acl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceRequestController : BaseApiController
    {
        private readonly IWorkContext _workContext;
        private readonly AuthDbContext _authDbContext;
        private readonly Suno.Acl.Services.IEmailSender _emailSender;
        private readonly AppSettings _appSettings;

        public ServiceRequestController(AuthDbContext authDbContext, IWorkContext workContext, Services.IEmailSender emailSender,
            AppSettings appSettings)
        {
            _workContext = workContext;
            _authDbContext = authDbContext;
            _emailSender = emailSender;
            _appSettings = appSettings;
        }


        [HttpPost("extend-service-request")]
        public IActionResult PostExtendationRequest([FromBody] ServiceRequestModel request)
        {
            var companyId = _workContext.CompanyId;
            var membershipId = _workContext.MembershipId;

            var lastRequest = _authDbContext.ServiceRequests.Where(p => p.CompanyId == _workContext.CompanyId && p.RequestedBy == membershipId && p.Type == eServiceRequest.ExtendationRequest).OrderByDescending(p => p.RequestedDate).Select(p => p.RequestedDate).FirstOrDefault();

            _authDbContext.ServiceRequests.Add(new ServiceRequest
            {
                CompanyId = _workContext.CompanyId,
                RequestedBy = _workContext.MembershipId,
                RequestedDate = DateTimeHelper.Now,
                Type = eServiceRequest.ExtendationRequest
            });
            _authDbContext.SaveChanges();

            var msg = "Yêu cầu gia hạn dịch vụ: companyId =  " + _workContext.CompanyId + "; ";
            _emailSender.SendEmailAsync(_appSettings.EmailNotifySetting.ExtendRequest, msg, msg);


            return Ok(new
            {
                LastRequest = lastRequest,
                Days = DateTimeHelper.Now.Subtract(lastRequest).TotalDays
            });
        }
    }
}