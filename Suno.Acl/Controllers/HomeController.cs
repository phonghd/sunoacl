﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using System;
using System.Linq;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Core.Flash;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Serilog;
using Suno.Acl.Data;
using Suno.Acl.Infrastructure;
using Suno.Acl.Models.Home;
using Suno.Acl.Services;
using Suno.Core.Acl.Domain.V2;
using UserProfile = Suno.Core.Acl.Domain.V1.UserProfile;


namespace Suno.Acl
{
    //[SecurityHeaders]
    public class HomeController : Controller
    {
        private readonly IIdentityServerInteractionService _interaction;
        private readonly WorkContext _workContext;
        private readonly AppSettings _appSettings;
        private readonly IFlasher _flashMessage;
        private readonly AuthService _authService;
        private readonly AuthDbContext _authDbContext;

        public HomeController(
            IIdentityServerInteractionService interaction,
             WorkContext workContext, AppSettings appSettings, IFlasher flashMessage, AuthService authService, AuthDbContext authDbContext)
        {
            _interaction = interaction;
            _workContext = workContext;
            _appSettings = appSettings;
            _flashMessage = flashMessage;
            _authService = authService;
            _authDbContext = authDbContext;
        }

        public async Task<IActionResult> Index()
        {
            if (_appSettings.PosAsDefaultApp && !string.IsNullOrEmpty(_appSettings.AppsPosUrl))
                return Redirect(_appSettings.AppsPosUrl);
            

            var viewModel = new HomeIndexViewModel { DisplayName = "anh/chị", AppSettings = _appSettings };
            if (User.Identity.IsAuthenticated)
            {
                var userProfile = GetUserProfileById(_workContext.UserId);

                if (userProfile != null)
                    viewModel.DisplayName = userProfile.DisplayName ?? userProfile.UserName ?? userProfile.Email ?? viewModel.DisplayName;
            }

            return View(viewModel);
        }

        [Authorize]
        public async Task<IActionResult> Profile()
        {


            var userProfile = GetUserProfileById(_workContext.UserId);
            return View(userProfile);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Profile(UserProfileViewModel user)
        {
            if (ModelState.IsValid)
            {
               
                var userObj = _authService.Users().Where(p => p.User.SubjectId == _workContext.UserId).Select(p => p.User).FirstOrDefault();


                var userProfile = new UserProfile { UserId = userObj.UserId };

                _authDbContext.UserProfiles.Attach(userProfile);
                userProfile.DisplayName = user.DisplayName;
                userProfile.PhoneNumber = user.PhoneNumber;
                if (!string.IsNullOrEmpty(user.Avatar))
                {
                    var avatarClaim = _authDbContext.UserClaims
                        .FirstOrDefault(p => p.UserSubjectId == _workContext.UserId && p.ClaimType == JwtClaimTypes.Picture);

                    if (avatarClaim != null)
                        avatarClaim.ClaimValue = user.Avatar;
                    else _authDbContext.UserClaims.Add(new UserClaim { UserSubjectId = _workContext.UserId, ClaimType = JwtClaimTypes.Picture, ClaimValue = user.Avatar });
                }

                 await _authDbContext.SaveChangesAsync();

                _flashMessage.Flash("success", "Cập nhật thông tin thành công");
                user = GetUserProfileById(_workContext.UserId);
            }

            return View(user);
        }



        /// <summary>
        /// Shows the error page
        /// </summary>
        //[Route("/Home/Error")]
        public async Task<IActionResult> Error(string errorId)
        {
            var vm = new ErrorViewModel();

            // retrieve error details from identityserver
            var message = await _interaction.GetErrorContextAsync(errorId);
            if (message != null)
            {
                vm.Error = message;
            }
            Log.Error(message.Error);
            return View("Error", vm);
        }

        private UserProfileViewModel GetUserProfileById(Guid userId)
        {
            var user = _authService.Users().Where(p => p.User != null && p.Membership != null && p.User.SubjectId == userId)
                .Select(p => new UserProfileViewModel
                {
                    UserName = p.Membership.UserName,
                    Email = p.UserProfile.Email,
                    DisplayName = p.UserProfile.DisplayName,
                    PhoneNumber = p.UserProfile.PhoneNumber,
                }).FirstOrDefault();


            if (user != null)
            {
                user.Avatar = _authDbContext.UserClaims.Where(p => p.UserSubjectId == userId && p.ClaimType == JwtClaimTypes.Picture).Select(p => p.ClaimValue).FirstOrDefault();
            }
            return user;
        }
    }
}