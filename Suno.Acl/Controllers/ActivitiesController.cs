﻿using Microsoft.AspNetCore.Mvc;
using Suno.Acl.Data;
using Suno.Acl.Infrastructure;
using Suno.Acl.Services.Models;
using System;


namespace Suno.Acl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivitiesController : BaseApiController
    {
        private readonly IWorkContext _workContext;
        private readonly AuthDbContext _authDbContext;
        private readonly Suno.Acl.Services.IEmailSender _emailSender;
        private readonly AppSettings _appSettings;

        public ActivitiesController(AuthDbContext authDbContext, IWorkContext workContext, Services.IEmailSender emailSender,
            AppSettings appSettings)
        {
            _workContext = workContext;
            _authDbContext = authDbContext;
            _emailSender = emailSender;
            _appSettings = appSettings;
        }


        [HttpPost("")]
        public IActionResult PostActivity([FromBody] ActivityModel request)
        {
            _authDbContext.ClientInfos.Add(new Core.Acl.Domain.V2.ClientInfo
            {
                RegisterCompanyId = _workContext.CompanyId,
                CreatedDate = DateTime.Now,
                Info = request.Info,
                Action = request.Action
            });
            _authDbContext.SaveChanges();

            var msg = " companyId =  " + _workContext.CompanyId + "; " + request.Info;
            _emailSender.SendEmailAsync(_appSettings.EmailNotifySetting.Activities, request.Action, msg);

            return Ok();
        }
    }
}