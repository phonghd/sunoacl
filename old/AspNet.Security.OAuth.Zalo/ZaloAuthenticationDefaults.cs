﻿/*
 * Licensed under the Apache License, Version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 * See https://github.com/aspnet-contrib/AspNet.Security.OAuth.Providers
 * for more information concerning the license and the contributors participating to this project.
 */

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;

namespace AspNet.Security.OAuth.Zalo
{
    /// <summary>
    /// Default values used by the GitHub authentication middleware.
    /// </summary>
    public static class ZaloAuthenticationDefaults
    {
        /// <summary>
        /// Default value for <see cref="AuthenticationScheme.Name"/>.
        /// </summary>
        public const string AuthenticationScheme = "Zalo";

        /// <summary>
        /// Default value for <see cref="AuthenticationScheme.DisplayName"/>.
        /// </summary>
        public const string DisplayName = "Zalo";

        /// <summary>
        /// Default value for <see cref="AuthenticationSchemeOptions.ClaimsIssuer"/>.
        /// </summary>
        public const string Issuer = "Zalo";

        /// <summary>
        /// Default value for <see cref="RemoteAuthenticationOptions.CallbackPath"/>.
        /// </summary>
        public const string CallbackPath = "/signin-zalo";

        /// <summary>
        /// Default value for <see cref="OAuthOptions.AuthorizationEndpoint"/>.
        /// </summary>
//        public const string AuthorizationEndpoint = "https://github.com/login/oauth/authorize";
        public const string AuthorizationEndpoint = "https://oauth.zaloapp.com/v3/auth";
        //https://oauth.zaloapp.com/v3/auth?app_id={1}&redirect_uri={2}

        /// <summary>
        /// Default value for <see cref="OAuthOptions.TokenEndpoint"/>.
        /// </summary>
//        public const string TokenEndpoint = "https://github.com/login/oauth/access_token";       
//        https://oauth.zaloapp.com/v3/access_token?app_id={1}&app_secret={2}&code={3}
        public const string TokenEndpoint = "https://oauth.zaloapp.com/v3/access_token";
        /// <summary>
        /// Default value for <see cref="OAuthOptions.UserInformationEndpoint"/>.
        /// </summary>
        public const string UserInformationEndpoint = "https://graph.zalo.me/v2.0/me";

        /// <summary>
        /// Default value for <see cref="ZaloAuthenticationOptions.UserEmailsEndpoint"/>.
        /// </summary>
        public const string UserEmailsEndpoint = "";
    }
}
