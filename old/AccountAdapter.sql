/********************************************************************************************
Table affected with company
    1. Company
    2. CompanyPackage
    3. CompanyContract (hop dong)
    4. CompanyExtension (gia han)
    5. Package (with default data)
	6. Industry
	7. CompanyFeatureLimit
********************************************************************************************/
/*
DECLARE @companyId int = 103;
select * from Company where companyId = @companyId
select * from CompanyPackage where companyid= @companyId
select * from CompanyContract where companyId =@companyId 
select * from CompanyExtension where CompanyId = @companyId
select * from CompanyFeatureLimit where companyId = @companyId
*/


/*******************************************SCRIPT ON ACCOUNT DB*************************************************/

-- Add Column tenantId to company
ALTER TABLE Company
Add TenantId UNIQUEIDENTIFIER NULL;

/*******************************************SCRIPT ON ACL DB*************************************************/


-- CREATE CompanyPackage Table --
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyPackage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,    
	[TenantId] UNIQUEIDENTIFIER Not NULL,
	[CompanyId] [int] NULL,
	[PackageId] [int] NOT NULL,
	[ActivatedDate] [datetime] NOT NULL,
	[ExpiredDate] [datetime] NULL,
	[PayStatus] [int] NOT NULL,
	[Amount] [decimal](18, 5) NOT NULL,
	[HasNextPackage] [bit] NOT NULL,
	[ModifiedCount] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL default 0,
	[CreatedDate] [datetime] NOT NULL DEFAULT GETDATE(),
	[CreatedUser] [UNIQUEIDENTIFIER] NOT NULL,
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CompanyPackage] ADD  CONSTRAINT [PK_CompanyPackage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO 

-- CREATE Package Table --
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Package](
	[PackageId] [int] NOT NULL,
	[Name] [nvarchar](125) NOT NULL,
	[IsTrial] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
    [CreatedUser] [UNIQUEIDENTIFIER] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsFree] [bit] NOT NULL,
	[Key] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Package] ADD  CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED 
(
	[PackageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
INSERT [dbo].[Package] ([PackageId], [Name], [IsTrial], [CreatedDate], [UpdatedDate], [UpdatedUser], [DeletedUser], [DeletedDate], [IsDeleted], [CreatedUser], [IsFree], [Key]) VALUES (1, N'Trial', 1, CAST(N'2014-10-08 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0, 0, 1, N'TRIAL')
GO
INSERT [dbo].[Package] ([PackageId], [Name], [IsTrial], [CreatedDate], [UpdatedDate], [UpdatedUser], [DeletedUser], [DeletedDate], [IsDeleted], [CreatedUser], [IsFree], [Key]) VALUES (2, N'Free', 0, CAST(N'2014-10-08 00:00:00.000' AS DateTime), NULL, NULL, 1, CAST(N'2014-10-27 00:00:00.000' AS DateTime), 1, 0, 1, N'FREE')
GO
INSERT [dbo].[Package] ([PackageId], [Name], [IsTrial], [CreatedDate], [UpdatedDate], [UpdatedUser], [DeletedUser], [DeletedDate], [IsDeleted], [CreatedUser], [IsFree], [Key]) VALUES (3, N'Basic', 0, CAST(N'2014-10-08 00:00:00.000' AS DateTime), NULL, NULL, 1, CAST(N'2014-10-27 00:00:00.000' AS DateTime), 1, 0, 0, N'BASIC')
GO
INSERT [dbo].[Package] ([PackageId], [Name], [IsTrial], [CreatedDate], [UpdatedDate], [UpdatedUser], [DeletedUser], [DeletedDate], [IsDeleted], [CreatedUser], [IsFree], [Key]) VALUES (4, N'Advanced', 0, CAST(N'2014-10-08 00:00:00.000' AS DateTime), NULL, NULL, NULL, NULL, 0, 0, 0, N'ADVANCED')
GO


-- CREATE Package Table --
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Industry](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](511) NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Industry] ADD  CONSTRAINT [PK_Industry] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- Create Table CompanyFeatureLimit --
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyFeatureLimit](
	TenantId [UNIQUEIDENTIFIER] NOT NULL,
	[CompanyId] [int] NULL,
	[MaxEmailPerMonth] [int] NULL,
	[MaxUser] [int] NULL,
	[MaxStore] [int] NULL,
	[MaxSaleOrderPerDay] [int] NULL,
	[MaxProduct] [int] NULL,
	[MaxProductItem] [int] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[CompanyFeatureLimit] ADD  CONSTRAINT [PK_CompanyFeatureLimit] PRIMARY KEY CLUSTERED 
(
	[TenantId] ASC
)WITH (
	PAD_INDEX = OFF, 
	STATISTICS_NORECOMPUTE = OFF, 
	SORT_IN_TEMPDB = OFF, 
	IGNORE_DUP_KEY = OFF, 
	ONLINE = OFF, 
	ALLOW_ROW_LOCKS = ON, 
	ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[PackagePlan]    Script Date: 4/23/2018 10:34:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PackagePlan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TenantId] UNIQUEIDENTIFIER NOT NULL,
	[CompanyId] [int] NULL,
	[PackageId] [int] NOT NULL,
	[SelectedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[StatusDate] [datetime] NOT NULL,
 CONSTRAINT [PK_PackagePlan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO