﻿using System;
using IdentityServer4;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ServiceStack;
using ServiceStack.Data;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Acl.Infrastructure;
using Suno.Acl.Models.Account;
using Suno.Acl.Services;
using Suno.Core.Hash;
using Swashbuckle.AspNetCore.Swagger;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;

namespace Suno.Acl
{
    public class Startup
    {
        
        //private static IUnitOfWork _unitOfWorkIntance;

        private IConfigurationRoot Configuration { get; set; }
        private readonly string _environmentName;

        public Startup(IHostingEnvironment env)
        {
            _environmentName = env.EnvironmentName;

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile($"appsettings.json", optional: true)
                .AddJsonFile($"appsettings.{_environmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddFlashes();
            services.AddMvc().AddSessionStateTempDataProvider();
            services.AddSession();
            var licenseKey = Configuration["servicestack:license"];
            Licensing.RegisterLicense(licenseKey);

            var connectionString = Configuration["ConnectionStrings:DefaultConnection"];


            Console.WriteLine("Environment::" + _environmentName);
            Console.WriteLine("ConnectionString::" + connectionString);

            //sqlProvider.StringConverter.UseUnicode = true;

            var mailConfig = new MailConfigOptions
            {
                Host = Configuration["Mail:Host"],
                Port = Configuration["Mail:Port"].ToInt(),
                Sender = Configuration["Mail:Sender"],
                User = Configuration["Mail:User"],
                Password = Configuration["Mail:Password"]
            };
            var appSettings = new AppSettings
            {
                AppsPosUrl = Configuration.GetValue<string>("Apps:PosUrl"),
                AppsCdnUrl = Configuration.GetValue<string>("Apps:CdnUrl"),
                EnableOAuthFacebook = Configuration.GetValue<bool>("Hosting:FacebookAuth:Enable"),
                EnableOAuthZalo = Configuration.GetValue<bool>("Hosting:ZaloAuth:Enable"),
                EnableOAuthGoogle = Configuration.GetValue<bool>("Hosting:GoogleAuth:Enable"),
                SunoOAuthV1Endpoint = Configuration.GetValue<string>("SunoOAuthV1Endpoint"),
                EnableV1Login = Configuration.GetValue<bool>("EnableV1Login"),
                PosAsDefaultApp = Configuration.GetValue<bool>("PosAsDefaultApp")
            };

            //CrmSettings
            var crmConfig = new CrmSettings();
            Configuration.GetSection("CrmSettings").Bind(crmConfig);
            services.AddSingleton(p => crmConfig);


            services.AddSingleton<AppSettings>(p => appSettings);
            services.AddDbContext<AuthDbContext>(options => options.UseSqlServer(connectionString));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<WorkContext>();
            services.AddScoped<ApiWorkContext>();
            services.AddScoped<CrmService>();


            services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IExternalAdapterService, ExtenalAdapterService>();
            services.AddTransient<IEmailSender, EmailSender>(x => new EmailSender(mailConfig));

            services.AddScoped<SaltedHash>();
            services.AddScoped<AuthService>();
            services.AddScoped<BasicDataService>();
            services.AddScoped<CryptographyHelper>();
            services.AddScoped<CommonHelper>();
            services.AddScoped<SettingService>();
            services.AddScoped<PackagePricingService>();

            //Hosting Configure
            services.Configure<IISOptions>(Configuration);

            
            var cert = new X509Certificate2(Path.Combine(".", "IdentityServer4Auth.pfx"), "p@ssw0rd");

            services.AddIdentityServer()
                .AddSigningCredential(cert)

                //.AddSigningCredential(new X509Certificate2(Path.Combine(".", "IdentityServer4Auth.pfx"), "p@ssw0rd"))
                //.AddValidationKey(new X509Certificate2(Path.Combine(".", "IdentityServer4Auth.pfx"), "p@ssw0rd"))
                //.AddDeveloperSigningCredential()

                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApiResources())

                .AddProfileService<ProfileService>()
                .AddClientStore<ClientStore>()
                .AddCorsPolicyService<CorsPolicyService>()


                ;

            //
            services
                .AddAuthentication(options =>
                {
                    //options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    //options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
                })
              
                .AddCookie()
                .AddIdentityServerAuthentication("Bearer", options =>
                {
                    options.Authority = Configuration["Hosting:AuthorityUrl"];
                    options.Audience = "MasterData";
                    options.RequireHttpsMetadata = false;

                    var certificate = new X509Certificate2(Path.Combine(".", "IdentityServer4Auth.pfx"), "p@ssw0rd");
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidAudience = Configuration["Hosting:AuthorityUrl"] + "/resources",
                        ValidIssuer = Configuration["Hosting:AuthorityUrl"],
                        IssuerSigningKey = new X509SecurityKey(certificate),
                        IssuerSigningKeyResolver = (string token, SecurityToken securityToken, string kid, TokenValidationParameters validationParameters)
                        => new List<X509SecurityKey> { new X509SecurityKey(certificate) }
                    };

                }, null)
                .AddZalo("Zalo", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.ClientId = Configuration["Hosting:ZaloAuth:ClientId"];
                    options.ClientSecret = Configuration["Hosting:ZaloAuth:ClientSecret"];
                })
                .AddFacebook("facebook", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.ClientId = Configuration["Hosting:FacebookAuth:ClientId"];
                    options.ClientSecret = Configuration["Hosting:FacebookAuth:ClientSecret"];
                })
                .AddGoogle("Google", options =>
                {
                    options.SignInScheme = IdentityServerConstants.ExternalCookieAuthenticationScheme;
                    options.ClientId = Configuration["Hosting:GoogleAuth:ClientId"];
                    options.ClientSecret = Configuration["Hosting:GoogleAuth:ClientSecret"];
                });
            services.AddCors(options =>
            {
                // this defines a CORS policy called "default"
                //TODO: Add Cors Dynamic
                options.AddPolicy("default", policy =>
                {
                    policy
                        .AllowAnyOrigin()
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            services.AddDataProtection().SetApplicationName("SunoAcl");

            // services.AddResponseCompression();
            // Register the Swagger generator, defining one or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Suno Acl", Version = "v1" });
            });


            // Build the intermediate service provider
            var serviceProvider = services.BuildServiceProvider();
            ProcessMigration(serviceProvider);
        }

        private bool SslRequired
        {
            get
            {
                var sslRequired = Configuration.GetValue<bool>("SslRequired");
                return sslRequired;
            }
        }

        private void ProcessMigration(ServiceProvider serviceProvider)
        {
            using (var context = serviceProvider.GetService<AuthDbContext>())
            {
                context.SaveChanges();

                var connection = context.Database.GetDbConnection();
                connection.Open();
                var cmd = connection.CreateCommand();

                cmd.CommandText = "SELECT COUNT(*)  FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = '__EFMigrationsHistory'";
                var existTable = (int)cmd.ExecuteScalar();
                if (existTable == 0)
                {
                    context.Database.ExecuteSqlCommand(
                        "CREATE TABLE [dbo].[__EFMigrationsHistory](	[MigrationId] [nvarchar](150) NOT NULL,	[ProductVersion] [nvarchar](32) NOT NULL, CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED (	[MigrationId] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ) ON [PRIMARY]");
                }

                cmd.CommandText = "SELECT COUNT(*) FROM [dbo].[__EFMigrationsHistory] ";
                var migrationCount = (int)cmd.ExecuteScalar();
                if (migrationCount == 0)
                    context.Database.ExecuteSqlCommand(
                        "INSERT INTO [dbo].[__EFMigrationsHistory] VALUES('20180524081222_InitDbV1', '2.0.0-rtm-26452')");

                context.Database.Migrate();

                var basicDataService = serviceProvider.GetService<BasicDataService>();
                basicDataService.SeedData(context);

            }
        }



        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            //handler.AddHandler(sender);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseExceptionHandler("/home/error");
            }

            loggerFactory.AddConsole(LogLevel.Error);
            loggerFactory.AddSerilog();

            var sslRequired = Configuration.GetValue<bool>("SslRequired");

            if (sslRequired)
            {
                var forwardOptions = new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
                    RequireHeaderSymmetry = false
                };

                forwardOptions.KnownNetworks.Clear();
                forwardOptions.KnownProxies.Clear();

                app.UseForwardedHeaders(forwardOptions);
            }



            app.UseStaticFiles();
            //            app.UseResponseCompression();
            app.UseCors("default");
            app.UseIdentityServer();

            //app.UseJwtBearerAuthentication

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Suno ACL");
                c.RoutePrefix = "WebApi";
            });

            app.UseSession();
            app.UseMvc(routes => routes.MapRoute(
                name: "Default",
                template: "{controller=Home}/{action=Index}/{id?}")
            );

        }
    }
}
