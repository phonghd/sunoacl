﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Suno.Acl.Data.Migrations.AuthDb
{
    public partial class AddedUserTenantStoreAndRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTenant_Tenants_TenantId",
                schema: "v2",
                table: "UserTenant");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTenant_Users_UserSubjectId",
                schema: "v2",
                table: "UserTenant");

            migrationBuilder.DropTable(
                name: "UserStore",
                schema: "v2");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserTenant",
                schema: "v2",
                table: "UserTenant");

            migrationBuilder.RenameTable(
                name: "UserTenant",
                schema: "v2",
                newName: "UserTenants");

            migrationBuilder.RenameIndex(
                name: "IX_UserTenant_UserSubjectId",
                schema: "v2",
                table: "UserTenants",
                newName: "IX_UserTenants_UserSubjectId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserTenants",
                schema: "v2",
                table: "UserTenants",
                columns: new[] { "TenantId", "UserSubjectId" });

            migrationBuilder.CreateTable(
                name: "UserTenantRoles",
                schema: "v2",
                columns: table => new
                {
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    UserSubjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    RoleId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTenantRoles", x => new { x.TenantId, x.UserSubjectId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_UserTenantRoles_Roles_RoleId",
                        column: x => x.RoleId,
                        principalSchema: "v2",
                        principalTable: "Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTenantRoles_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "v2",
                        principalTable: "Tenants",
                        principalColumn: "TenantId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTenantRoles_Users_UserSubjectId",
                        column: x => x.UserSubjectId,
                        principalSchema: "v2",
                        principalTable: "Users",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserTenantStores",
                schema: "v2",
                columns: table => new
                {
                    UserSubjectId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StoreId = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    TenantId = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTenantStores", x => new { x.UserSubjectId, x.StoreId, x.TenantId });
                    table.ForeignKey(
                        name: "FK_UserTenantStores_Stores_StoreId",
                        column: x => x.StoreId,
                        principalSchema: "v2",
                        principalTable: "Stores",
                        principalColumn: "StoreId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTenantStores_Tenants_TenantId",
                        column: x => x.TenantId,
                        principalSchema: "v2",
                        principalTable: "Tenants",
                        principalColumn: "TenantId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserTenantStores_Users_UserSubjectId",
                        column: x => x.UserSubjectId,
                        principalSchema: "v2",
                        principalTable: "Users",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserTenantRoles_RoleId",
                schema: "v2",
                table: "UserTenantRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTenantRoles_UserSubjectId",
                schema: "v2",
                table: "UserTenantRoles",
                column: "UserSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTenantStores_StoreId",
                schema: "v2",
                table: "UserTenantStores",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_UserTenantStores_TenantId",
                schema: "v2",
                table: "UserTenantStores",
                column: "TenantId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserTenants_Tenants_TenantId",
                schema: "v2",
                table: "UserTenants",
                column: "TenantId",
                principalSchema: "v2",
                principalTable: "Tenants",
                principalColumn: "TenantId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTenants_Users_UserSubjectId",
                schema: "v2",
                table: "UserTenants",
                column: "UserSubjectId",
                principalSchema: "v2",
                principalTable: "Users",
                principalColumn: "SubjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserTenants_Tenants_TenantId",
                schema: "v2",
                table: "UserTenants");

            migrationBuilder.DropForeignKey(
                name: "FK_UserTenants_Users_UserSubjectId",
                schema: "v2",
                table: "UserTenants");

            migrationBuilder.DropTable(
                name: "UserTenantRoles",
                schema: "v2");

            migrationBuilder.DropTable(
                name: "UserTenantStores",
                schema: "v2");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserTenants",
                schema: "v2",
                table: "UserTenants");

            migrationBuilder.RenameTable(
                name: "UserTenants",
                schema: "v2",
                newName: "UserTenant");

            migrationBuilder.RenameIndex(
                name: "IX_UserTenants_UserSubjectId",
                schema: "v2",
                table: "UserTenant",
                newName: "IX_UserTenant_UserSubjectId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserTenant",
                schema: "v2",
                table: "UserTenant",
                columns: new[] { "TenantId", "UserSubjectId" });

            migrationBuilder.CreateTable(
                name: "UserStore",
                schema: "v2",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    DeletedBy = table.Column<Guid>(nullable: true),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    StoreId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserStore", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserStore_Stores_StoreId",
                        column: x => x.StoreId,
                        principalSchema: "v2",
                        principalTable: "Stores",
                        principalColumn: "StoreId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserStore_Users_UserId",
                        column: x => x.UserId,
                        principalSchema: "v2",
                        principalTable: "Users",
                        principalColumn: "SubjectId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserStore_StoreId",
                schema: "v2",
                table: "UserStore",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_UserStore_UserId",
                schema: "v2",
                table: "UserStore",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserTenant_Tenants_TenantId",
                schema: "v2",
                table: "UserTenant",
                column: "TenantId",
                principalSchema: "v2",
                principalTable: "Tenants",
                principalColumn: "TenantId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserTenant_Users_UserSubjectId",
                schema: "v2",
                table: "UserTenant",
                column: "UserSubjectId",
                principalSchema: "v2",
                principalTable: "Users",
                principalColumn: "SubjectId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
