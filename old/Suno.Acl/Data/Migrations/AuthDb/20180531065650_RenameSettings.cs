﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Suno.Acl.Data.Migrations.AuthDB
{
    public partial class RenameSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Settings",
                schema: "jup",
                table: "Settings");

            migrationBuilder.RenameTable(
                name: "Settings",
                schema: "jup",
                newName: "UserSettings");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserSettings",
                schema: "jup",
                table: "UserSettings",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_TenantId",
                schema: "jup",
                table: "UserSettings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_UserId",
                schema: "jup",
                table: "UserSettings",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettings_Tenants_TenantId",
                schema: "jup",
                table: "UserSettings",
                column: "TenantId",
                principalSchema: "jup",
                principalTable: "Tenants",
                principalColumn: "TenantId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSettings_Users_UserId",
                schema: "jup",
                table: "UserSettings",
                column: "UserId",
                principalSchema: "jup",
                principalTable: "Users",
                principalColumn: "SubjectId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserSettings_Tenants_TenantId",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSettings_Users_UserId",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserSettings",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.DropIndex(
                name: "IX_UserSettings_TenantId",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.DropIndex(
                name: "IX_UserSettings_UserId",
                schema: "jup",
                table: "UserSettings");

            migrationBuilder.RenameTable(
                name: "UserSettings",
                schema: "jup",
                newName: "Settings");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Settings",
                schema: "jup",
                table: "Settings",
                column: "Id");
        }
    }
}
