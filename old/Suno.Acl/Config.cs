﻿using System;
using System.Collections.Generic;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace Suno.Acl
{
    public class Config
    {


        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                //TODO: Make API resource list Discover
                new ApiResource("productCatalog", "Access right to Product Catalog"),
                new ApiResource("MasterData", "Access right to MasterData"),
                new ApiResource("inventory", "Access right to Inventory"),
                new ApiResource("cashflow", "Access right to Cashflow"),
                new ApiResource("pricing", "Access right to Pricing"),
                new ApiResource("sales", "Access right to Sales"),
                new ApiResource("purchase", "Access right to Purchase"),
                new ApiResource("crm", "Access right to Crm"),
                new ApiResource("admin", "Access right to Admin"),
                new ApiResource("Report", "Access right to Report"),
                new ApiResource("api1", "Access right to Report")
            };
        }

        public static IEnumerable<Client> GetClient()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "service.client",
                    ClientSecrets = { new Secret("secret".Sha256()) },

                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    AllowedScopes = { "api1" }
                },

                new Client
                {
                    ClientId = "ro.client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "api1" }
                }

            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "password"
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password"
                }
            };
        }



        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
            };
        }


    }
}
