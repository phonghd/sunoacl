﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.EntityFrameworkCore;
using ServiceStack;
using Suno.Acl.Data;

using Suno.Acl.Models.Account;
using Suno.Acl.Models.Common;
using Suno.Acl.Models.Company;
using Suno.Acl.Models.Custom;
using Suno.Acl.Models.Data;
using Suno.Acl.Models.UserInfo;
using Suno.Acl.Services.Models;
using Suno.Core.Acl.Domain.V1;
using Suno.Core.Acl.Domain.V2;
using Suno.Core.Hash;

namespace Suno.Acl.Services
{
    public class AuthService
    {
        private readonly AuthDbContext _authDbContext;
        private readonly SaltedHash _saltedHash;
        private readonly IExternalAdapterService _externalService;
        private readonly BasicDataService _basicDataService;

        private readonly CrmService _crmService;
        private readonly Suno.Acl.Infrastructure.CrmSettings _crmSettings;


        public AuthService(AuthDbContext authDbContext, SaltedHash saltedHash, IExternalAdapterService externalService, BasicDataService basicDataService,
            CrmService crmService, Suno.Acl.Infrastructure.CrmSettings crmSettings)
        {
            _authDbContext = authDbContext;
            _saltedHash = saltedHash;
            _externalService = externalService;
            _basicDataService = basicDataService;
            _crmService = crmService;
            _crmSettings = crmSettings;
        }

        public OperationResult CreateNewCompany(SignupInputModel input, OAuthUser oauthUser)
        {
            var result = new OperationResult();

            var originalCompanyCode = CreateCompanyCode(input.CompanyName);
            var companyCode = originalCompanyCode;
            if (IsExistCompany(companyCode))
            {
                var rnd = new Random();
                companyCode = originalCompanyCode + rnd.Next(1, 99);
                if (IsExistCompany(companyCode))
                {
                    companyCode = originalCompanyCode + rnd.Next(1, 99);
                    if (IsExistCompany(companyCode))
                        companyCode = originalCompanyCode + DateTime.Now.Ticks;
                }
            }

            if (oauthUser == null)
            {
                if (IsExistUsername(input.Email))
                    result.Errors.Add("Email đăng nhập đã tồn tại trong hệ thống");
            }
            if (result.Errors != null && result.Errors.Any())
                return result;

            var userClaims = new List<UserClaim>();
            if (oauthUser != null)
            {
                input.Email = oauthUser.Email;

                if (!string.IsNullOrEmpty(oauthUser.Avatar))
                    userClaims.Add(new UserClaim { ClaimType = JwtClaimTypes.Picture, ClaimValue = oauthUser.Avatar });
                if (!string.IsNullOrEmpty(oauthUser.Birthday))
                    userClaims.Add(new UserClaim { ClaimType = JwtClaimTypes.BirthDate, ClaimValue = oauthUser.Birthday });
                if (!string.IsNullOrEmpty(oauthUser.Gender))
                    userClaims.Add(new UserClaim { ClaimType = JwtClaimTypes.Gender, ClaimValue = oauthUser.Gender });
            }

            var appCode = _basicDataService.App.Code;
            var appId = _authDbContext.Apps.Where(p => p.Code == appCode).Select(p => p.AppId).FirstOrDefault();

            var defaultPackageId = _authDbContext.Packages.Where(p => p.AppId == appId && p.IsDefault).Select(p => p.PackageId).FirstOrDefault();
            using (var dbContextTransaction = _authDbContext.Database.BeginTransaction())
            {
                try
                {
                    var tenantId = Guid.NewGuid();
                    var userSubjectId = Guid.NewGuid();
                    var storeId = Guid.NewGuid();
                    string hash, salt;

                    //External user registration has no password input
                    if (!string.IsNullOrEmpty(input.Password))
                        _saltedHash.GetHashAndSaltString(input.Password, out hash, out salt);
                    else
                        hash = salt = string.Empty;

                    var createToDomainSuccess = _externalService.CreateTenantToDomain(tenantId.ToString(), companyCode, input);
                    if (createToDomainSuccess == false)
                        throw new HttpRequestException("Không thể tạo công ty");

                    // Create company
                    var company = new Company
                    {
                        CompanyName = input.CompanyName,
                        CreatedDate = DateTime.Now,
                        Status = 1,
                        CompanyCode = companyCode,
                        LegalRepresentative = input.FullName ?? input.CompanyName,
                        Industry = input.Industry
                    };
                    _authDbContext.Companies.Add(company);
                    _authDbContext.SaveChanges();
                    _authDbContext.Tenant.Add(new Tenant { CompanyId = company.CompanyId, TenantId = tenantId, AppId = appId });

                    _authDbContext.CompanyExtensions.Add(new CompanyExtension { CompanyId = company.CompanyId, ProvinceId = input.ProvinceId });

                    var userProfile = new UserProfile
                    {
                        CompanyId = company.CompanyId,
                        CreatedDate = DateTime.Today,
                        DisplayName = input.FullName ?? input.CompanyName,
                        Email = input.Email,
                        PhoneNumber = input.Phone ?? string.Empty,
                        CultureInfo = "vi-VN",
                        IsActived = true,
                        IsAdmin = true
                    };
                    _authDbContext.UserProfiles.Add(userProfile);
                    _authDbContext.SaveChanges();

                    // Create user
                    var membership = new Membership
                    {
                        CompanyId = company.CompanyId,
                        UserName = input.Email,
                        PasswordSalt = salt,
                        Password = hash,
                        UserId = userProfile.UserId
                    };
                    _authDbContext.Memberships.Add(membership);
                    _authDbContext.SaveChanges();


                    _authDbContext.Users.Add(new User
                    {
                        SubjectId = userSubjectId,
                        UserId = userProfile.UserId,
                        AppId = appId,
                        UserTenants = new List<UserTenant> { new UserTenant { IsDefault = true, TenantId = tenantId } }
                    });

                    if (userClaims.Any())
                    {
                        userClaims.ForEach(uc => uc.UserSubjectId = userSubjectId);
                        _authDbContext.UserClaims.AddRange(userClaims);
                    }

                    if (oauthUser != null)
                    {
                        _authDbContext.OAuthMemberships.Add(new OAuthMembership
                        {
                            Provider = oauthUser.ProviderId,
                            CompanyId = company.CompanyId,
                            ProviderUserId = oauthUser.ProviderUserId,
                            UserId = userProfile.UserId
                        });
                    }

                    _authDbContext.Stores.Add(new Store
                    {
                        CreatedDate = DateTime.Now,
                        CreatedUser = userSubjectId,
                        TenantId = tenantId,
                        StoreId = storeId,
                        StoreName = input.StoreName ?? input.CompanyName,
                        IsDeleted = false,
                        StoreCode = CreateCompanyCode(input.StoreName ?? input.CompanyName)
                    });

                    if (defaultPackageId != 0)
                    {
                        _authDbContext.CompanyPackages.Add(new CompanyPackage
                        {
                            ExpiredDate = DateTime.Now.Date.Add(TimeSpan.FromDays(7)),
                            PackageId = defaultPackageId,
                            Amount = 0,
                            HasNextPackage = false,
                            CreatedUser = userProfile.UserId,
                            CreatedDate = DateTime.Now,
                            CompanyId = company.CompanyId,
                            ActivedDate = DateTime.Now,

                        });

                        _authDbContext.CompanyFeatureLimits.Add(new CompanyFeatureLimit
                        {
                            CompanyId = company.CompanyId,
                            MaxStore = 1,
                            CreatedDate = DateTime.Now,
                            CreatedUser = userProfile.UserId
                        });


                    }

                    //Add User to store
                    _authDbContext.UserTenantStores.Add(new UserTenantStore
                    {
                        UserSubjectId = userSubjectId,
                        StoreId = storeId,
                        TenantId = tenantId
                    });

                    _authDbContext.UserTenantRoles.Add(new UserTenantRoleV2 { UserSubjectId = userSubjectId, TenantId = tenantId, RoleId = 1 });

                    _authDbContext.SaveChanges();
                    dbContextTransaction.Commit();

                    result.Succeeded = true;
                    result.UserId = userSubjectId;
                    result.TenantId = tenantId;


                    if (_crmSettings.Enable)
                    {
                        ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                        var obj = input.ConvertTo<SignupInputModel>();
                        try
                        {
                            using (var client = new WebClient())
                            {
                                var sessionId = _crmService.SugarCRMLogin(client);
                                var provineName = _authDbContext.Provinces.Where(p => p.ProvinceId == input.ProvinceId).Select(p => p.Name).FirstOrDefault();
                                var industryName = _authDbContext.Industries.Where(p => p.Id == input.Industry).Select(p => p.Name).FirstOrDefault();
                                _crmService.SugarCRMAddLead(client, sessionId, company.CompanyId.ToString(), input.StoreName ?? input.CompanyName, input.FullName,
                                    input.FullName, input.Phone, input.Email, string.Empty, provineName, string.Empty, "SunoV2-" + industryName, string.Empty, string.Empty);

                                _crmService.SugarCRMLogout(client, sessionId);

                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    Console.WriteLine(e);
                    throw;
                }
            }

            return result;
        }

        public OperationResult CreateCompanyUser(UserInputModel user)
        {
            if (IsExistUsername(user.Username))
                throw new Exception("The Username is existed");

            _saltedHash.GetHashAndSaltString(user.Password, out var hash, out var salt);
            var tenant = _authDbContext.Tenant.Where(p => p.TenantId == user.TenantId).Select(p => new { p.CompanyId })
                .FirstOrDefault();
            var company = _authDbContext.Companies.Where(p => p.CompanyId == tenant.CompanyId).Select(p => new { p.CompanyCode }).FirstOrDefault();

            var companyCode = company.CompanyCode;
            var result = new OperationResult();

            var now = DateTime.Now;
            //using (var dbContextTransaction = _authDbContext.Database.BeginTransaction())

            var appCode = _basicDataService.App.Code;
            var appId = _authDbContext.Apps.Where(p => p.Code == appCode).Select(p => p.AppId).FirstOrDefault();

            using (var dbTrans = _authDbContext.Database.BeginTransaction())
            {
                try
                {
                    var userSubjectId = Guid.NewGuid();
                    var userEntity = new User { SubjectId = userSubjectId, AppId = appId};
                    var userProfile = new UserProfile
                    {
                        DisplayName = user.DisplayName,
                        User = userEntity,
                        CompanyId = tenant.CompanyId,
                        CreatedDate = now,
                        CreatedUser = 0,
                        CultureInfo = "vi-VN",
                        IsActived = true,
                        PhoneNumber = user.PhoneNumber ?? string.Empty,
                        Email = user.Email ?? string.Empty
                    };
                    _authDbContext.UserProfiles.Add(userProfile);
                    _authDbContext.SaveChanges();

                    // Create user
                    var membership = new Membership
                    {
                        CompanyId = tenant.CompanyId,
                        UserName = user.Username,
                        PasswordSalt = salt,
                        Password = hash,
                        UserId = userProfile.UserId
                    };
                    _authDbContext.Memberships.Add(membership);

                    user.StoreAccess.ForEach(storeId =>
                    {
                        // Add User to store
                        _authDbContext.UserTenantStores.Add(new UserTenantStore
                        {
                            UserSubjectId = userSubjectId,
                            StoreId = storeId,
                            TenantId = user.TenantId
                        });
                    });

                    user.Roles.ForEach(roleId =>
                    {
                        _authDbContext.UserTenantRoles.Add(new UserTenantRoleV2 { UserSubjectId = userSubjectId, TenantId = user.TenantId, RoleId = roleId });
                    });

                    _authDbContext.UserTenants.Add(new UserTenant { IsDefault = true, UserSubjectId = userSubjectId, TenantId = user.TenantId });

                    _authDbContext.SaveChanges();
                    dbTrans.Commit();
                    result.UserId = userSubjectId;
                    result.Succeeded = true;
                    result.TenantId = user.TenantId;

                    return result;
                }
                catch (Exception e)
                {
                    dbTrans.Rollback();
                    throw e;
                }
            }
        }


        public bool ValidateCredentials(Guid userId, string password)
        {
            var user = Users().Where(p => p.User.SubjectId == userId).Select(p => new { p.Membership.Password, p.Membership.PasswordSalt }).FirstOrDefault();

            if (null != user)
                return _saltedHash.VerifyHashString(password, user.Password, user.PasswordSalt);

            return false;
        }


        public ListModel<UserProfileModel> GetListUser(Guid tenantId)
        {
            var result = new ListModel<UserProfileModel>();
            var userIds = _authDbContext.UserTenants.Where(p => p.TenantId == tenantId).Select(p => p.UserSubjectId).ToList();
            if (userIds.Any())
            {
                var userProfiles = Users().Where(p => userIds.Contains(p.User.SubjectId))
                    .Select(p => new UserProfileModel
                    {
                        UserId = p.User.SubjectId,
                        Username = p.Membership.UserName,
                        Email = p.UserProfile.Email,
                        DisplayName = p.UserProfile.DisplayName,
                        IsAdmin = p.UserProfile.IsAdmin,
                        Enabled = p.UserProfile.IsActived,
                        LanguageCode = p.UserProfile.CultureInfo,
                        PhoneNumber = p.UserProfile.PhoneNumber,
                    }).ToList();

                var avatarUsers = _authDbContext.UserClaims.Where(p => userIds.Contains(p.UserSubjectId) && p.ClaimType == JwtClaimTypes.Picture).ToList();
                var userTenantRoles = _authDbContext.UserTenantRoles.Where(p => p.TenantId == tenantId && userIds.Contains(p.UserSubjectId)).ToList();
                var userTenantStores = _authDbContext.UserTenantStores.Where(p => p.TenantId == tenantId && userIds.Contains(p.UserSubjectId)).ToList();

                userProfiles.ForEach(userProfile =>
                {
                    userProfile.Avatar = avatarUsers.Where(a => a.UserSubjectId == userProfile.UserId).Select(a => a.ClaimValue).FirstOrDefault();
                    userProfile.Roles = userTenantRoles.Where(r => r.UserSubjectId == userProfile.UserId).Select(r => r.RoleId).ToList();
                    userProfile.StoreAccess = userTenantStores.Where(p => p.UserSubjectId == userProfile.UserId).Select(p => p.StoreId).ToList();
                });

                result.Items = userProfiles;
                result.Total = userProfiles.Count;
            }
            return result;
        }

        public List<KeyValuePair<int, string>> GetTenantRoles(Guid tenantId)
        {
            var roles = _authDbContext.RolesV2.Where(p => p.IsGlobal || p.TenantId == tenantId).Where(p => p.IsDeleted == false)
                .Select(p => new { p.RoleId, p.Name }).ToList();

            return roles.Select(r => new KeyValuePair<int, string>(r.RoleId, r.Name)).ToList();
        }

        public OperationResult UpdateUserInfo(UserUpdateModel user)
        {
            var result = new OperationResult();
            var userEntity = Users().Where(p => p.User.SubjectId == user.UserId).Select(p => new
            {
                p.UserProfile.UserId,
                p.Membership.Password,
                p.Membership.PasswordSalt,
                p.UserProfile.DisplayName,
                p.UserProfile.PhoneNumber,
                p.UserProfile.CultureInfo,
                p.UserProfile.Email,
                p.User.SubjectId,
                p.UserProfile.IsActived

            }).FirstOrDefault();
            if (userEntity == null)
            {
                result.Errors.Add("Không tìm thấy thông tin người dùng");
                return result;
            }

            string hash = userEntity.Password, salt = userEntity.PasswordSalt;


            if (!string.IsNullOrEmpty(user.Password))
                _saltedHash.GetHashAndSaltString(user.Password, out hash, out salt);





            using (var dbTransaction = _authDbContext.Database.BeginTransaction())
            {
                try
                {

                    var userProfile = _authDbContext.UserProfiles.Where(p => p.UserId == userEntity.UserId).FirstOrDefault();
                    _authDbContext.UserProfiles.Attach(userProfile);
                    userProfile.DisplayName = user.DisplayName ?? userEntity.DisplayName;
                    userProfile.PhoneNumber = user.PhoneNumber ?? userEntity.PhoneNumber;
                    userProfile.CultureInfo = user.LanguageCode ?? userEntity.CultureInfo;
                    userProfile.Email = user.Email ?? userEntity.Email;
                    userProfile.IsActived = user.Enabled.Value;
                    _authDbContext.SaveChanges();

                    var membership = new Membership { UserId = userEntity.UserId };
                    _authDbContext.Memberships.Attach(membership);
                    membership.Password = hash;
                    membership.PasswordSalt = salt;

                    #region "Update Roles"
                    var userTenantRoles = _authDbContext.UserTenantRoles.Where(p => p.UserSubjectId == userEntity.SubjectId && p.TenantId == user.TenantId).ToList();
                    if (!user.Roles.Any())
                        _authDbContext.UserTenantRoles.RemoveRange(userTenantRoles);
                    else
                    {
                        //user.Roles
                        var needToDelete = userTenantRoles.Where(p => !user.Roles.Contains(p.RoleId)).ToList();
                        _authDbContext.UserTenantRoles.RemoveRange(needToDelete);

                        user.Roles.ForEach(roleId =>
                        {
                            if (!userTenantRoles.Where(p => p.RoleId == roleId).Select(p => true).FirstOrDefault())
                            {
                                _authDbContext.UserTenantRoles.Add(new UserTenantRoleV2
                                {
                                    RoleId = roleId,
                                    TenantId = user.TenantId,
                                    UserSubjectId = userEntity.SubjectId
                                });
                            }
                        });
                    }
                    #endregion

                    #region "Update Store"
                    var userTenantStores = _authDbContext.UserTenantStores.Where(p => p.UserSubjectId == userEntity.SubjectId && p.TenantId == user.TenantId).ToList();
                    if (!user.StoreAccess.Any())
                        _authDbContext.UserTenantStores.RemoveRange(userTenantStores);
                    else
                    {
                        //user.Roles
                        var needToDelete = userTenantStores.Where(p => !user.StoreAccess.Contains(p.StoreId)).ToList();
                        _authDbContext.UserTenantStores.RemoveRange(needToDelete);

                        user.StoreAccess.ForEach(storeId =>
                        {
                            if (!userTenantStores.Where(p => p.StoreId == storeId).Select(p => true).FirstOrDefault())
                            {
                                _authDbContext.UserTenantStores.Add(new UserTenantStore
                                {
                                    StoreId = storeId,
                                    TenantId = user.TenantId,
                                    UserSubjectId = userEntity.SubjectId
                                });
                            }
                        });
                    }

                    #endregion

                    _authDbContext.SaveChanges();
                    dbTransaction.Commit();
                    result.Succeeded = true;
                    result.TenantId = user.TenantId;
                    result.UserId = user.UserId;
                }
                catch (Exception e)
                {
                    dbTransaction.Rollback();
                    throw;
                }
            }
            return result;
        }




        public void ChangeUserPassword(ChangePasswordModel user)
        {
            var userRequest = Users().Where(p => p.User != null && p.User.SubjectId == user.ByUserId && p.Membership != null)
                .Select(p => new
                {
                    p.UserProfile.IsAdmin,
                    p.User.SubjectId
                }).FirstOrDefault();
            _saltedHash.GetHashAndSaltString(user.Password, out var hash, out var salt);

            if (userRequest != null)
            {
                if (userRequest.IsAdmin || userRequest.SubjectId == user.UserId)
                {
                    var userNeedToChangePassword = Users().Where(p => p.User != null && p.User.SubjectId == user.UserId && p.Membership != null)
                        .Select(p => new
                        {
                            p.Membership.UserId
                        }).FirstOrDefault();
                    if (userNeedToChangePassword != null)
                    {
                        var membership = new Membership { UserId = userNeedToChangePassword.UserId };
                        _authDbContext.Memberships.Attach(membership);
                        membership.Password = hash;
                        membership.PasswordSalt = salt;
                        _authDbContext.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("The user is not found");
                    }
                }
                else
                {
                    throw new Exception("This user not granted for update other user infomation");
                }
            }

        }




        public void CreateResetToken(int userId, out string tokenString)
        {
            var token = Guid.NewGuid();
            var membership = new Membership { UserId = userId };

            _authDbContext.Memberships.Attach(membership);
            membership.ResetPasswordToken = token;

            _authDbContext.SaveChanges();
            tokenString = token.ToString();
        }



        public OperationResult ResetPassword(int userId, string code, string password)
        {
            _saltedHash.GetHashAndSaltString(password, out var hash, out var salt);
            var result = new OperationResult { };
            var membership = new Membership { UserId = userId };
            _authDbContext.Memberships.Attach(membership);
            membership.Password = hash;
            membership.PasswordSalt = salt;

            _authDbContext.SaveChanges();
            result.Succeeded = true;

            return result;
        }


        #region Private
        //private List<string> CreateCompanyValidator(string companyCode, string username)
        //{
        //    var errors = new List<string>();
        //    if (!IsValidCompany(companyCode))
        //        errors.Add("Tên công ty đã tồn tại");
        //    if (!IsValidUsername(username))
        //        errors.Add("Email đã tồn tại");
        //    return errors;
        //}

        private string CreateCompanyCode(string str)
        {
            str = str.ToLower();

            str = Regex.Replace(str, @"à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ", "a");
            str = Regex.Replace(str, @"è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/", "e");
            str = Regex.Replace(str, @"ì|í|ị|ỉ|ĩ", "i");
            str = Regex.Replace(str, @"ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ", "o");
            str = Regex.Replace(str, @"ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ", "u");
            str = Regex.Replace(str, @"ỳ|ý|ỵ|ỷ|ỹ", "y");
            str = Regex.Replace(str, @"đ", "d");
            str = Regex.Replace(str, @"\!|\@|%|^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\""|\&|\#|\[|\]|~|$|_", string.Empty);
            str = Regex.Replace(str, @"[^0-9a-zA-Z-]+", String.Empty);

            str = Regex.Replace(str, @"-+-", "-");
            str = Regex.Replace(str, @"^\-+|\-+$", "");
            str = Regex.Replace(str, @"congty", String.Empty);
            str = Regex.Replace(str, @"cuahang", String.Empty);

            return str;
        }

        private bool IsExistCompany(string companyCode)
        {
            return _authDbContext.Companies.Where(p => p.CompanyCode == companyCode).Select(p => true).FirstOrDefault();
        }

        private bool IsExistUsername(string username)
        {
            return _authDbContext.Memberships.Where(p => p.UserName == username)
                .Select(p => true).FirstOrDefault();

        }
        #endregion


        public IQueryable<Models.UserModel> Users()
        {
            var query = (from m in _authDbContext.Memberships
                         join u in _authDbContext.Users on m.UserId equals u.UserId into tmpUser
                         from tu in tmpUser.DefaultIfEmpty()
                         join p in _authDbContext.UserProfiles on m.UserId equals p.UserId into tmpProfile
                         from tp in tmpProfile.DefaultIfEmpty()
                         select new Models.UserModel
                         {
                             Membership = m,
                             UserProfile = tp,
                             User = tu
                         });

            return query;
        }
    }
}

