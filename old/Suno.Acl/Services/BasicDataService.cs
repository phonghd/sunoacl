﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using ServiceStack;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Core.Acl.Domain;
using Suno.Core.Acl.Domain.V2;
using Feature = Suno.Core.Acl.Domain.V1.Feature;
using Package = Suno.Core.Acl.Domain.V1.Package;
using PackageFeatureLimit = Suno.Core.Acl.Domain.V1.PackageFeatureLimit;

namespace Suno.Acl.Services
{
    public class BasicDataService
    {
        private readonly IHostingEnvironment _hostingEnvironment;

        public BasicDataService(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        public List<RoleV2> Roles = new List<RoleV2>
        {
            new RoleV2{ RoleId = 1, Name = "Chủ cửa hàng"},
            new RoleV2{ RoleId = 2, Name = "Quản lý"},
            new RoleV2{ RoleId = 3, Name = "Nhân viên kho"},
            new RoleV2{ RoleId = 4, Name = "Nhân viên thu ngân"},
            new RoleV2{ RoleId = 5, Name = "Nhân viên bán hàng"}
        };

        public List<PermissionV2> Permissions = new List<PermissionV2>
        {
            new PermissionV2 {PermissionCode = "Product_Create", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Product_Read", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 {PermissionCode = "Product_Update", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Product_Delete", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Product_Search", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Buy_Price_Update", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
            }},
            new PermissionV2 { PermissionCode = "Buy_Price_Read", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
            }},
            new PermissionV2 { PermissionCode = "Company_Settings", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
            }},
            new PermissionV2 { PermissionCode = "Template_Settings", Scope = "productCatalog", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
            }},


            new PermissionV2 { PermissionCode = "Stock_Take_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Take_Create", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Take_Update", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 {PermissionCode = "Stock_Take_Delete", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Transfer_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 {PermissionCode = "Stock_Transfer_Create", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Transfer_Update", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Transfer_Delete", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Inventory_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},
            new PermissionV2 { PermissionCode = "Stock_Card_Read", Scope = "inventory", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
            }},


            new PermissionV2 { PermissionCode = "Pos_Order_Create", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 {PermissionCode = "Pos_Order_Read", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 {PermissionCode = "Pos_Order_Update", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Pos_Order_Delete", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Online_Order_Create", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 {PermissionCode = "Online_Order_Read", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Online_Order_Update", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Online_Order_Delete", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Sales_Return_Create", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Sales_Return_Read", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5},
            }},
            new PermissionV2 { PermissionCode = "Sales_Return_Update", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Sales_Return_Delete", Scope = "sales", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},

            new PermissionV2 { PermissionCode = "Purchase_Order_Create", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Purchase_Order_Read", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Purchase_Order_Update", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Purchase_Order_Delete", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Purchase_Return_Create", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Purchase_Return_Read", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 { PermissionCode = "Purchase_Return_Update", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},
            new PermissionV2 {PermissionCode = "Purchse_Return_Delete", Scope = "purchase", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
            }},


            new PermissionV2 { PermissionCode = "Receipt_Voucher_Create", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}

            }},
            new PermissionV2 { PermissionCode = "Receipt_Voucher_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Receipt_Voucher_Update", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Receipt_Voucher_Delete", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Payment_Voucher_Create", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Payment_Voucher_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 {PermissionCode = "Payment_Voucher_Update", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Payment_Voucher_Delete", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Cashflow_Summary_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Cashflow_Ledger_Read", Scope = "cashflow", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},



            new PermissionV2 { PermissionCode = "Customer_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Customer_Search", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Customer_Owner_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 {PermissionCode = "Customer_Owner_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Customer_Owner_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Customer_Owner_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Customer_All_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Group_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Customer_Campaign_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Customer_Campaign_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Campaign_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Customer_Campaign_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Loyalty_Settings", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Customer_Campaign_Settings", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Search", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Create", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 {PermissionCode = "Supplier_Owner_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Supplier_All_Read", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Ower_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 {PermissionCode = "Supplier_All_Update", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Supplier_Owner_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Supplier_All_Delete", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 {PermissionCode = "Supplier_Owner_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 {PermissionCode = "Supplier_All_Export", Scope = "crm", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},


            new PermissionV2 {PermissionCode = "Promotion_Create", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Promotion_Read", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Promotion_Update", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Promotion_Delete", Scope = "pricing", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},


            new PermissionV2 { PermissionCode = "Employee_Create", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Employee_Read", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Employee_Update", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 { PermissionCode = "Employee_Delete", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 { PermissionCode = "Store_Settings", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Audit_Trail", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},
            new PermissionV2 {PermissionCode = "Pricing_Plan", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2}
            }},


            new PermissionV2 { PermissionCode = "Daily_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4},
                new RolePermissionV2{RoleId = 5}
            }},
            new PermissionV2 { PermissionCode = "Revenue_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Inventory_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 3}
            }},
            new PermissionV2 {PermissionCode = "Employee_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1},
                new RolePermissionV2{RoleId = 2},
                new RolePermissionV2{RoleId = 4}
            }},
            new PermissionV2 { PermissionCode = "Profit_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},
            new PermissionV2 {PermissionCode = "Profit_Loss_Report", Scope = "Report", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }},

            new PermissionV2 { PermissionCode = "Manage_Access", Scope = "MasterData", RolePermissions = new List<RolePermissionV2>
            {
                new RolePermissionV2{RoleId = 1}
            }}

        };

        public App App = new App
        {
            Code = StaticData.PosV2.Code,
            Name = StaticData.PosV2.Name,
            Version = StaticData.PosV2.Version,

            Packages = new List<Package>
            {
                new Package
                {
                    CreatedDate = DateTimeHelper.Now,
                    CreatedUser = 0,
                    IsDefault = true,
                    IsFree = false,
                    IsTrial = false,
                    Key = "PosV2Basic",
                    Name = "Basic",
                    PackageId = 10
                },
                new Package
                {
                    CreatedDate = DateTimeHelper.Now,
                    CreatedUser = 0,
                    IsDefault = false,
                    IsFree = false,
                    IsTrial = false,
                    Key = "PosV2Pro",
                    Name = "Pro",
                    PackageId = 11
                },
                new Package
                {
                    CreatedDate = DateTimeHelper.Now,
                    CreatedUser = 0,
                    IsDefault = false,
                    IsFree = false,
                    IsTrial = false,
                    Key = "PosV2Advanced",
                    Name = "Advanced",
                    PackageId = 12
                }
            }
        };

        public void SeedData(AuthDbContext authDbContext)
        {
            if (!authDbContext.Clients.Any())
            {
                authDbContext.Clients.Add(new Client
                {
                    ClientId = "js",
                    ClientName = "Suno Pos - Local",
                    AccessTokenLifetime = 86400,
                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true,
                    ClientUri = "http://localhost:4200,http://localhost:4200/silent-renew.html,http://localhost:4200/auth-callback",
                    IdentityTokenLifetime = 90000,
                    RequireConsent = false,
                    Enabled = true,
                    RedirectUris = "http://localhost:4200,http://localhost:4200/silent-renew.html,http://localhost:4200/auth-callback",
                    PostLogoutRedirectUris = "http://localhost:4200",
                    ClientScopes = (new List<string> { "openid", "offline_access", "productCatalog", "MasterData", "inventory", "pricing", "cashflow", "sales", "purchase", "crm", "Report" })
                    .Select(p => new ClientScope { Scope = p }).ToList(),
                    ClientGrantTypes = new List<ClientGrantType> { new ClientGrantType { GrantType = "implicit" } }
                });

                authDbContext.SaveChanges();
            }

            var appId = authDbContext.Apps.Where(p => p.Code == App.Code).Select(p => p.AppId).FirstOrDefault();
            if (appId == 0)
            {
                var app = App.CreateCopy();
                authDbContext.Apps.Add(app);
                authDbContext.SaveChanges();
                appId = app.AppId;
            }

            if (!authDbContext.Features.Where(p => p.AppId == appId).Select(p => true).FirstOrDefault())
            {
                var content = new List<string>
                                   {
                                       "Quản lý hàng hóa; QLHangHoa",
                                       "Quản lý bán hàng, đặt hàng; QLBanHangDatHang",
                                       "Quản lý kho; QLKho",
                                       "Quản lý khách hàng / nhà cung cấp; QLKhachHangNCC",
                                       "Báo cáo doanh số; BaoCaoDoanhSo",
                                       "Thiết lập, phân quyền; SettingsPermission",
                                       "Quản lý công nợ; QLCongNo",
                                       "Quản lý khuyến mãi; QLKhuyenMai",
                                       "Quản lý tích lũy điểm; QLTichLuyDiem",
                                       "Quản lý dòng tiền, sổ quỹ; QLDongTienSoQuy",
                                       "Báo cáo kinh doanh; BaoCaoKinhDoanh",
                                       "Rao hàng Zalo; RaoHangZalo",
                                       "Rao hàng Facebook; RaoHangFacebook",
                                       "Bán hàng từ Zalo; BanHangZalo",
                                       "Bán hàng từ Facebook; BanHangFacebook",
                                       "Chăm sóc khách hàng(SMS, Zalo); CSKH",
                                       "Kết nối website(*); KetNoiWebsite",
                                       "Kết nối vận chuyển; KetNoiVanChuyen"
                                   };
                var featureList = content
                    .Select(line => { return line.Split(';').Select(p => p.Trim()).ToList(); }).ToList()
                    .Select(p => new Feature
                    {
                        Description = p[0],
                        Name = p[0],
                        FeatureKey = p[1],
                        CreatedDate = DateTimeHelper.Now,
                        AppId = appId
                    }).ToList();

                authDbContext.Features.AddRange(featureList);
                authDbContext.SaveChanges();
            }

            if (authDbContext.PackageFeatureLimits.Where(p => p.PackageId == 10).Select(p => true).FirstOrDefault())
                authDbContext.PackageFeatureLimits.Add(new PackageFeatureLimit { MaxUser = 3, MaxProduct = 100, MaxCustomer = 500, PackageId = 10 });

            if (authDbContext.PackageFeatureLimits.Where(p => p.PackageId == 11).Select(p => true).FirstOrDefault())
                authDbContext.PackageFeatureLimits.Add(new PackageFeatureLimit { PackageId = 11 });

            if (authDbContext.PackageFeatureLimits.Where(p => p.PackageId == 12).Select(p => true).FirstOrDefault())
                authDbContext.PackageFeatureLimits.Add(new PackageFeatureLimit { PackageId = 12 });

            authDbContext.SaveChanges();

            Roles.ForEach(role =>
            {
                if (!authDbContext.RolesV2.Where(p => p.RoleId == role.RoleId).Select(p => true).FirstOrDefault())
                {
                    var sql = $"SET IDENTITY_INSERT [{AuthDbContext.schemaV2}].[Roles] ON; insert  [{AuthDbContext.schemaV2}].[Roles] ( RoleId, Name, CreatedDate, IsGlobal, CreatedUser, IsDeleted) VALUES( {role.RoleId} , N'{role.Name}', '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}', 1, 0, 0); SET IDENTITY_INSERT [{AuthDbContext.schemaV2}].[Roles] OFF";
                    authDbContext.Database.ExecuteSqlCommand(sql);
                }
            });

            Permissions.ForEach(permission =>
            {
                if (!authDbContext.PermissionsV2.Where(p => p.PermissionCode == permission.PermissionCode).Select(p => true).FirstOrDefault())
                {
                    permission.CreatedDate = DateTimeHelper.Now;
                    authDbContext.PermissionsV2.Add(permission);
                }
            });
            authDbContext.SaveChanges();






        }
    }
}
