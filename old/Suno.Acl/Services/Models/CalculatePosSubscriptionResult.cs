﻿using System;

namespace Suno.Acl.Services.Models
{
    public class CalculatePosSubscriptionResult
    {
        public decimal TotalAmount { get; set; }
        public DateTime ActivedDate { get; set; }
        public DateTime ExpiredDate { get; set; }

    }
}

