namespace Suno.Acl.Services.Models
{
    public class OAuthUser
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string DisplayName { get; set; }
        public string Avatar { get; set; }
        public string Gender { get; set; }
        public string Birthday { get; set; }
        public string ProviderId { get; set; }
        public string ProviderUserId { get; set; }
    }
}