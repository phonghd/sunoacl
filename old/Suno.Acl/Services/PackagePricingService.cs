﻿using System;
using System.Collections.Generic;
using System.Linq;
using Suno.Acl.Services.Models;

namespace Suno.Acl.Services
{
    public class PackagePricingService
    {
        public List<PackagePricingModel> PricingModels { get; set; }

        public PackagePricingService()
        {
            PricingModels = new List<PackagePricingModel>
            {
                new PackagePricingModel
                {
                    PackageId =  10,
                    Price = 90000,
                    Label = "Cơ bản",
                    Name = "Basic",
                    PriceText = "Chỉ 90k/tháng",
                    Features = new List<string>{"Quản lý hàng hóa", "Quản lý bán hàng, đặt hàng", "Quản lý kho", "Quản lý khách hàng/nhà cung cấp", "Báo cáo doanh số", "Thiết lập, phân quyền", "3 người dùng", "100 hàng hóa", "500 khách hàng"},
                },
                new PackagePricingModel
                {
                    PackageId =  11,
                    Price = 199000,
                    Label = "Nâng cao",
                    Name = "Pro",
                    PriceText = "Chỉ <span class='line-through'>299K</span> 199K/tháng",
                    Features = new List<string>{"Bao gồm gói Cơ bản", "Quản lý công nợ", "Quản lý khuyến mãi", "Quản lý tích lũy điểm", "Quản lý dòng tiền, sổ quỹ", "Báo cáo kinh doanh", "Thiết lập, phân quyền", "KHÔNG giới hạn người dùng", "KHÔNG giới hạn hàng hóa", "KHÔNG giới hạn đơn hàng", "KHÔNG giới hạn dung lượng", "KHÔNG tăng giá"},
                    IsRecommended = true
                },
                new PackagePricingModel
                {
                    PackageId =  12,
                    Price = 90000,
                    Label = "Omnichannel",
                    Name = "Omnichannel",
                    PriceText = "<span class='line-through'>699K</span> 499K/tháng ",
                    Features = new List<string>{"Bao gồm gói Nâng cao", "Rao hàng Facebook", "Rao hàng Zalo", "Bán hàng từ Facebook", "Bán hàng từ Zalo", "Chăm sóc khách hàng (SMS,Zalo)", "Kết nối website (*)", "Kết nối vận chuyển", "Open API tích hợp"},
                },
            };
        }

        public CalculatePosSubscriptionResult CalculatePosSubscriptionPrice(CreatePosSubscriptionRequest request)
        {
            var activedDate = request.ActivedDate ?? DateTime.Now;
            var result = new CalculatePosSubscriptionResult();
            var package = this.PricingModels.Where(p => p.PackageId == request.PackageId).FirstOrDefault();
            var totalAmount = package.Price * request.Month;

            result.TotalAmount = totalAmount;
            result.ActivedDate = activedDate;
            result.ExpiredDate = activedDate.AddMonths(request.Month);
            return result;
        }


    }
}

