﻿using System.Threading.Tasks;

namespace Suno.Acl.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
        Task SendResetPasswordEmail(string mailto, string subject, string message);
    }
}