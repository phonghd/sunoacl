﻿using System;
using Suno.Acl.Models.Common;

namespace Suno.Acl.Models.UserInfo
{
    public class ChangePasswordModel : OperationData
    {
        public Guid UserId { get; set; }
        public string Password { get; set; }
    }
}
