﻿using System;
using System.Collections.Generic;
using Suno.Acl.Models.Common;

namespace Suno.Acl.Models.UserInfo

{
    public class UserInputModel : OperationData
    {
        public string Username { get; set; }
        public List<int> Roles { get; set; }
        public List<Guid> StoreAccess { get; set; }
        public string DisplayName { get; set; }
        public string LanguageCode { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public UserInputModel()
        {
            Roles = new List<int>();
            StoreAccess = new List<Guid>();
        }
    }

    public class UserUpdateModel : UserInputModel
    {
        public Guid UserId { get; set; }
        public bool? Enabled { get; set; }
    }
}
