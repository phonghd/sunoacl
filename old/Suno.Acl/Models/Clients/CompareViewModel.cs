﻿using Suno.Core.Acl.Domain.V2;

namespace Suno.Acl.Models.Clients
{
    public class CompareViewModel
    {
        public Client FirstClient { get; set; }
        public Client SecondClient { get; set; }
    }
}
