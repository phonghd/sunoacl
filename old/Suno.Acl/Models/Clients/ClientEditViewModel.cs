﻿using System.Collections.Generic;
using Suno.Acl.Domain.DTOs;

namespace Suno.Acl.Models.Clients
{
    public class ClientEditViewModel : Domain.DTOs.Clients
    {
        public List<ClientScopes> ClientScopes { get; set; }
    }
}
