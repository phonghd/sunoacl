﻿using System.ComponentModel.DataAnnotations;

namespace Suno.Acl.Models.Account
{
    public class SignupInputModel
    {
        public string FullName { get; set; }
        [Required(ErrorMessage="địa chỉ email")]
        public string Email { get; set; }
        [Required(ErrorMessage="mật khẩu")]
        public string Password { get; set; }
        [Required(ErrorMessage="tên công ty")]
        public string CompanyName { get; set; }
        public string StoreName { get; set; }
        public string Phone { get; set; }
        public string ReturnUrl { get; set; }
        public string ProviderSubjectId { get; set; }
        public string ProviderName { get; set; }

        public int? Industry { get; set; }
        public int? ProvinceId { get; set; }

    }
}
