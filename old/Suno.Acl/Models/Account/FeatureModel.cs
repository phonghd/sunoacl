﻿namespace Suno.Acl.Models.Account
{
    public class FeatureModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string FeatureKey { get; set; }
    }
}
