﻿namespace Suno.Acl.Models.Account
{
    public class PackageFlashMsg
    {
        public bool Show { get; set; }
        public string Type { get; set; }
        public string Message { get; set; }
    }
}
