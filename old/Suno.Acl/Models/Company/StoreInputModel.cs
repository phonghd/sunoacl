﻿using System;
using System.Runtime.Serialization;
using Suno.Acl.Models.Common;

namespace Suno.Acl.Models.Company
{
    public class StoreInputModel : OperationData
    {
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public string StorePhone { get; set; }
        public string TaxCode { get; set; }
        public string StoreCode { get; set; }
        public Guid StoreId { get; set; }
    }
}
