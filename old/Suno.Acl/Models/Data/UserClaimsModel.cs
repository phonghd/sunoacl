﻿using System;

namespace Suno.Acl.Models.Data
{
    public class UserClaimsModel
    {
        public string DisplayName { get; set; }
        public string Username { get; set; }
        public Guid TenantId { get; set; }
        public Guid UserId { get; set; }
        public bool IsActive { get; set; }
    }
}
