﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Suno.Acl.Models.Common
{
    public class Paging<T>
    {
        public List<T> Items { get; set; }
        public long Total { get; set; }

        public Paging()
        {
            Items = new List<T>();
        }
    }

    public class ListModel<T>
    {
        public List<T> Items { get; set; }
        public long Total { get; set; }

        public ListModel()
        {
            Items = new List<T>();
        }
    }

    public class KeyValueModel<T>
    {
        public T Value { get; set; }
        public string Name { get; set; }
    }

}
