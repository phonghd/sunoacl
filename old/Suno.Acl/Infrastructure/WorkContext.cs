﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using Microsoft.AspNetCore.Http;
using Suno.Acl.Data;
using Suno.Acl.Services;

namespace Suno.Acl.Infrastructure
{
    public class WorkContext
    {
        private readonly ClaimsPrincipal _principal;
        private UserProfile _userProfile;

        private readonly AuthService _authService;
        private readonly AuthDbContext _authDbContext;

        public WorkContext(IHttpContextAccessor httpContext, AuthService authService, AuthDbContext authDbContext)
        {
            _authService = authService;
            _authDbContext = authDbContext;
            _principal = httpContext.HttpContext.User;
        }
        public bool IsAuthenticated => _principal.Identity.IsAuthenticated;

        public Guid UserId => _principal.Claims.Select(c => new { c.Type, c.Value }).Where(p => p.Type == "sub").Select(p => Guid.Parse(p.Value)).FirstOrDefault();
        public UserProfile UserProfile
        {
            get
            {
                if (!IsAuthenticated)
                    return null;
                if (_userProfile != null)
                    return _userProfile;

                _userProfile = _authService.Users().Where(p => p.User != null && p.Membership != null && p.User.SubjectId == UserId)
                    .Select(p => new UserProfile
                    {
                        UserName = p.Membership.UserName,
                        Email = p.UserProfile.Email,
                        DisplayName = p.UserProfile.DisplayName,
                        PhoneNumber = p.UserProfile.PhoneNumber,
                    }).FirstOrDefault();


                if (_userProfile != null)
                {
                    _userProfile.Avatar = _authDbContext.UserClaims.Where(p => p.UserSubjectId == UserId && p.ClaimType == JwtClaimTypes.Picture).Select(p => p.ClaimValue).FirstOrDefault();
                    var userTenants = _authDbContext.UserTenants.Where(p => p.UserSubjectId == UserId).ToList();
                    _userProfile.TenantIds = userTenants.Select(p => p.TenantId).ToList();
                    _userProfile.TenantId = userTenants.OrderByDescending(p => p.IsDefault).Select(p => p.TenantId.ToString()).FirstOrDefault();
                }

                return _userProfile;
            }

        }
    }

    public class ApiWorkContext
    {
        private readonly ClaimsPrincipal _principal;
        private readonly HttpContext _httpContext;

        public ApiWorkContext(IHttpContextAccessor httpContext)
        {
            _principal = httpContext.HttpContext.User;
            _httpContext = httpContext.HttpContext;
        }

        public bool IsAuthenticated => _principal.Identity.IsAuthenticated;
        public Guid UserId => _principal.Claims.Select(c => new { c.Type, c.Value }).Where(p => p.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").Select(p => Guid.Parse(p.Value)).FirstOrDefault();

        //public Guid UserId => Guid.Parse(_principal.Identity.Name);
        public Guid TenantId => Guid.Parse(_httpContext.Request.Headers["tenantId"].ToString());
        public int MembershipId => _principal.Claims.Select(c => new { c.Type, c.Value }).Where(p => p.Type == "membershipId").Select(p => int.Parse(p.Value)).FirstOrDefault();
        public int CompanyId => _principal.Claims.Select(c => new { c.Type, c.Value }).Where(p => p.Type == "companyId").Select(p => int.Parse(p.Value)).FirstOrDefault();

        public List<Guid> TenantIds
        {
            get
            {
                var tenantIdsClaim = _principal.Claims.Select(c => new { c.Type, c.Value }).Where(p => p.Type == "TenantIds").Select(p => p.Value).FirstOrDefault();
                if (string.IsNullOrEmpty(tenantIdsClaim))
                    return new List<Guid>();
                return tenantIdsClaim.Split(",").Select(Guid.Parse).ToList();
            }
        }
    }
}
