﻿using System;
using System.Collections.Generic;

namespace Suno.Acl.Infrastructure
{
    public class UserProfile
    {
        public string DisplayName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Avatar { get; set; }
        public string UserName { get; set; }
        public string TenantId { get; set; }

        public List<Guid> TenantIds { get; set; }
    }
}
