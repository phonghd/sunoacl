﻿namespace Suno.Acl.Infrastructure
{
    public class AppSettings
    {
        public string AppsPosUrl { get; set; }
        public string AppsCdnUrl { get; set; }
        public bool EnableOAuthFacebook { get; set; }
        public bool EnableOAuthZalo { get; set; }
        public bool EnableOAuthGoogle { get; set; }
        public string SunoOAuthV1Endpoint { get; set; }
        public bool EnableV1Login { get; set; }

        public bool PosAsDefaultApp { get; set; }
    }

    public class CrmSettings
    {
        public string Host { get; set; }
        public bool Enable { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
