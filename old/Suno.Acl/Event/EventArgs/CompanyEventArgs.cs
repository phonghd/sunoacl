﻿using System;

namespace Suno.Acl.Event.EventArgs
{
    public class CompanyEventArgs : System.EventArgs
    {
        //Company
        public Guid TenantId { get; set; }
    }
}