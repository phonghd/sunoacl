﻿using Suno.Acl.Domain.DTOs;

namespace Suno.Acl.Event
{
    public class CompanyData : Company
    {
        public bool IsSynced { get; set; }
    }
}