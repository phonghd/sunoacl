﻿using System;
using Suno.Acl.Event.EventArgs;

namespace Suno.Acl.Event
{
    public class CompanyEventSender : ICompanyEventSender
    {
        public event EventHandler<CompanyEventArgs> CompanyCreated;

        public void RaiseEvent(CompanyEventArgs data)
        {
            OnCompanyCreated(data);
        }

        protected virtual void OnCompanyCreated(CompanyEventArgs e)
        {
            EventHandler<CompanyEventArgs> handler = CompanyCreated;
            handler?.Invoke(this, e);
        }
    }
}