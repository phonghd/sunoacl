﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Acl.Infrastructure;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Acl.Controllers
{
    [Produces("application/json")]
    [Route("api/Invoices")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class InvoicesController : Controller
    {
        private readonly AuthDbContext _authDbContext;
        private readonly ApiWorkContext _workContext;

        public InvoicesController(AuthDbContext authDbContext, ApiWorkContext workContext)
        {
            _authDbContext = authDbContext;
            _workContext = workContext;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var invoices = await _authDbContext.Invoices.Where(p => p.CompanyId == _workContext.CompanyId)
                .Include(p => p.InvoiceDetails).Include(p => p.InvoicePayments).OrderByDescending(p => p.Id).ToListAsync();
            invoices.ForEach(invoice =>
            {
                invoice.InvoiceDetails?.ForEach(detail =>
                {
                    detail.Invoice = null;
                });

                invoice.InvoicePayments?.ForEach(payment => { payment.Invoice = null; });
            });

            return Ok(invoices);
        }


    }
}