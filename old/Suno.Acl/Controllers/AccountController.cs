using IdentityModel;
using IdentityServer4.Services;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using IdentityServer4.Events;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using Suno.Acl.Models.Account;
using System.IdentityModel.Tokens.Jwt;
using System.Text.Encodings.Web;
using System.Web;
using Core.Flash;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Suno.Acl.Data;
using Suno.Acl.Extensions;
using Suno.Acl.Filters;
using Suno.Acl.Helpers;
using Suno.Acl.Infrastructure;
using Suno.Acl.Models.UserInfo;
using Suno.Acl.Services;
using Suno.Acl.Services.Models;
using Suno.Core.Hash;
using Suno.Core.Acl.Domain.V1;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Suno.Acl
{
    [SecurityHeaders]
    public class AccountController : BaseController
    {
        //private readonly TestUserStore _users;
        private readonly IIdentityServerInteractionService _interaction;
        private readonly IClientStore _clientStore;
        private readonly IAuthenticationSchemeProvider _schemeProvider;
        private readonly IEventService _events;
        private readonly IEmailSender _emailSender;
        private readonly IFlasher _flashMessage;
        private readonly WorkContext _workContext;
        private readonly AppSettings _appSettings;
        private readonly SaltedHash _saltedHash;
        private readonly AuthDbContext _authDbContext;
        private readonly AuthService _authService;
        private readonly CryptographyHelper _rsa;
        private readonly BasicDataService _basicDataService;

        public AccountController(
            IIdentityServerInteractionService interaction,
            IClientStore clientStore,
            IAuthenticationSchemeProvider schemeProvider,
            IEventService events,
            IEmailSender emailSender,
            IFlasher flashMessage, WorkContext workContext, AppSettings appSettings, SaltedHash saltedHash, AuthDbContext authDbContext, AuthService authService, CryptographyHelper rsa, BasicDataService basicDataService)
        {
            //_users = users;
            _interaction = interaction;
            _clientStore = clientStore;
            _schemeProvider = schemeProvider;
            _events = events;
            _emailSender = emailSender;
            //_handler = handler;
            _flashMessage = flashMessage;
            _workContext = workContext;
            _appSettings = appSettings;
            _saltedHash = saltedHash;
            _authDbContext = authDbContext;
            _authService = authService;
            _rsa = rsa;
            _basicDataService = basicDataService;

            //_handler.AddHandler(_cEvent);
        }


        [TempData]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Show login page
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl = "")
        {

            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                    return Redirect(returnUrl);
                return Redirect("/");
            }
            // build a model so we know what to show on the login page
            var vm = await BuildLoginViewModelAsync(returnUrl);
            if (vm.IsExternalLoginOnly)
            {
                // we only have one option for logging in and it's an external provider
                return ExternalLogin(vm.ExternalLoginScheme, returnUrl);
            }

            

            return View(vm);
        }

        /// <summary>
        /// Handle postback from username/password login
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginInputModel model, string button)
        {


            if (button != "login")
            {
                // the user clicked the "cancel" button
                var context = await _interaction.GetAuthorizationContextAsync(model.ReturnUrl);
                if (context != null)
                {
                    // if the user cancels, send a result back into IdentityServer as if they 
                    // denied the consent (even if this client does not require consent).
                    // this will send back an access denied OIDC error response to the client.
                    await _interaction.GrantConsentAsync(context, ConsentResponse.Denied);

                    // we can trust model.ReturnUrl since GetAuthorizationContextAsync returned non-null
                    return Redirect(model.ReturnUrl);
                }
                else
                {
                    // since we don't have a valid context, then we just go back to the home page
                    return Redirect("~/");
                }
            }

            if (ModelState.IsValid)
            {
                model.Username = model.Username.Trim();
                // validate username/password against in-memory store
                var membership = _authDbContext.Memberships.Where(p => p.UserName == model.Username).Select(p => new { p.PasswordSalt, p.Password, p.UserId }).FirstOrDefault();

                if (membership != null && _saltedHash.VerifyHashString(model.Password, membership.Password, membership.PasswordSalt))
                {
                    var user = _authDbContext.Users.Include(p => p.App).FirstOrDefault(p => p.UserId == membership.UserId);
                    if (user != null && user.App != null && user.App.Code != _basicDataService.App.Code)
                        return await AllowLoginAsyc(model, user.SubjectId);

                    if (user != null)
                        return await AllowLoginAsyc(model, user.SubjectId);

                    if (_appSettings.EnableV1Login)
                    {
                        // check package in version 1
                        var companyId = _authDbContext.UserProfiles.Where(p => p.UserId == membership.UserId)
                            .Where(p => p.CompanyId.HasValue && p.CompanyId.Value > 0)
                            .Select(p => p.CompanyId.Value).FirstOrDefault();

                        var dateTimeNow = DateTimeHelper.Now;
                        var currentPackage = _authDbContext.CompanyPackages.FirstOrDefault(p => p.CompanyId == companyId && p.ActivedDate <= dateTimeNow && p.ExpiredDate > dateTimeNow);

                        if (currentPackage != null)
                        {
                            if (_appSettings.EnableV1Login)
                                return RedirectToAuthV1(model);
                        }
                        else
                            ModelState.AddModelError("", "Gói dịch vụ của bạn không hợp lệ hoặc đã hết hạn sử dụng, vui lòng liên hệ quản lý để biết thêm chi tiết");
                        return RedirectToAuthV1(model);
                    }



                }
                else
                    ModelState.AddModelError("", AccountOptions.InvalidCredentialsErrorMessage);

                await _events.RaiseAsync(new UserLoginFailureEvent(model.Username, "invalid credentials"));

            }

            // something went wrong, show form with error
            var vm = await BuildLoginViewModelAsync(model);
            return View(vm);
        }

        private IActionResult RedirectToAuthV1(LoginInputModel model)
        {
            // try to login to V1
            var token = _rsa.Encrypt(JsonConvert.SerializeObject(new
            {
                UserName = model.Username,
                Password = model.Password
            }), _rsa.DefaultSecretKey);
            return Redirect(_appSettings.SunoOAuthV1Endpoint + "?token=" + HttpUtility.UrlEncode(token));
        }

        private async Task<IActionResult> AllowLoginAsyc(LoginInputModel model, Guid userSubjectId)
        {
            await _events.RaiseAsync(new UserLoginSuccessEvent(model.Username, userSubjectId.ToString(), model.Username));

            // only set explicit expiration here if user chooses "remember me". 
            // otherwise we rely upon expiration configured in cookie middleware.
            AuthenticationProperties props = null;
            if (AccountOptions.AllowRememberLogin && model.RememberLogin)
            {
                props = new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTimeOffset.UtcNow.Add(AccountOptions.RememberMeLoginDuration)
                };
            }

            // issue authentication cookie with subject ID and username
            //await HttpContext.SignInAsync(user.SubjectId, user.Username, props);
            await HttpContext.SignInAsync(userSubjectId.ToString(), model.Username, props);

            // make sure the returnUrl is still valid, and if so redirect back to authorize endpoint or a local page
            // the IsLocalUrl check is only necessary if you want to support additional local pages, otherwise IsValidReturnUrl is more strict
            if (_interaction.IsValidReturnUrl(model.ReturnUrl) || Url.IsLocalUrl(model.ReturnUrl))
            {
                return Redirect(model.ReturnUrl);
            }

            return Redirect("~/");
        }

        /// <summary>
        /// initiate roundtrip to external authentication provider
        /// </summary>
        [HttpGet]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            // start challenge and roundtrip the return URL and 
            var props = new AuthenticationProperties()
            {
                RedirectUri = Url.Action("ExternalLoginCallback", new { redirectUrl = returnUrl }),
                Items =
                    {
                        { "returnUrl", returnUrl },
                        { "scheme", provider },
                    }
            };

            return Challenge(props, provider);
        }

        /// <summary>
        /// Post processing of external authentication
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> ExternalLoginCallback(string redirectUrl)
        {


            // read external identity from the temporary cookie
            var result = await HttpContext.AuthenticateAsync(IdentityServer4.IdentityServerConstants.ExternalCookieAuthenticationScheme);
            if (result?.Succeeded != true)
            {
                throw new Exception("External authentication error");
            }

            // lookup our user and external provider info
            var (user, provider, providerUserId, claims) = FindUserFromExternalProvider(result);
            if (user == null)
            {
                var extuser = AutoProvisionUser(provider, providerUserId, claims);
                var token = _rsa.Encrypt(JsonConvert.SerializeObject(extuser), _rsa.DefaultSecretKey);
                return RedirectToAction("SignupExternal", "Account", new
                {
                    returnUrl = redirectUrl,
                    token = token
                });
            }

            var companyId = _authDbContext.UserProfiles.Where(p => p.UserId == user.User.UserId)
                .Where(p => p.CompanyId.HasValue && p.CompanyId.Value > 0)
                .Select(p => p.CompanyId.Value).FirstOrDefault();

            var dateTimeNow = DateTimeHelper.Now;
            var currentPackage = _authDbContext.CompanyPackages.FirstOrDefault(p => p.CompanyId == companyId && p.ActivedDate <= dateTimeNow && p.ExpiredDate > dateTimeNow);
            if (currentPackage == null)
            {
                _flashMessage.Flash("warning", "Gói dịch vụ của bạn không hợp lệ hoặc đã hết hạn sử dụng, vui lòng liên hệ quản lý để biết thêm chi tiết");
                return Redirect("Login");
            }



            // this allows us to collect any additonal claims or properties
            // for the specific prtotocols used and store them in the local auth cookie.
            // this is typically used to store data needed for signout from those protocols.
            var additionalLocalClaims = new List<Claim>();
            var localSignInProps = new AuthenticationProperties();
            ProcessLoginCallbackForOidc(result, additionalLocalClaims, localSignInProps);

            // issue authentication cookie for user
            await _events.RaiseAsync(new UserLoginSuccessEvent(provider, providerUserId, user.User.SubjectId.ToString(), user.Membership.UserName));
            await HttpContext.SignInAsync(user.User.SubjectId.ToString(), user.Membership.UserName, provider, localSignInProps, additionalLocalClaims.ToArray());

            // delete temporary cookie used during external authentication
            await HttpContext.SignOutAsync(IdentityServer4.IdentityServerConstants.ExternalCookieAuthenticationScheme);

            // validate return URL and redirect back to authorization endpoint or a local page
            var returnUrl = result.Properties.Items["returnUrl"];
            if (_interaction.IsValidReturnUrl(returnUrl) || Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return Redirect("~/");
        }

        /// <summary>
        /// Show logout page
        /// </summary>
        [HttpGet]
        public async Task<IActionResult> Logout(string logoutId)
        {
            // build a model so the logout page knows what to display
            var vm = await BuildLogoutViewModelAsync(logoutId);

            if (vm.ShowLogoutPrompt == false)
            {
                // if the request for logout was properly authenticated from IdentityServer, then
                // we don't need to show the prompt and can just log the user out directly.
                return await Logout(vm);
            }

            return View(vm);
        }

        /// <summary>
        /// Handle logout page postback
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout(LogoutInputModel model)
        {
            // build a model so the logged out page knows what to display
            var vm = await BuildLoggedOutViewModelAsync(model.LogoutId);

            if (User?.Identity.IsAuthenticated == true)
            {
                // delete local authentication cookie
                await HttpContext.SignOutAsync();

                // raise the logout event
                await _events.RaiseAsync(new UserLogoutSuccessEvent(User.GetSubjectId(), User.GetDisplayName()));
            }

            // check if we need to trigger sign-out at an upstream identity provider
            if (vm.TriggerExternalSignout)
            {
                // build a return URL so the upstream provider will redirect back
                // to us after the user has logged out. this allows us to then
                // complete our single sign-out processing.
                string url = Url.Action("Logout", new { logoutId = vm.LogoutId });

                // this triggers a redirect to the external provider for sign-out
                return SignOut(new AuthenticationProperties { RedirectUri = url }, vm.ExternalAuthenticationScheme);
            }

            if (vm.PostLogoutRedirectUri != null)
            {
                return RedirectPermanent(vm.PostLogoutRedirectUri);
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpGet]
        public async Task<IActionResult> Signup(string returnUrl = "")
        {
            var vm = new SignupViewModel { ReturnUrl = returnUrl };

            var vmLogin = await BuildLoginViewModelAsync(returnUrl);
            ViewBag.LoginVM = vmLogin;
            ViewBag.Provinces = this.GetProvines();
            ViewBag.Industries = this.GetIndustries();

            return View(vm);
        }

        [HttpGet]
        public IActionResult SignupExternal(string token, string returnUrl = "")
        {
            var extuser = JsonConvert.DeserializeObject<OAuthUser>(_rsa.Decrypt(token, _rsa.DefaultSecretKey));
            ViewBag.Provinces = this.GetProvines();
            ViewBag.Industries = this.GetIndustries();

            var model = new SignupViewModel
            {
                Email = extuser.Email,
                FullName = extuser.DisplayName,
                Phone = extuser.PhoneNumber,
                ProviderSubjectId = extuser.ProviderUserId,
                ProviderName = extuser.ProviderId,
                Token = token,
                ReturnUrl = returnUrl,
                CompanyName = extuser.DisplayName,
                StoreName = extuser.DisplayName
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Signup(SignupViewModel model)
        {
            if (!string.IsNullOrEmpty(model.ProviderSubjectId))
                ModelState.Remove("Password");




            if (ModelState.IsValid)
            {
                try
                {
                    var result = _authService.CreateNewCompany(model, null);

                    if (result.Succeeded)
                    {
                        //await _events.RaiseAsync(new UserLoginSuccessEvent(user.Username, user.SubjectId, user.Username));
                        await _events.RaiseAsync(new UserLoginSuccessEvent(model.Email, result.UserId.ToString(), model.Email));

                        // issue authentication cookie with subject ID and username
                        //await HttpContext.SignInAsync(user.SubjectId, user.Username, props);
                        await HttpContext.SignInAsync(result.UserId.ToString(), model.Email);

                        // make sure the returnUrl is still valid, and if so redirect back to authorize endpoint or a local page
                        // the IsLocalUrl check is only necessary if you want to support additional local pages, otherwise IsValidReturnUrl is more strict
                        if (_interaction.IsValidReturnUrl(model.ReturnUrl) || Url.IsLocalUrl(model.ReturnUrl))
                        {
                            return Redirect(model.ReturnUrl);
                        }

                        return Redirect("~/");
                    }
                    else
                    {
                        if (result.Errors != null && result.Errors.Any())
                        {
                            foreach (var error in result.Errors)
                            {
                                ModelState.AddModelError("", error);
                            }
                        }
                    }
                }
                catch (System.Exception e)
                {

                    ModelState.AddModelError("", "Không thể tạo tài khoản");
                    throw e;
                }
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignupExternal(SignupViewModel model)
        {
            ModelState.Remove("Password");

            if (ModelState.IsValid)
            {
                try
                {
                    var extuser = JsonConvert.DeserializeObject<OAuthUser>(_rsa.Decrypt(model.Token, _rsa.DefaultSecretKey));
                    var result = _authService.CreateNewCompany(model, extuser);

                    if (result.Succeeded)
                    {
                        //await _events.RaiseAsync(new UserLoginSuccessEvent(user.Username, user.SubjectId, user.Username));
                        await _events.RaiseAsync(new UserLoginSuccessEvent(model.Email, result.UserId.ToString(), model.Email));

                        // issue authentication cookie with subject ID and username
                        //await HttpContext.SignInAsync(user.SubjectId, user.Username, props);
                        await HttpContext.SignInAsync(result.UserId.ToString(), model.Email);

                        // make sure the returnUrl is still valid, and if so redirect back to authorize endpoint or a local page
                        // the IsLocalUrl check is only necessary if you want to support additional local pages, otherwise IsValidReturnUrl is more strict
                        if (_interaction.IsValidReturnUrl(model.ReturnUrl) || Url.IsLocalUrl(model.ReturnUrl))
                        {
                            return Redirect(model.ReturnUrl);
                        }

                        return Redirect("~/");
                    }
                    else
                    {
                        if (result.Errors != null && result.Errors.Count > 0)
                        {
                            foreach (var error in result.Errors)
                            {
                                ModelState.AddModelError("", error);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("", "Đã có lỗi xảy ra trong quá trình thiết lập thông tin");
                        }
                    }
                }
                catch (System.Exception)
                {
                    ModelState.AddModelError("", "Không thể tạo tài khoản");
                }
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ForgotPassword(ForgotPasswordModel model)
        {
            var emailSubject = $"[SUNO.vn] Đặt lại mật khẩu {model.email}";
            TempData["email"] = model.email;
            if (ModelState.IsValid)
            {
                var user = _authService.Users().Where(p => p.UserProfile != null && p.UserProfile.Email == model.email && p.User != null).Select(p => new { p.User.SubjectId, p.UserProfile.UserId }).FirstOrDefault();
                if (user != null)
                {
                    try
                    {
                        _authService.CreateResetToken(user.UserId, out var token);
                        var resetLink = Url.ResetPasswordLink(user.SubjectId.ToString(), token, Request.Scheme);
                        //TODO: creat new Thread task
                        Task.Factory.StartNew(() =>
                        {
                            _emailSender.SendResetPasswordEmail(model.email, emailSubject, resetLink);
                        });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", "Không reset được mật khẩu");
                    }
                }

                //Don't return cannot find Email
                return RedirectToAction(nameof(ForgotPasswordConfirmation));
            }
            else
            {
                ModelState.AddModelError("", "Không thể tạo lại mật khẩu mới");
            }
            return View();
        }

        [HttpGet]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        public IActionResult ResetPassword(string userId, string code)
        {
            if (code == null || userId == null)
            {
                //return BadRequest();
                ModelState.AddModelError(string.Empty, "Mã kiểm tra không hợp lệ");
                return View();
            }

            var model = new ResetPasswordModel
            {
                UserId = userId,
                Code = code
            };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //var user = await _account.FindByUserId(model.UserId);
            var userId = Guid.Parse(model.UserId);
            var user = _authService.Users().Where(p => p.User != null && p.User.SubjectId == userId && p.Membership != null).Select(p => new { p.User.SubjectId, p.UserProfile.UserId, p.Membership.ResetPasswordToken }).FirstOrDefault();
            if (user == null)
            {
                //Dont return cannot find email
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }

            //if (user.ResetPasswordExpired < DateTime.Now)
            //{
            //    ErrorMessage = "Mã kiểm tra đã hết hạn, vui lòng tạo lại mã mới";
            //    return RedirectToAction(nameof(ForgotPassword));
            //}

            if (user.ResetPasswordToken.ToString() != model.Code)
            {
                ModelState.AddModelError(string.Empty, "Mã kiểm tra không hợp lệ");
                return View();
            }

            var result = _authService.ResetPassword(user.UserId, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordModelView model)
        {
            if (ModelState.IsValid)
            {

                if (!_authService.ValidateCredentials(_workContext.UserId, model.OldPassword))
                {
                    ModelState.AddModelError("", "Mật khẩu cũ sai");
                    return View(model);
                }

                if (model.NewPassword != model.RepeatPassword)
                {
                    ModelState.AddModelError("", "Mật khẩu mới không khớp");
                    return View(model);
                }

                _authService.ChangeUserPassword(new ChangePasswordModel
                {
                    UserId = _workContext.UserId,
                    ByUserId = _workContext.UserId,
                    Password = model.NewPassword,
                    TenantId = Guid.Parse(_workContext.UserProfile.TenantId)
                });

                _flashMessage.Flash("success", "Thay đổi mật khẩu thành công");
            }
            return View();
        }

        /*****************************************/
        /* helper APIs for the AccountController */
        /*****************************************/
        private async Task<LoginViewModel> BuildLoginViewModelAsync(string returnUrl)
        {
            var context = await _interaction.GetAuthorizationContextAsync(returnUrl);
            if (context?.IdP != null)
            {
                // this is meant to short circuit the UI and only trigger the one external IdP
                return new LoginViewModel
                {
                    EnableLocalLogin = false,
                    ReturnUrl = returnUrl,
                    Username = context?.LoginHint,
                    ExternalProviders = new ExternalProvider[] { new ExternalProvider { AuthenticationScheme = context.IdP } }
                };
            }

            var schemes = await _schemeProvider.GetAllSchemesAsync();

            var providers = schemes
                .Where(x => x.DisplayName != null ||
                            (x.Name.Equals(AccountOptions.WindowsAuthenticationSchemeName, StringComparison.OrdinalIgnoreCase))
                )
                .Select(x => new ExternalProvider
                {
                    DisplayName = x.DisplayName,
                    AuthenticationScheme = x.Name
                }).ToList();

            var allowLocal = true;
            if (context?.ClientId != null)
            {
                var client = await _clientStore.FindEnabledClientByIdAsync(context.ClientId);
                if (client != null)
                {
                    allowLocal = client.EnableLocalLogin;

                    if (client.IdentityProviderRestrictions != null && client.IdentityProviderRestrictions.Any())
                    {
                        providers = providers.Where(provider => client.IdentityProviderRestrictions.Contains(provider.AuthenticationScheme)).ToList();
                    }
                }
            }

            return new LoginViewModel
            {
                AllowRememberLogin = AccountOptions.AllowRememberLogin,
                EnableLocalLogin = allowLocal && AccountOptions.AllowLocalLogin,
                ReturnUrl = returnUrl,
                Username = context?.LoginHint,
                ExternalProviders = providers.ToArray()
            };
        }

        private async Task<LoginViewModel> BuildLoginViewModelAsync(LoginInputModel model)
        {
            var vm = await BuildLoginViewModelAsync(model.ReturnUrl);
            vm.Username = model.Username;
            vm.RememberLogin = model.RememberLogin;
            return vm;
        }

        private async Task<LogoutViewModel> BuildLogoutViewModelAsync(string logoutId)
        {
            var vm = new LogoutViewModel { LogoutId = logoutId, ShowLogoutPrompt = AccountOptions.ShowLogoutPrompt };

            if (User?.Identity.IsAuthenticated != true)
            {
                // if the user is not authenticated, then just show logged out page
                vm.ShowLogoutPrompt = false;
                return vm;
            }

            var context = await _interaction.GetLogoutContextAsync(logoutId);
            if (context?.ShowSignoutPrompt == false)
            {
                // it's safe to automatically sign-out
                vm.ShowLogoutPrompt = false;
                return vm;
            }

            // show the logout prompt. this prevents attacks where the user
            // is automatically signed out by another malicious web page.
            return vm;
        }

        private async Task<LoggedOutViewModel> BuildLoggedOutViewModelAsync(string logoutId)
        {
            // get context information (client name, post logout redirect URI and iframe for federated signout)
            var logout = await _interaction.GetLogoutContextAsync(logoutId);

            var vm = new LoggedOutViewModel
            {
                AutomaticRedirectAfterSignOut = AccountOptions.AutomaticRedirectAfterSignOut,
                PostLogoutRedirectUri = logout?.PostLogoutRedirectUri,
                ClientName = string.IsNullOrEmpty(logout?.ClientName) ? logout?.ClientId : logout?.ClientName,
                SignOutIframeUrl = logout?.SignOutIFrameUrl,
                LogoutId = logoutId
            };

            if (User?.Identity.IsAuthenticated == true)
            {
                var idp = User.FindFirst(JwtClaimTypes.IdentityProvider)?.Value;
                if (idp != null && idp != IdentityServer4.IdentityServerConstants.LocalIdentityProvider)
                {
                    var providerSupportsSignout = await HttpContext.GetSchemeSupportsSignOutAsync(idp);
                    if (providerSupportsSignout)
                    {
                        if (vm.LogoutId == null)
                        {
                            // if there's no current logout context, we need to create one
                            // this captures necessary info from the current logged in user
                            // before we signout and redirect away to the external IdP for signout
                            vm.LogoutId = await _interaction.CreateLogoutContextAsync();
                        }

                        vm.ExternalAuthenticationScheme = idp;
                    }
                }
            }

            return vm;
        }

        private (UserModel user, string provider, string providerUserId, IEnumerable<Claim> claims) FindUserFromExternalProvider(AuthenticateResult result)
        {
            var externalUser = result.Principal;

            // try to determine the unique id of the external user (issued by the provider)
            // the most common claim type for that are the sub claim and the NameIdentifier
            // depending on the external provider, some other claim type might be used
            var userIdClaim = externalUser.FindFirst(JwtClaimTypes.Subject) ??
                              externalUser.FindFirst(ClaimTypes.NameIdentifier) ??
                              throw new Exception("Unknown userid");

            // remove the user id claim so we don't include it as an extra claim if/when we provision the user
            var claims = externalUser.Claims.ToList();
            claims.Remove(userIdClaim);

            var provider = result.Properties.Items["scheme"];
            var providerUserId = userIdClaim.Value;

            // find external user
            //var user = _account.FindByExternalProvider(provider, providerUserId);


            var userId = _authDbContext.OAuthMemberships.Where(p => p.Provider == provider && p.ProviderUserId == providerUserId)
                .Select(p => p.UserId).FirstOrDefault();

            var user = _authService.Users().FirstOrDefault(p => p.User.UserId == userId);
            return (user, provider, providerUserId, claims);
        }



        private OAuthUser AutoProvisionUser(string provider, string providerUserId, IEnumerable<Claim> claims)
        {
            // create a list of claims that we want to transfer into our store
            var filtered = new List<Claim>();

            foreach (var claim in claims)
            {
                // if the external system sends a display name - translate that to the standard OIDC name claim
                if (claim.Type == ClaimTypes.Name)
                {
                    filtered.Add(new Claim(JwtClaimTypes.Name, claim.Value));
                }
                // if the JWT handler has an outbound mapping to an OIDC claim use that
                else if (JwtSecurityTokenHandler.DefaultOutboundClaimTypeMap.ContainsKey(claim.Type))
                {
                    filtered.Add(new Claim(JwtSecurityTokenHandler.DefaultOutboundClaimTypeMap[claim.Type], claim.Value));
                }
                // copy the claim as-is
                else
                {
                    filtered.Add(claim);
                }
            }

            // if no display name was provided, try to construct by first and/or last name
            if (filtered.All(x => x.Type != JwtClaimTypes.Name))
            {
                var first = filtered.FirstOrDefault(x => x.Type == JwtClaimTypes.GivenName)?.Value;
                var last = filtered.FirstOrDefault(x => x.Type == JwtClaimTypes.FamilyName)?.Value;
                if (first != null && last != null)
                {
                    filtered.Add(new Claim(JwtClaimTypes.Name, first + " " + last));
                }
                else if (first != null)
                {
                    filtered.Add(new Claim(JwtClaimTypes.Name, first));
                }
                else if (last != null)
                {
                    filtered.Add(new Claim(JwtClaimTypes.Name, last));
                }
            }

            // create a new unique subject id
            var sub = Guid.NewGuid();
            // check if a display name is available, otherwise fallback to subject id
            var name = filtered.FirstOrDefault(c => c.Type == JwtClaimTypes.Name)?.Value ?? sub.ToString();
            var email = filtered.FirstOrDefault(c => c.Type == JwtClaimTypes.Email)?.Value ?? string.Empty;
            var phone = filtered.FirstOrDefault(c => c.Type == JwtClaimTypes.PhoneNumber)?.Value ?? string.Empty;
            var birthday = filtered.FirstOrDefault(c => c.Type == JwtClaimTypes.BirthDate)?.Value ?? string.Empty;
            var gender = filtered.FirstOrDefault(c => c.Type == JwtClaimTypes.Gender)?.Value ?? string.Empty;
            var picture = filtered.FirstOrDefault(c => c.Type == JwtClaimTypes.Picture)?.Value ?? string.Empty;

            if (string.IsNullOrEmpty(email) && provider.ToLower() == "zalo")
                email = $"{providerUserId}@zalo.me";

            if (string.IsNullOrEmpty(email) && provider.ToLower() == "facebook")
                email = $"{providerUserId}@facebook.com";



            return new OAuthUser
            {
                UserName = providerUserId,
                Email = email,
                PhoneNumber = phone,
                DisplayName = name,
                Avatar = picture,
                Gender = gender,
                Birthday = birthday,
                ProviderId = provider,
                ProviderUserId = providerUserId
            };
        }

        private void ProcessLoginCallbackForOidc(AuthenticateResult externalResult, List<Claim> localClaims, AuthenticationProperties localSignInProps)
        {
            // if the external system sent a session id claim, copy it over
            // so we can use it for single sign-out
            var sid = externalResult.Principal.Claims.FirstOrDefault(x => x.Type == JwtClaimTypes.SessionId);
            if (sid != null)
            {
                localClaims.Add(new Claim(JwtClaimTypes.SessionId, sid.Value));
            }

            // if the external provider issued an id_token, we'll keep it for signout
            var id_token = externalResult.Properties.GetTokenValue("id_token");
            if (id_token != null)
            {
                localSignInProps.StoreTokens(new[] { new AuthenticationToken { Name = "id_token", Value = id_token } });
            }
        }


        private List<SelectListItem> GetIndustries()
        {
            return _authDbContext.Industries.Select(p => new SelectListItem { Text = p.Name, Value = p.Id.ToString() }).ToList();
        }

        private List<SelectListItem> GetProvines()
        {
            return _authDbContext.Provinces.Select(p => new SelectListItem { Text = p.Name, Value = p.ProvinceId.ToString() }).ToList();
        }
    }
}