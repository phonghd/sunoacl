﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using Suno.Acl.Data;
using Suno.Acl.Infrastructure;
using Suno.Acl.Models.UserInfo;
using Suno.Acl.Models.Users;
using Suno.Acl.Services;
using Suno.Core.Acl.Domain.V2;
using Suno.Core.Hash;
using UserProfile = Suno.Core.Acl.Domain.V1.UserProfile;
using System.IO;

namespace Suno.Acl.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class UsersController : Controller
    {
        private readonly ApiWorkContext _workContext;
        private readonly SettingService _settingService;
        private readonly AuthDbContext _authDbContext;
        private readonly AuthService _authService;
        private readonly SaltedHash _saltedHash;

        public UsersController(ApiWorkContext workContext, SettingService settingService, AuthDbContext authDbContext, AuthService authService, SaltedHash saltedHash)
        {
            _workContext = workContext;
            _settingService = settingService;
            _authDbContext = authDbContext;
            _authService = authService;
            _saltedHash = saltedHash;
        }

        [HttpGet("userinfo")]
        public IActionResult GetUserInfo()
        {
            var userProfile = _authDbContext.UserProfiles.Where(p => p.UserId == _workContext.MembershipId).Select(p => new { p.IsAdmin }).FirstOrDefault();

            var viewedWelcome = false;
            var viewedSalesTour = false;
            var viewedOverviewTour = false;
            if (userProfile != null)
            {
                viewedWelcome = _settingService.GetValue(_workContext.UserId, _workContext.TenantId, SettingKeys.ViewedWelcome, false);
                viewedSalesTour = _settingService.GetValue(_workContext.UserId, _workContext.TenantId, SettingKeys.ViewedSalesTour, false);
                viewedOverviewTour = _settingService.GetValue(_workContext.UserId, _workContext.TenantId, SettingKeys.ViewedOverviewTour, false);
            }

            return Ok(new
            {
                userProfile.IsAdmin,
                viewedOverviewTour,
                viewedWelcome,
                viewedSalesTour
            });
        }

        [HttpPut("userseting/view")]
        public IActionResult SaveUserViewSetting(string key)
        {
            if (key == SettingKeys.ViewedOverviewTour || key == SettingKeys.ViewedSalesTour || key == SettingKeys.ViewedWelcome)
            {
                Console.WriteLine("Save User View Setting:::" + _workContext.UserId.ToString() + "::" + _workContext.TenantId.ToString() + "::" + _workContext.MembershipId);

                _settingService.PutValue(_workContext.UserId, _workContext.TenantId, key, true);
                return Ok();
            }
            return BadRequest();

        }

        [HttpPut("userseting")]
        public IActionResult SaveUserSetting(string key, string value)
        {
            var ignoreKeys = new List<string> { SettingKeys.ViewedOverviewTour, SettingKeys.ViewedSalesTour, SettingKeys.ViewedWelcome };
            if (ignoreKeys.Contains(key))
                return BadRequest();

            _settingService.PutValue(_workContext.UserId, _workContext.TenantId, key, value);
            return Ok();
        }


        [HttpPut("userprofile")]
        public IActionResult SaveUserProfile([FromBody] SaveUserProfileRequest request)
        {
            var userProfile = new UserProfile { UserId = _workContext.MembershipId };
            _authDbContext.UserProfiles.Attach(userProfile);
            if (!string.IsNullOrEmpty(request.DisplayName))
                userProfile.DisplayName = request.DisplayName;
            if (!string.IsNullOrEmpty(request.PhoneNumber))
                userProfile.PhoneNumber = request.PhoneNumber;

            if (!string.IsNullOrEmpty(request.Avatar))
            {
                var userClaimAvatar = _authDbContext.UserClaims.FirstOrDefault(p => p.UserSubjectId == _workContext.UserId && p.ClaimType == JwtClaimTypes.Picture);
                if (userClaimAvatar == null)
                {
                    userClaimAvatar = new UserClaim { ClaimType = JwtClaimTypes.Picture, UserSubjectId = _workContext.UserId, ClaimValue = request.Avatar };
                    _authDbContext.UserClaims.Add(userClaimAvatar);
                }
                else
                    userClaimAvatar.ClaimValue = request.Avatar;
            }

            _authDbContext.SaveChanges();
            return Ok();
        }

        [HttpPut("change-password")]
        public IActionResult ChangePassword([FromBody] ChangePasswordRequest request)
        {
            var existUser = _authDbContext.Memberships.Where(p => p.UserId == _workContext.MembershipId)
                .Select(p => new
                {
                    p.Password,
                    p.PasswordSalt
                }).FirstOrDefault();

            if (existUser == null || !_saltedHash.VerifyHashString(request.OldPassword, existUser.Password, existUser.PasswordSalt))
            {
                return BadRequest(new List<string>
                {
                    "Mật khẩu cũ của bạn không đúng"
                });
            }

            _authService.ChangeUserPassword(new ChangePasswordModel
            {
                UserId = _workContext.UserId,
                ByUserId = _workContext.UserId,
                Password = request.NewPassword,
                TenantId = _workContext.TenantId
            });
            return Ok();
        }

    }
}