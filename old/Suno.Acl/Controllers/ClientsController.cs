﻿using System.Linq;
using System.Threading.Tasks;
using Core.Flash;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ServiceStack;
using Suno.Acl.Data;
using Suno.Acl.Models.Clients;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Acl.Controllers
{
    [Authorize]
    public class ClientsController : Controller
    {
        private readonly AuthDbContext _authDbContext;
        private readonly IFlasher _flashMessage;

        public ClientsController(AuthDbContext authDbContext, IFlasher flashMessage)
        {
            _authDbContext = authDbContext;
            _flashMessage = flashMessage;
        }

        // GET: Clients
        public async Task<IActionResult> Index()
        {
            var clients = await _authDbContext.Clients.ToListAsync();
            return View(clients);
        }

        public async Task<IActionResult> Compare(int first, int second)
        {
            var clientIds = new int[] { first, second };
            var clients = await _authDbContext.Clients.Where(p => clientIds.Contains(p.Id)).Include(p => p.ClientGrantTypes).Include(p => p.ClientScopes).AsNoTracking().ToListAsync();
            var firstClient = clients.FirstOrDefault(p => p.Id == first);
            var secondClient = clients.FirstOrDefault(p => p.Id == second);


            var viewModel = new CompareViewModel
            {
                FirstClient = firstClient,
                SecondClient = secondClient
            };
            return View(viewModel);
        }

        // GET: Clients/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Clients/Details/5
        public async Task<ActionResult> Clone(int id)
        {
            var client = await _authDbContext.Clients.Where(p => p.Id == id).Include(p => p.ClientGrantTypes).Include(p => p.ClientScopes).AsNoTracking().FirstOrDefaultAsync();

            if (client != null)
            {
                var clonedClient = client.CreateCopy();
                clonedClient.Id = 0;
                clonedClient.ClientName += " (Cloned)";
                clonedClient.ClientId += "_cloned";

                clonedClient.ClientGrantTypes.ForEach(p =>
                {
                    p.ClientId = 0;
                    p.Id = 0;
                });
                clonedClient.ClientScopes.ForEach(p =>
                {
                    p.ClientId = 0;
                    p.Id = 0;
                });

                _authDbContext.Clients.Add(clonedClient);
                await _authDbContext.SaveChangesAsync();
                _flashMessage.Flash("success", "The client has been cloned successfully");
            }
            return RedirectToAction("Index");
        }

        // GET: Clients/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            var client = await _authDbContext.Clients.Where(p => p.Id == id).AsNoTracking().FirstOrDefaultAsync();
            //var clientScopes = await _dbContext.ClientScopes.Where(p => p.ClientId == id).AsNoTracking().ToListAsync();

            //var viewModel = client.ConvertTo<ClientEditViewModel>();
            //viewModel.ClientScopes = clientScopes;
            return View(client);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ClientId,ClientSecret,ClientName,AccessTokenLifetime,AccessTokenType,AllowAccessTokensViaBrowser,AllowOfflineAccess,ClientUri,Description,IdentityTokenLifetime,RequireConsent,Enabled,RedirectUris,PostLogoutRedirectUris")] Client clients)
        {
            if (id != clients.Id)
            {
                _flashMessage.Flash("error", "Không tìm thấy thông tin");
                return RedirectToAction("Index");
            }

            if (ModelState.IsValid)
            {
                try
                {

                    _authDbContext.Update(clients.ConvertTo<Client>());
                    await _authDbContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    //if (!ClientsExists(clients.Id))
                    //{
                    //    return NotFound();
                    //}
                    //else
                    //{
                    throw;
                    //}

                }
                return RedirectToAction(nameof(Index));
            }
            return View(clients);
        }

        // GET: Clients/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Clients/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int id, IFormCollection collection)
        {
            try
            {
                if (await _authDbContext.Clients.CountAsync() == 1)
                {
                    _flashMessage.Flash("warning", "Please keep at least one client");
                    return RedirectToAction(nameof(Index));
                }

                var client = await _authDbContext.Clients.Where(p => p.Id == id).FirstOrDefaultAsync();
                //var clientScopes = await _dbContext.ClientScopes.Where(p => p.ClientId == id).ToListAsync();
                //var clientGrantTypes = await _dbContext.ClientGrantTypes.Where(p => p.ClientId == id).ToListAsync();

                //if (clientGrantTypes.Any())
                //    _dbContext.ClientGrantTypes.RemoveRange(clientGrantTypes);

                //if (clientScopes.Any())
                //    _dbContext.ClientScopes.RemoveRange(clientScopes);

                if (client != null)
                    _authDbContext.Clients.Remove(client);

                await _authDbContext.SaveChangesAsync();

                _flashMessage.Flash("success", "The client has been deleted successfully");

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}