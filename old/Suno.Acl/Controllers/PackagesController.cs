﻿using System;
using System.Linq;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Suno.Acl.Data;
using Suno.Acl.Helpers;
using Suno.Acl.Infrastructure;
using Suno.Acl.Services;
using Suno.Acl.Services.Models;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Acl.Controllers
{
    [Produces("application/json")]
    [Route("api/Packages")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    public class PackagesController : BaseApiController
    {
        private readonly AuthDbContext _authDbContext;
        private readonly ApiWorkContext _workContext;
        private readonly PackagePricingService _packagePricingService;
        private readonly CommonHelper _commonHelper;

        public PackagesController(AuthDbContext authDbContext, ApiWorkContext workContext, PackagePricingService packagePricingService, CommonHelper commonHelper)
        {
            _authDbContext = authDbContext;
            _workContext = workContext;
            _packagePricingService = packagePricingService;
            _commonHelper = commonHelper;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var packages = await _authDbContext.CompanyPackages.Where(p => p.CompanyId == _workContext.CompanyId)
                .Include(p => p.Package).Select(p => new
                {
                    p.Package.Name,
                    p.ActivedDate,
                    p.ExpiredDate,
                    p.Amount,
                    p.PayStatus,
                    InUse = p.ActivedDate <= DateTimeHelper.Now && p.ExpiredDate > DateTimeHelper.Now
                }).ToListAsync();
            return Ok(packages);
        }


        [HttpGet("pricing")]
        public IActionResult GetPricing()
        {
            return Ok(_packagePricingService.PricingModels);
        }

        [HttpPost("calculate-pos-subscription-price")]
        public IActionResult CalculatePosSubscriptionPrice([FromBody] CreatePosSubscriptionRequest request)
        {
            if (request == null)
            {
                ErrorMessage.Errors.Add("Vui lòng nhập đủ thông tin");
                return BadRequest(ErrorMessage);
            }

            var price = _packagePricingService.CalculatePosSubscriptionPrice(request);
            return Ok(price);
        }

        [HttpPost("pos-subscription")]
        public async Task<IActionResult> CreatePosSubscription([FromBody]CreatePosSubscriptionRequest request)
        {
            var calculatedResult = _packagePricingService.CalculatePosSubscriptionPrice(request);

            var invoice = new Invoice
            {
                CompanyId = _workContext.CompanyId,
                CreatedBy = _workContext.MembershipId,
                DueDate = DateTime.Now,
                InvoiceDate = DateTime.Now,
                IsPaid = false,
                InvoiceAmount = calculatedResult.TotalAmount

            };


            using (var dbContextTransaction = _authDbContext.Database.BeginTransaction())
            {
                try
                {
                    _authDbContext.Invoices.Add(invoice);
                    await _authDbContext.SaveChangesAsync();

                    invoice.InvoiceNumber = _commonHelper.GenerateCode(invoice.Id, 6, "HD");

                    _authDbContext.SubscriptionInvoiceDetails.Add(new SubscriptionInvoiceDetail
                    {
                        CompanyId = _workContext.CompanyId,
                        ActivedDate = calculatedResult.ActivedDate,
                        Description = "Pos Subscription",
                        DiscountAmount = 0,
                        ExpiredDate = calculatedResult.ExpiredDate,
                        ExtendedAmount = 0,
                        InvoiceId = invoice.Id,
                        IsApplied = false,
                        PackageId = request.PackageId,
                        ItemCode = "#" + request.PackageId.ToString(),
                        Price = calculatedResult.TotalAmount
                    });

                    await _authDbContext.SaveChangesAsync();
                    dbContextTransaction.Commit();

                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    throw;
                }
            }




            return Ok();
        }
    }
}