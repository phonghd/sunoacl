﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Suno.Acl.Controllers
{
    public class BaseApiController : ControllerBase
    {
        public ErrorMessage ErrorMessage { get; set; }

        public BaseApiController()
        {
            ErrorMessage = new ErrorMessage();
        }
    }


    public class ErrorMessage
    {
        public List<string> Errors { get; set; }

        public ErrorMessage()
        {
            Errors = new List<string>();
        }
    }
}