﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IdentityServer4.Stores;
using Microsoft.IdentityModel.Tokens;

namespace Suno.Acl.Stores
{
    public class SigningCredentialStore : ISigningCredentialStore
    {

        public Task<SigningCredentials> GetSigningCredentialsAsync()
        {
            var certificateFilePath = "~/public_privatekey.pfx";
            var certificatePassword = "p@ssw0rd";
            
            var certificate = new X509Certificate2(certificateFilePath, certificatePassword);
        
            if (certificate == null) throw new ArgumentNullException(nameof(certificate));
        
            if (!certificate.HasPrivateKey)
            {
                throw new InvalidOperationException("X509 certificate does not have a private key.");
            }
        
            var credential = new SigningCredentials(new X509SecurityKey(certificate), "SHA256WithRSA");            
            
            return Task.FromResult(credential);
        }
    }
}
