﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using Microsoft.EntityFrameworkCore;
using Suno.Acl.Data;

namespace Suno.Acl.Infrastructure
{
    public class ClientStore : IClientStore
    {
        private readonly AuthDbContext _authDbContext;

        public ClientStore(AuthDbContext authDbContext)
        {
            _authDbContext = authDbContext;
        }

        public async Task<Client> FindClientByIdAsync(string clientId)
        {

            if (clientId == "client_website")
            {
                var clientCredential = new Client
                {
                    ClientId = "client_website",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "productCatalog", "MasterData", "inventory", "pricing", "cashflow", "sales", "purchase", "crm", "Report" }
                };
                return clientCredential;
            }

            if (clientId == "suno_chrome_ext")
            {
                return new Client
                {
                    ClientId = "suno_chrome_ext",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets = {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "productCatalog", "MasterData", "inventory", "pricing", "cashflow", "sales", "purchase", "crm", "Report" }
                };

            }

            var clientEntity = await _authDbContext.Clients.Where(p => p.ClientId == clientId).Include(p => p.ClientGrantTypes).Include(p => p.ClientScopes).AsNoTracking().FirstOrDefaultAsync();
            if (clientEntity == null)
                return null;

            var client = new Client
            {
                ClientId = clientEntity.ClientId,
                ClientSecrets = new List<Secret> { new Secret(clientEntity.ClientSecret.Sha256()) },
                ClientName = clientEntity.ClientName,
                ClientUri = clientEntity.ClientUri,
                AccessTokenLifetime = clientEntity.AccessTokenLifetime,
                IdentityTokenLifetime = clientEntity.IdentityTokenLifetime,
                AllowAccessTokensViaBrowser = clientEntity.AllowAccessTokensViaBrowser,
                AllowOfflineAccess = clientEntity.AllowOfflineAccess,
                Enabled = clientEntity.Enabled,
                RequireConsent = clientEntity.RequireConsent,
                AllowedGrantTypes = clientEntity.ClientGrantTypes.Select(g => g.GrantType).ToList(),
                AllowedScopes = clientEntity.ClientScopes.Select(s => s.Scope).ToList(),
                RedirectUris = buildRediectUris(clientEntity.RedirectUris),
                PostLogoutRedirectUris = buildRediectUris(clientEntity.PostLogoutRedirectUris),
            };

            //(new Client()).AllowedGrantTypes = new List<string> { GrantTypes.ClientCredentials };

            return client;

        }


        //Simple Multiple redirect Uris by glue it with commas in storage
        private List<string> buildRediectUris(string OriginalUri)
        {
            string[] uriArraySplit = OriginalUri.Split(',');
            var listUris = new List<string>();
            foreach (var uri in uriArraySplit)
            {
                //trim '/' end of string for consistence url   
                listUris.Add(uri.TrimEnd('/'));
            }

            return listUris;
        }
    }
}
