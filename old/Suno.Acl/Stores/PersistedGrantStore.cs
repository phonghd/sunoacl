﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IdentityServer4.Models;
using IdentityServer4.Stores;
using ServiceStack.Redis;

namespace Suno.Acl.Infrastructure
{
    public class PersistedGrantStore: IPersistedGrantStore
    {
        private readonly IRedisClientsManager _clientsManager; 
        
        public PersistedGrantStore(IRedisClientsManager clientManager)
        {
            _clientsManager = clientManager;
        }
        
        public Task StoreAsync(PersistedGrant grant)
        {
//            var testPeristence = "OOOOO";
            using (var redis = _clientsManager.GetClient())
            {
                var existed = redis.Get<PersistedGrant>(grant.Key);

                if (existed == null)
                {
                    
                }
            }

            return Task.FromResult(0);
//            throw new System.NotImplementedException();
        }

        public Task<PersistedGrant> GetAsync(string key)
        {
            using (var redis = _clientsManager.GetClient())
            {
                var result = redis.Get<PersistedGrant>(key);
                return Task.FromResult(result);
            }            
        }

        public Task<IEnumerable<PersistedGrant>> GetAllAsync(string subjectId)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveAsync(string key)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveAllAsync(string subjectId, string clientId)
        {
            throw new System.NotImplementedException();
        }

        public Task RemoveAllAsync(string subjectId, string clientId, string type)
        {
            throw new System.NotImplementedException();
        }
    }
}