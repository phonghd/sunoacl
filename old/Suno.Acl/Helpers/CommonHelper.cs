﻿using System.Linq;
using Suno.Acl.Infrastructure;

namespace Suno.Acl.Helpers
{
    public class CommonHelper
    {
        private readonly AppSettings _appSetting;

        public CommonHelper(AppSettings appSetting)
        {
            _appSetting = appSetting;
        }

        public string ImagePath(string path)
        {
            if (string.IsNullOrEmpty(path))
                return path;
            if (!path.StartsWith("http") && !path.StartsWith("/"))
                path = _appSetting.AppsCdnUrl + "/" + path;

            return path;
        }

        public string GenerateCode(int number, int length, string prefix)
        {
            var result = prefix.ToUpper() +
                         string.Join("", Enumerable.Range(0, length - number.ToString().Length > 0 ?
                             length - number.ToString().Length : 0
                             ).Select(p => "0")) +
                         number;

            return result;
        }

    }
}