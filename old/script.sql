WITH userClaims (userId, displayname, userEmail, userPhonenumber, userisAdmin, isAdmin)
AS 
(
SELECT u.UserId, p.DisplayName, u.Email as userEmail, u.PhoneNumber as userPhonenumber, u.isAdmin as userisadmin, p.IsAdmin
FROM [user] u
INNER JOIN UserProfile p on u.UserId = p.UserId
)
--Update userClaims set userEmail = Email, userPhonenumber = PhoneNumber, userisAdmin = isAdmin
-- update userClaims set userisAdmin = isAdmin
select * from UserClaims
GO

alter TABLE UserProfile
drop COLUMN deletedDate, deletednote, enabled, Email, PhoneNumber, isDeleted, isAdmin, deletedUser
GO

-- alter table [user]
-- add isAdmin bit not NULL default 0

SELECT * FROM [User]
-- SELECT u.UserId, p.DisplayName, u.Email as userEmail, u.PhoneNumber as userPhonenumber, p.PhoneNumber
-- ,p.Email
-- FROM [user] u
-- INNER JOIN UserProfile p on u.UserId = p.UserId