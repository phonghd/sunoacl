﻿using System;
using System.Collections.Generic;
using System.Text;
using Suno.Core.Acl.Domain.V1;
using Suno.Core.Acl.Domain.V2;

namespace Suno.Core.Acl.Domain
{
    public static class StaticData
    {
        public static Version Version = new Version(2, 0, 0, 0);
        public static App PosV2 = new App
        {
            Code = "SunoPosV2",
            Name = "Suno Pos",
            Version = Version.ToString()
        };
    }
}
