﻿namespace Suno.Core.Acl.Domain.V2
{
    public abstract class InvoiceDetail
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal ExtendedAmount { get; set; }
        public bool IsApplied { get; set; }

        public Invoice Invoice { get; set; }
    }
}