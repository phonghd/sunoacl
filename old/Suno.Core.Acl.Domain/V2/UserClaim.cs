﻿using System;

namespace Suno.Core.Acl.Domain.V2
{
    public class UserClaim
    {
        public int Id { get; set; }
        public Guid UserSubjectId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }

        public User User { get; set; }
    }
}