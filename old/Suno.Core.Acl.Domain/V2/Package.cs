﻿using Suno.Core.Acl.Domain.V2;

namespace Suno.Core.Acl.Domain.V1
{
    public partial class Package
    {
        public int? AppId { get; set; }
        public App App { get; set; }
        public bool IsDefault { get; set; }
    }


    public partial class PackageFeatureLimit
    {
        public int? MaxCustomer { get; set; }
    }


    public partial class Feature
    {
        public int? AppId { get; set; }
        public string FeatureKey { get; set; }
        public App App { get; set; }
    }
}