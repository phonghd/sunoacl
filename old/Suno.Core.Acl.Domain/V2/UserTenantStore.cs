﻿using System;

namespace Suno.Core.Acl.Domain.V2
{
    public class UserTenantStore
    {
        public Guid UserSubjectId { get; set; }
        public Guid StoreId { get; set; }
        public Guid TenantId { get; set; }

        public User User { get; set; }
        public Store Store { get; set; }
        public Tenant Tenant { get; set; }
    }
}