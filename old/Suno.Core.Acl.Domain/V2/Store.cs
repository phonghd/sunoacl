﻿using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    public class Store
    {
        public Guid StoreId { get; set; }
        public string StoreName { get; set; }
        public string StoreAddress { get; set; }
        public string StorePhone { get; set; }
        public string TaxCode { get; set; }
        public string StoreCode { get; set; }
        public Guid TenantId { get; set; }
        public Guid CreatedUser { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? UpdatedUser { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public Guid? DeletedUser { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool IsDeleted { get; set; }

        public List<UserTenantStore> UserTenantStores { get; set; }
    }
}