﻿using System;
using System.Collections.Generic;
using Suno.Core.Acl.Domain.V1;

namespace Suno.Core.Acl.Domain.V2
{
    public class User
    {
        public Guid SubjectId { get; set; }
        public int UserId { get; set; }
        public int? AppId { get; set; }
        public App App { get; set; }

        public List<UserClaim> UserClaims { get; set; }
        public List<UserTenantStore> UserTenantStores { get; set; }
        public List<UserTenant> UserTenants { get; set; }
        public UserProfile UserProfile { get; set; }
        public List<UserTenantRoleV2> UserTenantRoles { get; set; }
        public List<UserSetting> UserSettings { get; set; }
    }
}