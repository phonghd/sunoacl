﻿using System;

namespace Suno.Core.Acl.Domain.V2
{
    public class UserTenant
    {
        public Guid UserSubjectId { get; set; }
        public Guid TenantId { get; set; }
        public bool IsDefault { get; set; }


        public Tenant Tenant { get; set; }
        public User User { get; set; }
    }
}