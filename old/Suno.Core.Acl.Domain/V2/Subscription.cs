﻿using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V2
{
    //public class Subscription : ISubscription
    //{
    //    public int Id { get; set; }

    //    public string Name { get; set; }
    //    public string Note { get; set; }
    //    public Guid TenantId { get; set; }
    //    public Tenant Tenant { get; set; }
    //    public Guid OwnerId { get; set; }
    //    public User Owner { get; set; }
    //    public DateTime ExpiredDate { get; set; }
    //    public int? MaxEmailPerMonth { get; set; }
    //    public int? MaxUser { get; set; }
    //    public int? MaxStore { get; set; }
    //    public int? MaxSaleOrderPerDay { get; set; }
    //    public int? MaxProduct { get; set; }
    //    public int? MaxProductItem { get; set; }
    //}


    public class App
    {
        public int AppId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }

        public List<User> Users { get; set; }

        public List<V1.Package> Packages { get; set; }
        public List<V1.Feature> Features { get; set; }
    }

    //public class PackageV2
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public int AppId { get; set; }
    //    public App App { get; set; }
    //    public List<FeatureV2> Features { get; set; }
    //}

    //public class FeatureV2
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public int PackageId { get; set; }
    //    public PackageV2 Package { get; set; }
    //}
}