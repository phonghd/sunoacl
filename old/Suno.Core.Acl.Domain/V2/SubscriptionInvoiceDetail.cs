﻿using System;

namespace Suno.Core.Acl.Domain.V2
{
    public class SubscriptionInvoiceDetail  : InvoiceDetail
    {
        public int CompanyId { get; set; }
        public int PackageId { get; set; }
        public DateTime ActivedDate { get; set; }
        public DateTime ExpiredDate { get; set; }

        //public int? MaxEmailPerMonth { get; set; } //(int, null)
        //public int? MaxUser { get; set; } //(int, null)
        //public int? MaxStore { get; set; } //(int, null)
        //public int? MaxSaleOrderPerDay { get; set; } //(int, null)
        //public int? MaxProduct { get; set; } //(int, null)
        //public int? MaxProductItem { get; set; } //(int, null)
    }
}