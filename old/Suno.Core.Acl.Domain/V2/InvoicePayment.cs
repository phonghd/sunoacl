﻿using System;

namespace Suno.Core.Acl.Domain.V2
{
    public class InvoicePayment
    {
        public int Id { get; set; }
        public int InvoiceId { get; set; }
        public string Type { get; set; }
        public string Reference { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }

        public Invoice Invoice { get; set; }
    }
}