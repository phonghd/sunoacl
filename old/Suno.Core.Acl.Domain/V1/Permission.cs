using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V1
{
    public class Permission
    {
        public int PermissionID { get; set; } //(int, not null)
        public string PermissionCode { get; set; } //(nvarchar(511), not null)
        public string Description { get; set; } //(nvarchar(2000), null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int? CreatedUser { get; set; } //(int, null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? FeatureId { get; set; } //(int, null)


        public Feature Feature { get; set; }
        public List<RolePermission> RolePermissions { get; set; }
    }
}