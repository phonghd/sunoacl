namespace Suno.Core.Acl.Domain.V1
{
    public class Province
    {
        public int ProvinceId { get; set; } //(int, not null)
        public int CountryId { get; set; } //(int, not null)
        public string Code { get; set; } //(nvarchar(10), null)
        public int? PhoneCode { get; set; } //(int, null)
        public string Name { get; set; } //(nvarchar(100), not null)
        public bool? IsDeleted { get; set; } //(bit, null)
    }
}