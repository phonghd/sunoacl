namespace Suno.Core.Acl.Domain.V1
{
    public class MailQueueHistoryErrorLog
    {
        public long Id { get; set; } //(bigint, not null)
        public string ErrorLogs { get; set; } //(nvarchar(max), not null)
        public long HistoryId { get; set; } //(bigint, not null)
        public long QueueId { get; set; } //(bigint, not null)

    }
}