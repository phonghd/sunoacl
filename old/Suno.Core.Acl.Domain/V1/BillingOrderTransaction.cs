using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class BillingOrderTransaction
    {
        public int Id { get; set; } //(int, not null)
        public string OrderCode { get; set; } //(nvarchar(20), null)
        public string Fullname { get; set; } //(nvarchar(50), not null)
        public string Phone { get; set; } //(nvarchar(50), not null)
        public string Email { get; set; } //(nvarchar(100), null)
        public decimal Amount { get; set; } //(money, not null)
        public string ProviderCode { get; set; } //(nvarchar(10), null)
        public byte Status { get; set; } //(tinyint, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CreatedUser { get; set; } //(int, not null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public int? DeletedUser { get; set; } //(int, null)
        public int CompanyId { get; set; } //(int, not null)
        public bool isDeleted { get; set; } //(bit, not null)
        public string Token { get; set; } //(nvarchar(50), null)
        public string CheckoutUrl { get; set; } //(nvarchar(max), null)
        public string ErrorCode { get; set; } //(nvarchar(5), null)
        public string TransactionStatus { get; set; } //(nvarchar(10), null)
        public string TransactionId { get; set; } //(nvarchar(50), null)
    }
}