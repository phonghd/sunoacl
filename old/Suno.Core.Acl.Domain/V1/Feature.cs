using System;
using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V1
{
    public partial class Feature
    {
        public int FeatureId { get; set; } //(int, not null)
        public string Name { get; set; } //(nvarchar(255), not null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int? CreatedUser { get; set; } //(int, null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public string Description { get; set; } //(nvarchar(1023), null)

        public List<PackageFeature> PackageFeatures { get; set; }
        public List<Permission> Permissions { get; set; }
    }
}