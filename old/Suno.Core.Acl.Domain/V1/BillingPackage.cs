using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class BillingPackage
    {
        public int Id { get; set; } //(int, not null)
        public string Name { get; set; } //(nchar(150), not null)
        public string Description { get; set; } //(nvarchar(200), null)
        public decimal Price { get; set; } //(money, not null)
        public int NumberOfUnit { get; set; } //(int, not null)
        public byte Type { get; set; } //(tinyint, not null)
        public bool IsActive { get; set; } //(bit, not null)
        public bool? isDefaultPackage { get; set; } //(bit, null)
        public decimal? Discount { get; set; } //(money, null)
        public byte? DiscountPercent { get; set; } //(tinyint, null)
        public bool? IsPercent { get; set; } //(bit, null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CreatedUser { get; set; } //(int, not null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public int? DeletedUser { get; set; } //(int, null)
        public bool isDeleted { get; set; } //(bit, not null)
    }
}