using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class CompanyPackage
    {
        public long Id { get; set; } //(bigint, not null)
        public int CompanyId { get; set; } //(int, not null)
        public int PackageId { get; set; } //(int, not null)
        public DateTime ActivedDate { get; set; } //(datetime, not null)
        public DateTime? ExpiredDate { get; set; } //(datetime, null)
        public int PayStatus { get; set; } //(int, not null)
        public decimal Amount { get; set; } //(decimal(18,5), not null)
        public bool HasNextPackage { get; set; } //(bit, not null)
        public int ModifiedCount { get; set; } //(int, not null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int? CreatedUser { get; set; } //(int, null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public int? DeletedUser { get; set; } //(int, null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)

        public Company Company { get; set; }
        public Package Package { get; set; }
    }
}