using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class MailQueueHistory
    {
        public long Id { get; set; } //(bigint, not null)
        public long QueueId { get; set; } //(bigint, not null)
        public DateTime ExeDate { get; set; } //(datetime, not null)
        public bool ExeStatus { get; set; } //(bit, not null)
        public long TenantId { get; set; } //(bigint, not null)

    }
    
}