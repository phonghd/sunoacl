using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class Rating
    {
        public long Id { get; set; } //(bigint, not null)
        public byte Score { get; set; } //(tinyint, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int CompanyId { get; set; } //(int, not null)
        public int UserId { get; set; } //(int, not null)
    }
}