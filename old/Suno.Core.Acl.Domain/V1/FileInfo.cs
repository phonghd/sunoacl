using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class FileInfo
    {
        public long Id { get; set; } //(bigint, not null)
        public decimal FileSize { get; set; } //(decimal(18,0), not null)
        public int FileTypeId { get; set; } //(int, not null)
        public int ContentType { get; set; } //(int, not null)
        public string ContentTypeText { get; set; } //(nvarchar(511), not null)
        public int? CompanyId { get; set; } //(int, null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public int? CreatedUser { get; set; } //(int, null)
        public DateTime? UpdatedDate { get; set; } //(datetime, null)
        public int? UpdatedUser { get; set; } //(int, null)
        public bool IsDeleted { get; set; } //(bit, not null)
        public DateTime? DeletedDate { get; set; } //(datetime, null)
        public int? DeletedUser { get; set; } //(int, null)
    }
}