namespace Suno.Core.Acl.Domain.V1
{
    public partial class PackageFeatureLimit
    {
        public int PackageId { get; set; } //(int, not null)
        public int? MaxEmailPerMonth { get; set; } //(int, null)
        public int? MaxUser { get; set; } //(int, null)
        public int? MaxStore { get; set; } //(int, null)
        public int? MaxSaleOrderPerDay { get; set; } //(int, null)
        public int? MaxProduct { get; set; } //(int, null)
        public int? MaxProductItem { get; set; } //(int, null)
    }
}