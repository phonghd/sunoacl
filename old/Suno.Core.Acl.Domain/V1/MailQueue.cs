using System;

namespace Suno.Core.Acl.Domain.V1
{
    public class MailQueue
    {
        public long Id { get; set; } //(bigint, not null)
        public string Name { get; set; } //(nvarchar(max), not null)
        public int Status { get; set; } //(int, not null)
        public int ExeCount { get; set; } //(int, not null)
        public int OrderIndex { get; set; } //(int, not null)
        public DateTime CreatedDate { get; set; } //(datetime, not null)
        public long MailEntryId { get; set; } //(bigint, not null)
        public long TenantId { get; set; } //(bigint, not null)
        public DateTime ExeDate { get; set; } //(datetime, not null)


    }
}