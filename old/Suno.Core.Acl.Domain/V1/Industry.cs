using System.Collections.Generic;

namespace Suno.Core.Acl.Domain.V1
{
    public class Industry
    {
        public int Id { get; set; } //(int, not null)
        public string Name { get; set; } //(nvarchar(511), null)

        public List<Company> Companies { get; set; }
    }
}