﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Suno.Core.Hash
{
    public class SaltedHash : IHashProvider
    {
        private readonly HashAlgorithm HashProvider;
        private readonly int SalthLength;

        public SaltedHash(HashAlgorithm HashAlgorithm, int theSaltLength)
        {
            this.HashProvider = HashAlgorithm;
            this.SalthLength = theSaltLength;
        }

        public SaltedHash()
            : this((HashAlgorithm)new SHA256Managed(), 4)
        {
        }

        private byte[] ComputeHash(byte[] Data, byte[] Salt)
        {
            byte[] buffer = new byte[Data.Length + this.SalthLength];
            Array.Copy((Array)Data, (Array)buffer, Data.Length);
            Array.Copy((Array)Salt, 0, (Array)buffer, Data.Length, this.SalthLength);
            return this.HashProvider.ComputeHash(buffer);
        }

        public void GetHashAndSalt(byte[] Data, out byte[] Hash, out byte[] Salt)
        {
            Salt = new byte[this.SalthLength];
            new RNGCryptoServiceProvider().GetNonZeroBytes(Salt);
            Hash = this.ComputeHash(Data, Salt);
        }

        public void GetHashAndSaltString(string Data, out string Hash, out string Salt)
        {
            byte[] Hash1;
            byte[] Salt1;
            this.GetHashAndSalt(Encoding.UTF8.GetBytes(Data), out Hash1, out Salt1);
            Hash = Convert.ToBase64String(Hash1);
            Salt = Convert.ToBase64String(Salt1);
        }

        public bool VerifyHash(byte[] Data, byte[] Hash, byte[] Salt)
        {
            byte[] hash = this.ComputeHash(Data, Salt);
            if (hash.Length != Hash.Length)
                return false;
            for (int index = 0; index < Hash.Length; ++index)
            {
                if (!Hash[index].Equals(hash[index]))
                    return false;
            }
            return true;
        }

        public bool VerifyHashString(string Data, string Hash, string Salt)
        {
            byte[] Hash1 = Convert.FromBase64String(Hash);
            byte[] Salt1 = Convert.FromBase64String(Salt);
            return this.VerifyHash(Encoding.UTF8.GetBytes(Data), Hash1, Salt1);
        }
    }
}
